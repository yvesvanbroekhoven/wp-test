<?php
/*
Template Name: Blog Template
*/
?>
<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    get_post_meta($post->ID,'_sidebar',true);

$blog_layout = $super_options[SN."_blog_layout"];
$items_limit = $super_options[SN."_posts_item_limit"];
$items_limit =  (!$items_limit) ? 6 : $items_limit ; 

$clear  = 1;
switch($blog_layout)
{
	case "List Layout" : $pwidth = 980; $pheight = 400; break;
	case "Grid Layout" : $pwidth = 280; $pheight = 200;$clear = 2;  break; 
	
}

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 629;
	switch($blog_layout)
	{
	  case "List Layout" : $pwidth = 629; $pheight = 250; break;
	  case "Grid Layout" : $pwidth = 280; $pheight = 200; $clear = 2; break; 
	  
	}
	
else :
	$width = 980;
	$layout = 'full-width';
endif;

$categories = array("c_name"=>"category_name");
$categories['inputs'] = (trim(get_post_meta($post->ID,"_category",true))=="") ? false : get_post_meta($post->ID,"_category",true);

 get_header(); ?>   

<?php  if(have_posts()): while(have_posts()) : the_post(); ?>
 <?php  if($super_options[SN."_breadcrumbs_enable"]=="true") $helper->breadcrumbs(); ?> <!-- The breadcrumb for the theme -->
<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1 class="custom-font"><?php the_title(); ?></h1>
   </div>
</div> <!-- End of Parent Slider Container -->


<div class="container page content   <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php if($sidebar!="full") echo "two-third-width"; else echo "full-width"; ?> preload" id="main-content">
    
    
        <div class="posts <?php if($blog_layout=="Grid Layout") echo "grid-layout"; ?> blog-posts">
        
       <?php $helper->showPosts(array( "post_type"=>"post" , "image_width"=> $pwidth , "image_height"=> $pheight, 'extras' => true , 'content_limit' =>200 ,  'clear' => $clear , 'limit'=> $items_limit  , 'separator' => true , 'categories' => $categories));  ?>
        
        
        </div>
                  
   <?php $helper->pagination(); ?>
            
    
    </div>
     <?php  
	  wp_reset_query();
	  if($sidebar!="full") 
	  get_sidebar();  
	 ?> 

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      