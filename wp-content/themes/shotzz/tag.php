<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    "right-sidebar";
$blog_layout = "List Layout";
$clear  = 1;
switch($blog_layout)
{
	case "List Layout" : $pwidth = 980; $pheight = 400; break;
	case "Grid Layout" : $pwidth = 305; $pheight = 200;$clear = 2;  break; 
	
}

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 618;
	switch($blog_layout)
	{
	  case "List Layout" : $pwidth = 629; $pheight = 250; break;
	  case "Grid Layout" : $pwidth = 295; $pheight = 200; $clear = 2; break; 
	  
	}
	
else :
	$width = 980;
	$layout = 'full-width';
endif;

 get_header(); ?>   


 <?php  if($super_options[SN."_breadcrumbs_enable"]=="true") $helper->breadcrumbs(); ?> <!-- The breadcrumb for the theme --><div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1 class="custom-font"> Tags : <?php single_tag_title(); ?> </h1>
   </div>
</div> <!-- End of Parent Slider Container -->


<div class="container page content   <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php if($sidebar!="full") echo "two-third-width"; else echo "full-width"; ?> preload" id="main-content">
    
    
        <div class="posts <?php if($blog_layout=="Grid Layout") echo "grid-layout"; ?> blog-posts">
        
       
            
		 <ul class="clearfix posts">
          <?php 
	        $counter = 1; $clear = 1;
	        if ( have_posts() ) : while ( have_posts() ) : the_post();
		    
            $more = 0;
			if($counter==0)
			{
				$counter = $clear; echo "<li class='separator'><a href='#'>TOP</a></li>";
			}
	      ?>
              <li class="clearfix">
                 <?php 
				
				 	$width = "half";
			  
			if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) : 
				 
					 $id = get_post_thumbnail_id();
	          	     $ar = wp_get_attachment_image_src( $id , array(9999,9999) );
	    	         $theImageSrc = $helper->getMUFix($ar[0]);
					 echo $helper->imageDisplay($theImageSrc , $pheight , $pwidth , true);
				
				
				 ?>
                 
                     
               <?php else: $width = "";  endif; ?>
                      <div class="description <?php echo $width;?> ">
                        <h2 class="custom-font"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> 
                        
                               <p>
                                 <?php  
							  global $more;    // Declare global $more (before the loop).
                              $more = 1;
							  $content = get_the_content('');
							  $content = apply_filters('the_content', $content);
                              $content = str_replace(']]>', ']]&gt;', $content);
							  $helper->shortenContent( 100 ,  strip_tags( $content  ) ); ?>
                             
                              </p>
                               <a class="more-link" href="<?php the_permalink() ?>"><?php _e('Continue &rarr;', 'h-framework'); ?></a>
                                <ul class="extras clearfix">
							 
							 <li class="date"><?php _e( get_the_time("d")." ".get_the_time("M")." ".get_the_time("Y") , 'h-framework'); ?></li>                             <li class="comment">
                              <a href="<?php the_permalink(); ?>#comment" > <?php _e("Comments ", 'h-framework');  comments_number('0', '1', '%'); ?></a>
                            </li>
                            </ul>
                       </div>
                       
                </li>
          <?php  $counter--; endwhile; else:
              echo '<h4>'.__( 'No posts yet !','h-framework' ).'</h4>';
            endif;
        	?>
     </ul>
     </div>
        <?php $helper->pagination(); ?>
            
    
    </div>
     <?php  
	  wp_reset_query();
	  if($sidebar!="full") 
	  get_sidebar();  
	 ?> 

</div>



<?php get_footer(); ?>
      