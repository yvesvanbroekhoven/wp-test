<?php
/*
Template Name: Portfolio 3 Column Page Template
*/
?>
<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    get_post_meta($post->ID,'_sidebar',true);

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 165; $height = 120;
else :
	$width = 280; $height = 210;
	$layout = 'full-width';
endif;

$items_limit = $super_options[SN."_portfolio3_item_limit"];
$items_limit =  (!$items_limit) ? 6 : $items_limit ; 
$limit =  $super_options[SN."_portfolio3_limit"];
$limit = (int) (!$limit) ? 250 : $limit;


$categories = array("c_name"=>"portfoliocategories");
$categories['inputs'] = (trim(get_post_meta($post->ID,"_category",true))=="") ? false : get_post_meta($post->ID,"_category",true);

 get_header(); ?>   

<?php  if(have_posts()): while(have_posts()) : the_post(); ?>

<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1><?php the_title(); ?></h1>
   </div>
</div> <!-- End of Parent Slider Container -->

 
<div class="container page content   <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php echo $layout ?>" id="main-content">
     
	 
	 <?php the_content(); ?>
   
      <div class="portfolio-three-column portfolio preload">
       <?php $helper->showPosts(array( "post_type"=>"portfolio" , "image_width"=> $width , "image_height"=> $height, 'extras' => false , 'content_limit' =>$limit ,  'clear' => 3 , 'limit'=> $items_limit  , 'separator' => true , 'categories' => $categories));  ?>
       </div>
    
      <?php $helper->pagination(); ?>
      
    </div>
     <?php  
	  wp_reset_query();
	  if($sidebar!="full")
	    get_sidebar();  
	 ?> 

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      