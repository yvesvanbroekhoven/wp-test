<?php
/*
Template Name: Portfolio 1 Column Page Template
*/
?>
<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    get_post_meta($post->ID,'_sidebar',true);

$items_limit = $super_options[SN."_portfolio1_item_limit"];
$items_limit =  (!$items_limit) ? 6 : $items_limit ; 
$limit =  $super_options[SN."_portfolio1_limit"];
$limit = (int) (!$limit) ? 250 : $limit;


	
if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 629; $height = 250;
else :
	$width = 980; $height = 350;
	$layout = 'full-width';
endif;


$categories = array("c_name"=>"portfoliocategories");
$categories['inputs'] = (trim(get_post_meta($post->ID,"_category",true))=="") ? false : get_post_meta($post->ID,"_category",true);

 get_header(); ?>   

<?php  if(have_posts()): while(have_posts()) : the_post(); ?>

<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1><?php the_title(); ?></h1>
   </div>
</div> <!-- End of Parent Slider Container -->

<div class="container page content   <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php echo $layout ?>" id="main-content">
     
	 
	 <?php  the_content(); ?>
   
      <div class="portfolio-one-column portfolio preload">
       <?php $helper->showPosts(array( "post_type"=>"portfolio" , "image_width"=> $width , "image_height"=> $height, 'extras' => false , 'content_limit' => $limit ,  'clear' => 1 , 'limit'=> $items_limit  , 'separator' => true , 'categories' => $categories));  ?>
       </div>
    
      <?php $helper->pagination(); ?>
      
    </div>
     <?php  
	  wp_reset_query();
	  if($sidebar!="full")
	    get_sidebar();  
	 ?> 

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      