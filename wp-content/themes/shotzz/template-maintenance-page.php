<?php
/*
Template Name: Maintenance Page Template
*/
?>
<?php

// == Layout Calculation =========================================

$helper->includeFile('fliptimer.js');


$hasSidebar = "";
$sidebar = "full";

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 618;
else :
	$width = 980;
	$layout = 'full-width';
endif;

 get_header();
  ?>   
<script type="text/javascript">
jQuery(function($){
	
	var y = <?php echo $super_options[SN."_uc_year"]?>;
	var m = <?php echo $super_options[SN."_uc_month"]?>;
	var d = <?php echo $super_options[SN."_uc_date"]?>;
	
	$("#udtimer").fliptimer({ year:y,month:m,days:d , path:"<?php echo URL."/sprites/k/"; ?>" });
	
	});
</script>

<?php  if(have_posts()): while(have_posts()) : the_post(); ?>
 
<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1 class="custom-font"><?php the_title(); ?></h1>
   </div>
</div> <!-- End of Parent Slider Container -->


<div class="container page    clearfix"> <!-- Start of loop -->
   
     <div class="full-width clearfix" id="main-content">
     <?php the_content(); ?>
     </div>
     
     <div id="udtimer" class="clearfix"></div>

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      