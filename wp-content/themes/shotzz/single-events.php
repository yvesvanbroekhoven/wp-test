<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    get_post_meta($post->ID,'_sidebar',true);
$sidebar = "right-sidebar" ; // ~~ For posts existing prior to theme activation should have right sidebar by default ~ 

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 600;
else :
	$width = 980;
	$layout = 'full-width';
endif;

$address = get_post_meta($post->ID,"address",true)." ,".get_post_meta($post->ID,"city",true).", ".get_post_meta($post->ID,"state",true);

$map = get_post_meta($post->ID,"google_map",true);
$map = $map[0];

// == Extra Post Data =============================================

$slides =  get_post_meta($post->ID,"gallery_items",true);

if(!is_array($slides))
    $slides = array();

$count = count($slides);	
$noeffect = false; $showStage = true;

if($count<=1 && trim( $slides[0]["src"] ) == ""   ) 
    $noeffect = true;

if( $noeffect == true && ! has_post_thumbnail()  )
    $showStage = false;

if(! has_post_thumbnail()) $count = $count  - 1;

	


 get_header(); ?>   

<?php  if(have_posts()): while(have_posts()) : the_post(); ?>
 <?php  if($super_options[SN."_breadcrumbs_enable"]=="true") $helper->breadcrumbs(); ?> <!-- The breadcrumb for the theme -->
<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1 class="custom-font"><?php the_title(); ?></h1>
   </div>
</div> <!-- End of Parent Slider Container -->

<div class="container page content  single-events  <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php if($sidebar!="full") echo "two-third-width"; else echo "full-width"; ?> preload" id="main-content">
    
     <?php if($showStage) : ?>  <!-- Show the Stage if Featured Image and/or Extra Images are there -->
              <div class="theme-style-wrapper">  <!-- The Default Element Styler -->
                <div id="single-portfolio-stage" class="scrollable" >  <!-- Single Portfolio Image/Stage begins -->
                      <div class="items"> <!-- Scrollable Items -->
                      <?php 
                       if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())   ) 
                       {
							 $id = get_post_thumbnail_id();
							 $ar = wp_get_attachment_image_src( $id, array(9999,9999) );
							 $theImageSrc = $helper->getMUFix($ar[0]);
                       }
                       if($noeffect) :   
                             echo $helper->imageDisplay($theImageSrc , $height , $width , true ,$slide["src"]);  
                       else : 
                        
						  if(has_post_thumbnail())
                              echo $helper->imageDisplay($theImageSrc , $height , $width   , true  , $slide["src"]); 
                          $slides =  get_post_meta($post->ID,"gallery_items",true);
                          if(!is_array($slides)) $slides = array();
                          foreach($slides as $slide) :
                              if(trim($slide["src"])!="")
							  echo $helper->imageDisplay( $helper->getMUFix($slide["src"])  , $height , $width , true , $slide["src"] );                     
                          endforeach;	
						  
						   endif;			  
                       ?>
                      </div>
                      <?php  if( !$noeffect ) :  ?>
                       <div class="arrow-set clearfix">  <!-- Arrow Set -->
                           <a class="prev" href="#"> &larr; </a>
                           <a class="next" href="#"> &rarr; </a>
                       </div>
                       
                     <?php endif; ?> 
                 </div>  <!-- End of Single Portfolio Image/Stage begins -->
               
               </div> <!-- End of Default Element Styler -->
    <?php endif; ?>
    
     <?php the_content(); ?>
    
     
    
     <?php if($super_options[SN."_social_set"]=="" || $super_options[SN."_social_set"]=="ON") { ?>     
         <div class="social-stuff clearfix">
                <h6 class="custom-font heading"> <?php _e("Share this:",'h-framework'); ?></h6> <?php $helper->socialStuff(); ?> 
         </div>  
      <?php } ?>   
     

   
  <div class="single-pagination">
 
 <?php next_post_link('%link', __('Next Event','h-framework')); ?>
 <?php previous_post_link('%link', __('Previous Event','h-framework')); ?> 
</div>
    
    
     
    </div>
   
      <div class="sidebar clearfix" id="sidebar"><!-- start of one-third column -->
      <h3 class="custom-font heading"><?php _e('Event Details','h-framework') ?></h3>
       <ul id="event-meta-data">
         <li class="clearfix"> <span><?php _e('Starting Date','h-framework') ?>:</span> <span class="info"><?php echo get_post_meta($post->ID,"start_date",true); ?></span></li>
         <li class="last clearfix"> <span><?php _e('Ending Date','h-framework') ?>:</span> <span  class="info"><?php echo get_post_meta($post->ID,"ending_date",true); ?></span></li>
         
       
         <li  class="clearfix"> <span><?php _e('Starting Time','h-framework') ?>:</span> <span class="info"><?php echo get_post_meta($post->ID,"starting_time",true); ?></span></li>
         <li class="last clearfix"> <span><?php _e('Ending Time','h-framework') ?>:</span> <span class="info"><?php echo get_post_meta($post->ID,"ending_time",true); ?></span></li>
      
         
         <li class="last clearfix"> <span><?php _e('Address','h-framework') ?>:</span> <span class="info"><?php echo $address; ?></span></li>
         <li  class="clearfix"> <span><?php _e('Pincode','h-framework') ?>:</span> <span class="info"><?php echo get_post_meta($post->ID,"pincode",true); ?></span></li>
         <li class="last clearfix"> <span><?php _e('Contact','h-framework') ?>:</span> <span class="info"><?php echo get_post_meta($post->ID,"contact_no",true); ?></span></li>
         
         <li class="last clearfix"> <span><?php _e('Costs','h-framework') ?>:</span> <span class="info"><?php
		 if(trim( get_post_meta($post->ID,"event_cost",true))!="")
		  echo "". get_post_meta($post->ID,"event_cost",true); ?></span></li>
          
          <?php if(trim(get_post_meta($post->ID,"book_label",true))!=""): ?>
        <li class="end clearfix"> 
        
        <span class="booking"><a href="<?php echo get_post_meta($post->ID,"book_link",true); ?>"><?php echo get_post_meta($post->ID,"book_label",true); ?></a></span>
       
         </li>
         <?php endif; ?>
       </ul>
       <?php  if(trim($map)=="Google Map") : ?>
       <h3 class="custom-font heading"><?php _e('Location','h-framework') ?></h3>
        <div id="event-map" class="clearfix"> <?php
	
		 echo do_shortcode("[map address='$address' width='270' height='300' /]");
	  ?>
    </div>
      <?php endif; ?>
      
      </div>
</div>



<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      