<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =   "full";

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 618;
else :
	$width = 980;
	$layout = 'full-width';
endif;

 get_header(); ?>   



<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1 class="custom-font"><?php echo $super_options[SN."_notfound_title"]; ?> </h1>
   </div>
</div> <!-- End of Parent Slider Container -->

 
<div class="container page content   <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php if($sidebar!="full") echo "two-third-width"; else echo "full-width"; ?> preload" id="main-content">
    
    
    <p class="not-found"><img src=" <?php 
    if(!$super_options[SN."_notfound_logo"]) echo URL."/sprites/i/notfound.png"; 
    else echo $super_options[SN."_notfound_logo"]; ?>" atl="Page Not Found" title="Page Not Found" />
    </p>
    <p class="not-found"> <?php echo $super_options[SN."_notfound_text"]; ?> </p>
    <div class="error-search"><?php get_search_form(); ?></div>
    
   
    </div>
     <?php  
	  wp_reset_query();
	  if($sidebar!="full") 
	  get_sidebar();  
	 ?> 

</div>



<?php get_footer(); ?>
      