<?php
$stage_option = ($super_options[SN."_stage_option"]!="") ? $super_options[SN."_stage_option"] : "Slider";
$sidebar = ($super_options[SN."_home_layout"]!="") ? $super_options[SN."_home_layout"] : "full-width";

$post_type =  ($super_options[SN."_home_scroll_post"]!="") ? $super_options[SN."_home_scroll_post"] : "post";

get_header();  
?>   

<div id="page-starter">

<div id="home-slider"> <!--  Parent Slider Container -->
  <div class="inner-slider-wrapper"> <!--  Inner Slider Container -->
    <div class="container clearfix"> <!--   Slider Container -->
        
        
        
        
        <?php 
		
		switch($stage_option)
		{
			case "Slider" : 
			 
			 $sliders = unserialize(get_option(SN."_sliders"));
	         $slider = $sliders[$super_options[SN."_home_slider"]]; 
			
			 $home_slider = new Orion(
			       $slider["title"],"homeslider",
				   $slider["width"],$slider["height"],
				   $slider["type"],'',$slider['controls'],
				   $slider['autoplay'],$slider['slides'],
				   ( ((int)$slider["interval"] ) * 1000 )
				   ,$slider["desc"]
				   );
	         echo '<div class="homepage-slider preload">'.$home_slider->getSlider()."</div>";
			 break;
			 
			 case "Static Image" :  
			 $image = $helper->getMUFix($super_options[SN."_home_static_image"]);
			 echo '<div class="homepage-static-image">'.$helper->imageDisplay($image,350,980,true,$super_options[SN."_home_static_image"],1,false,'','',false)."</div>";
			 break;
			 
			 case "Title" :  
			 $title = stripslashes($super_options[SN."_home_title"]);
			 echo '<h1 class="custom-font">'.$title."</h1>";
			 break;
			 
			 case "Half Text half Staged Image" : 
			 $image = $helper->getMUFix($super_options[SN."_home_staged_image"]);
			 ?>
			 
            <div class="description">
            <h2  class="custom-font"><?php echo stripslashes($super_options[SN."_home_staged_title"]) ?></h2>
            <?php echo $helper->customFormat($super_options[SN."_home_staged_text"]) ?>
            </div>
            
            <div class="imageholder">
           <?php echo $helper->imageDisplay($image,360,500,false,'',0,false,'','',false); ?>
            </div>
			 
			 
			 <?php break;
			 
		}
		
		?>
        
        
        
    </div> <!--  End of Parent Slider Container -->
  </div> <!--  End of Parent Slider Container -->
</div> <!-- End of Parent Slider Container -->


</div> <!-- End of Starter  -->



<?php  if($super_options[SN."_home_top_columns_enable"]=="true") : ?>

<div class="home-page-top-columns"> <!--  Home Page 4 columns -->

  <div class="container clearfix">
  
 
    <?php for($i=1;$i<5;$i++) : ?>
    <div class="one_fourth <?php if($i==4) echo 'one_fourth_last'; ?>">
 
 <div class="iconed-title clearfix">     
 <?php if($super_options[SN."_home_column{$i}_image"]!="")echo "<img src='".$super_options[SN."_home_column{$i}_image"]."' alt='stage-image' />"; ?>    
 <h2 class="custom-font"><?php echo $helper->customFormat($super_options[SN."_home_column{$i}_title"]); ?></h2>      
 </div>
 
     <p><?php echo $helper->customFormat($super_options[SN."_home_column{$i}_text"]); ?></p>
     <?php if(trim($super_options[SN."_home_column{$i}_button_label"])!="") : ?>
       <a href="<?php echo $super_options[SN."_home_column{$i}_button_link"]; ?>" class="more">
           <?php echo $super_options[SN."_home_column{$i}_button_label"]; ?>
       </a>
    <?php endif; ?>
    </div>
    <?php endfor; ?>   
  </div>
</div>  <!-- End of Home Page 4 columns -->

<?php endif; ?>


<?php  if($super_options[SN."_home_rp_enable"]=="true") : ?>
   
<div class="latest-home-posts clearfix">
	<div class="container clearfix">
     <div class="one_fourth">
     <h2 class="custom-font"><?php echo $helper->customFormat($super_options[SN."_home_rp_title"]); ?></h2>
     <?php echo wpautop($helper->customFormat($super_options[SN."_home_rp_text"])) ; ?>
    <?php  echo '<a href="'.$super_options[SN."_home_rp_link"].'" class="more custom-font">'.$super_options[SN."_home_rp_label"].'</a>'; ?>
     </div>
     
     <?php 
	 $post_type = $super_options[SN."_home_rp_post"];
	  wp_reset_query();
     $popPosts = new WP_Query();
	 $popPosts->query('post_type='.$post_type.'&posts_per_page=3'); 
     $i =0;
     while ($popPosts->have_posts()) : $popPosts->the_post();  $more = 0; ?>
     <div class="one_fourth<?php if($i==2) echo "_last"; ?>">
     <?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) : /* if post has post thumbnail */ ?>
      <div class="image">
      <?php 
            $id = get_post_thumbnail_id();
            $ar = wp_get_attachment_image_src( $id , array(9999,9999) );
            echo $helper->imageDisplay( $helper->getMUFix($ar[0])  ,  104 , 218 , true , $ar[0]  ); 
      ?>
      </div><!--image-->
      <?php endif; ?>
    
      <div class="description">
		<h3 class="custom-font"><a href="<?php the_permalink(); ?>"><?php $helper->shortenContent(60,strip_shortcodes( get_the_title() )); ?></a></h3>
        <span class="extra-info custom-font"> <?php echo get_the_date('M,d,Y'); ?> </span>
         <a href="<?php the_permalink(); ?>" class="more"> More </a>
      </div><!--details-->
      
     </div>
     <?php $i++; endwhile; ?>
       
    </div>
</div>

<?php endif; ?>

<?php  if($super_options[SN."_blurb_enable"]=="true") : if($super_options[SN."_blurb_button_link"]=="Disable") $cl = 'full-width'; else $cl = 'hasButton';
$blink = '';
switch($super_options[SN."_blurb_button_link"])
{
	case "Custom link" : $blink =  $super_options[SN."_blurb_custom_link"]; break;
	default : $blink = $super_options[SN."_blurb_link"]; break;
}



 ?>
<div class="blurb-wrapper"> <!-- Blurb Wrapper -->
  <div class="inner-blurb-wrapper clearfix container  <?php echo $cl; ?>">
    <p class="blurb-text fancy-title"><?php echo $helper->customFormat($super_options[SN."_blurb_text"]); ?></p>
    <?php if($super_options[SN."_blurb_button_link"]!="Disable") : ?>
    
    <p class="blurb-button">
      <a href="<?php echo $blink; ?>"><?php echo $super_options[SN."_blurb_button_label"]; ?></a>
    </p>
	<?php endif; ?>
  </div>
</div> <!-- Blurb Wrapper Ends -->
<?php endif; ?>
       

<div class="container home-page-content content <?php echo $sidebar; ?> clearfix"> <!-- Start of loop -->
       
       
    <div class="<?php if($sidebar!="full-width") echo "two-third-width"; else echo "full-width";  ?> home-editor-content" id="main-content">
		 <?php echo $helper->customFormat(get_option("hades_home_text")); ?>
         
        <div class="home-widgets preload clearfix">
           
           <div class="home-sidebar  clearfix"><?php dynamic_sidebar ('Home Bottom Widget Area 1');  ?></div>
           <div class="home-sidebar no-margin-right clearfix"><?php dynamic_sidebar ('Home Bottom Widget Area 2');  ?></div>
       
        </div>
    
   </div>
 
    
   <?php   wp_reset_query(); 	 if($sidebar!="full-width") : ?>
   
    <div class="sidebar" id="sidebar"><!-- start of one-third column -->

        <?php   
        $dsidebar = $super_options[SN."_home_sidebar"];
        
       if ( trim($dsidebar)!=""  ) 
        dynamic_sidebar ($dsidebar); 
       else  
        dynamic_sidebar ("Blog Sidebar"); 
  ?>  
    </div><!-- end of one-third column -->
    
  <?php endif; ?>


     
</div>
 

<?php get_footer(); ?>