<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =  "full"; //  get_post_meta($post->ID,'_sidebar',true);

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 618;
else :
	$width = 980;
	$layout = 'full-width';
endif;
$gheight = 500; $gwidth = 970;
// == Extra Post Data =============================================

$gallery = get_post_meta($post->ID,"gallery_column",true);
$slides =  get_post_meta($post->ID,"gallery_items",true);
$type = trim(get_post_meta($post->ID,"gallery_type",true));

switch($type)
{
	case "Flickr" : 
	case "Galleria" : 
					  $helper->includeFile('galleria-1.2.5.min.js');
					  $helper->includeFile('galleria-theme/galleria.classic.js');
	                  break;
	case "Titan Gallery" : $helper->includeFile('jquery.titangallery.js'); break;				  
	case "Landscape 970 x 500" : $gheight = 500; $gwidth=980;   break;	
	case "Landscape 450 x 300" : $gheight = 300; $gwidth=450;   break;				  
	case "Landscape 300 x 200" : $gheight = 200; $gwidth=300;   break;			
	case "Portrait 520 x 440" :	  $gheight = 520; $gwidth=440;   break;	
	case "Portrait 300 x 290" :	  $gheight = 300; $gwidth=290;   break;	
	case "Portrait 240 x 210" :	  $gheight = 240; $gwidth=210;   break;			
}
			
if(!is_array($slides))
    $slides = array();

$count = count($slides);	
$noeffect = false; $showStage = true;

if($count<=1 && trim( $slides[0]["src"] ) == "" ) 
    $noeffect = true;

if( $noeffect == true && ! has_post_thumbnail() )
    $showStage = false;

if(! has_post_thumbnail()) $count = $count  - 1;

$swidth =  $width/( (int)$count + 1 );		


get_header(); ?>   



<?php  if(have_posts()): while(have_posts()) : the_post(); ?> <!-- Begin Main Loop -->

<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1><?php the_title(); ?></h1>
   </div>
</div> <!-- End of Parent Slider Container -->

<div class="page container content gallery <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
    <?php if(!post_password_required($post->ID)) : ?>
    <div class="<?php echo $layout ?>" id="main-content">
    
    <?php 
	 the_content(); 
	 
	// == Switching Module based on the backend option ==================
	
	
	switch($type)
	{
		case "Galleria" : include(HPATH."/mods/galleries/galleria.php"); break;
		case "Titan Gallery" : include(HPATH."/mods/galleries/titan_gallery.php"); break;
		case "Flickr" : include(HPATH."/mods/galleries/flickr.php"); break;
		case "Portrait 300 x 290" :
		case "Portrait 240 x 210" :
		case "Portrait 520 x 440" : ?>
                          <div class=' clearfix gallery-portrait preload <?php echo str_replace(" ","",$type); ?>'> <?php
                          foreach($slides as $slide): 
						  $info ='';
						  if(trim($slide['title'])!="")
						  $info = "<span>$slide[title]</span>";
                          echo $helper->imageDisplay( $helper->getMUFix($slide["src"])  ,  $gheight , $gwidth , true , $slide["src"] , true, true ); 
                          endforeach; ?> 
                          </div> <?php
		
		 break;
		case "Landscape 300 x 200" :
		case "Landscape 450 x 300" :
		case "Landscape 970 x 500" : ?>
                          <div class=' clearfix gallery-landscape preload <?php echo str_replace(" ","",$type); ?>'> <?php
                          foreach($slides as $slide): 
						  
						  $info ='';
						  
						  if(trim($slide['title'])!="")
						  $info = "<span>$slide[title]</span>";
						  
						 
						  
                          echo $helper->imageDisplay( $helper->getMUFix($slide["src"])  ,  $gheight , $gwidth , true , $slide["src"] , true, true    ); 
                          endforeach; ?> 
                          </div> <?php
		
		 break;
	}
	
	?> 
    
    <?php endif; if(post_password_required($post->ID)) {
		 echo '<div class="password-form">'; 
	the_content(); 
	  echo '</div>'; } ?>
     
     
    </div>
    <?php  
	  wp_reset_query();
	  
	  if($sidebar!="full")
	  get_sidebar();  
	?> 

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      