<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    get_post_meta($post->ID,'_sidebar',true);
$sidebar = (trim($sidebar)=="") ? "full" : $sidebar; // ~~ For pages existing prior to theme activation should have full width by default ~  

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 618;
else :
	$width = 980;
	$layout = 'full-width';
endif;

 get_header(); ?>   

<?php  if(have_posts()): while(have_posts()) : the_post(); ?>
 <?php  if($super_options[SN."_breadcrumbs_enable"]=="true") $helper->breadcrumbs(); ?> <!-- The breadcrumb for the theme -->
<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     <h1 class="custom-font"><?php the_title(); ?></h1>
   </div>
</div> <!-- End of Parent Slider Container -->


<div class="container page content   <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php if($sidebar!="full") echo "two-third-width"; else echo "full-width"; ?> preload" id="main-content">
    
     <?php 
	 if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) 
	 {
		   $id = get_post_thumbnail_id();
		   $ar = wp_get_attachment_image_src( $id, array(9999,9999) );
		   $theImageSrc = $helper->getMUFix($ar[0]);
		   echo $helper->imageDisplay($theImageSrc , 350 , $width , false , true );  
	 }
	 ?>
    
     <?php the_content(); ?>
    </div>
     <?php  
	  wp_reset_query();
	  if($sidebar!="full") 
	  get_sidebar();  
	 ?> 

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      