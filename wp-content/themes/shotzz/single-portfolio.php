<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    "right-sidebar";
$vthumb = trim(get_post_meta($post->ID,"video_thumbnail",true));
if(trim($vthumb=="")) $vthumb = "No"; 
 
if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 529; $height = 550;
else :
	$width = 980; $height = 550;
	$layout = 'full-width';
endif;


// == Extra Post Data =============================================

$work_link = get_post_meta($post->ID,"_portfolio_link",true);
$slides =  get_post_meta($post->ID,"gallery_items",true);

		
if(!is_array($slides))
    $slides = array();

$count = count($slides);	
$noeffect = false; $showStage = true;

if($count<=1 && trim( $slides[0]["src"] ) == ""   && $vthumb!="Yes" ) 
    $noeffect = true;

if( $noeffect == true && ! has_post_thumbnail() && $vthumb!="Yes" )
    $showStage = false;

if(! has_post_thumbnail()) $count = $count  - 1;

$swidth =  $width/( (int)$count + 1 );		


get_header(); ?>   



<?php  if(have_posts()): while(have_posts()) : the_post(); ?> <!-- Begin Main Loop -->


<div class="container page content single-portfolio  <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php echo $layout ?>" id="main-content">
     
      
     <?php if($showStage) : ?>  <!-- Show the Stage if Featured Image and/or Extra Images are there -->
              <div class="theme-style-wrapper">  <!-- The Default Element Styler -->
                <div id="single-portfolio-stage" class="scrollable" >  <!-- Single Portfolio Image/Stage begins -->
                      <div class="items"> <!-- Scrollable Items -->
                      <?php 
                       if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())   ) 
                       {
							 $id = get_post_thumbnail_id();
							 $ar = wp_get_attachment_image_src( $id, array(9999,9999) );
							 $theImageSrc = $helper->getMUFix($ar[0]);
                       }
                       if($noeffect) :   
                             echo $helper->imageDisplay($theImageSrc , $height , $width , true , $ar[0] , false, false,'' ,'' ,false);  
                       else : 
                        
						
						 
						if(trim($vthumb)=="Yes") :
			
						$video_type =  trim(get_post_meta($post->ID,"video_type",true));
						$video_link =  trim(get_post_meta($post->ID,"video_code",true));
			  
			  
						$code = '';
						switch($video_type){
							  
							  case "Dedicated" : $code = do_shortcode("[video src='{$video_link}' height={$height} width={$width} title='' ]");  break;
							  case "Youtube" : 
							  $video_link = explode("v=", $video_link);
							  $code = do_shortcode("[youtube id='{$video_link[1]}' height='{$height}' width='{$width}' title='' ]");  break;
							  case "Vimeo" :  
							  $video_link = explode("/", $video_link);
							  $code = do_shortcode("[vimeo id='".$video_link[count($video_link)-1]."' height='{$height}' width='{$width}' title='' ]");  break;
							  
							  
							  }
						echo "<div class='imageholder'> $code </div>";		
						endif;
					
					
						  if(has_post_thumbnail())
                              echo $helper->imageDisplay($theImageSrc , $height , $width  , true , $ar[0] , false, false,'' ,'' ,false);  
                          $slides =  get_post_meta($post->ID,"gallery_items",true);
                          if(!is_array($slides)) $slides = array();
                          foreach($slides as $slide) :
                              if(trim($slide["src"])!="")
							  echo $helper->imageDisplay( $helper->getMUFix($slide["src"])  , $height , $width , true , $ar[0] , false, false,'' ,'' ,false);                       
                          endforeach;	
						  
						   endif;			  
                       ?>
                      </div>
                      <?php  if( !$noeffect && $count>0 ) :  ?>
                       <div class="arrow-set clearfix">  <!-- Arrow Set -->
                           <a class="prev" href="#"> &larr; </a>
                           <a class="next" href="#"> &rarr; </a>
                       </div>
                       
                     <?php endif; ?> 
                 </div>  <!-- End of Single Portfolio Image/Stage begins -->
               
               </div> <!-- End of Default Element Styler -->
    <?php endif; ?>
    
   
    
     
      <div class="prev-post-thumb preload hoverable">
     
     <?php
$prev_post = get_previous_post() ;
if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail($prev_post->ID))   ) 
                       {
							 $id = get_post_thumbnail_id($prev_post->ID);
							 $ar = wp_get_attachment_image_src( $id, array(9999,9999) );
							 $theImageSrc = $helper->getMUFix($ar[0]);
							  echo $helper->imageDisplay($theImageSrc , 100 , 265 , false , get_permalink($prev_post->ID) );  
                       }
  
?>
</div>
     
   
    
     <div class="next-post-thumb preload hoverable">
     
     <?php
$next_post = get_next_post() ;
if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail($next_post->ID))   ) 
                       {
							 $id = get_post_thumbnail_id($next_post->ID);
							 $ar = wp_get_attachment_image_src( $id, array(9999,9999) );
							 $theImageSrc = $helper->getMUFix($ar[0]);
							  echo $helper->imageDisplay($theImageSrc , 100 , 265 , false , get_permalink($next_post->ID) );  
                       }
                       
?>
</div>
     
    </div>
    <?php  
	  wp_reset_query();
	?> 
  <div class="meta_sidebar">
   <div class="title">
     <h1><?php the_title(); ?></h1>
   </div>
   
   <?php the_content(); ?>
   
 
    <?php if(trim($work_link)!="") echo " <a href=\"{$work_link}\" class=\"workbutton\">Visit this project.</a>"; ?>
    
  </div>

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      