<!doctype html> <!-- Start of page HTML5 enabled -->
<head> <!-- Start of head  -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>
	 <?php
					    if(is_home()) echo bloginfo(__('name') , 'h-framework' );
					    elseif(is_category()) {
					         _e('Browsing the Category ' , 'h-framework' );
					          wp_title(' ', true, '');
					  } elseif(is_archive()) wp_title('', true,'');
					    elseif(is_search())  echo __( 'Search Results for' , 'h-framework' ).$s;
					    elseif(is_404())     _e( '404 - Page got lost!'  , 'h-framework');
					    else                 bloginfo(__('name' , 'h-framework')); wp_title(__('-' , 'h-framework'), true, '');
					  
      ?></title>
	

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" /><!-- Feed  -->
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="shortcut icon" href="<?php echo get_option(SN.'_favicon'); ?>" />
	<?php if ( is_singular() && get_option( 'thread_comments' ) )      wp_enqueue_script( 'comment-reply' );
		     wp_head(); ?>
    
     <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>/sprites/stylesheets/ie7.css" />
        <![endif]-->  
                 
</head> <!-- End of Head -->
 
<body> <!-- Start of body  -->


<div id="menu-bar">
   <div class="container clearfix">
       <?php $logourl = (!get_option(SN."_logo")) ? URL."/sprites/i/logo.png" : 	get_option(SN."_logo"); ?>
       <a href="<?php echo home_url(); ?>" id="logo"><img src="<?php echo $logourl; ?>" alt="logo" /></a>
       
       <?php 
	   if(!is_page_template("template-maintenance-page.php"))
				if(function_exists("wp_nav_menu"))
				{
					wp_nav_menu(array(
								'theme_location'=>'primary_nav',
								'container'=>'',
								'depth' => 3,
								'container_class' => 'clearfix',
								'menu_id' => 'menu',
								'walker' => new H_Menu_Frontend() )
								);
				}
	   ?>
             
   </div>
</div>