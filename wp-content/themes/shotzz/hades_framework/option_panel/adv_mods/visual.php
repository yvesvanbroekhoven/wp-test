<?php 

$path = __FILE__;
$pathwp = explode( 'wp-content', $path );
$wp_url = $pathwp[0];
require_once( $wp_url.'/wp-load.php' );


$stylePath =  TEMPLATEPATH."/sprites/stylesheets/plain";
$styleUrl = URL."/sprites/stylesheets/plain";

$get_styles = scandir($stylePath);

?>



<div id="visual_plain_panel">
<h2> Plain Styles </h2>

<?php 
/*
 global $wpdb;
 $output = $wpdb->get_results("SELECT option_name,option_value FROM $wpdb->options WHERE option_name like '".SN."%'",ARRAY_A );
 $output = json_encode($output);
 $output = base64_encode($output);	
 echo "<textarea> $output </textarea>"; 
	*/ ?>

<ul class="clearfix">
  
  <?php $i=0; foreach($get_styles as $styles) : if($i>1) {?>
  
  <li class="<?php echo $styles; ?>">
     <a href="<?php echo $styles; ?>">
      <img src="<?php echo $styleUrl."/".$styles."/snapshot.jpg"; ?>" alt="">
      <span><?php echo $styles; ?></span></a>
  </li>
  
  <?php } $i++; endforeach; ?>
  <input type="hidden" name="<?php echo SN; ?>_plain_theme" id="hades_plain_theme" value="<?php echo get_option(SN.'_plain_theme'); ?>" />
</ul>

</div>



<?php 
$cs = array();
 $styles = unserialize(get_option(SN."_canvas")); 
		 if(!is_array($styles)) $styles = array();
         foreach($styles as $canvas) {
			 $cs[] = $canvas['title'];
			 }
 $gf = array( 
					    "name" => "Select Canvas",
						"desc" => "select the canvas saved from Canvas manager.",
						"id" => $shortname."_canvas_style",
						"type" => "select",
						"options" => $cs,
						"std" => ""
								);

?>

<?php 
 $ui->createHadesSelect($gf);	
?>