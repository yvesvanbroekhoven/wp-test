<?php 

/* 

======================================================================================
== Hades Option Panel ================================================================
======================================================================================

Version 6.1 
Authors - WPTitans

Current Elements -
--------------------------------------------------------
  1.  Text Box            =  ( code => text ) Creates a text box. 
  2.  Text Area           =  ( code => textarea) Creates a textarea 
  3.  Checkboxes          =  ( code => checkbox) Creates checkboxes
  4.  Radio buttons       =  ( code => radio) Creates Radio buttons
  5.  Slider              =  ( code => slider) Creates a numeric slider
  6.  Color picker        =  ( code => colorpicker) Creates a color picker with a supporting textbox
  7.  Drop down lists     =  ( code => select) Creates a dropdown list
  8.  Toggle              =  ( code => toggle) Creates a Yes/No radio button set
  9.  Includes            =  ( code => include) Adds a way to include advance panels
  10. Sub Panel Activated =  ( code => subtitle) Ability to create nested panels
  11. Upload              =  ( code => upload) Creates an upload box
  12. Help                =  ( code => help) Creates a inframe which can be link to pages.
  13. Custom Block        =  ( code => custom) Allows to add custom blocks in panels/
======================================================================================

*/

// == ~~ Get Sliders ===============

$ex_sliders = unserialize(get_option(SN."_sliders"));
$sliders = array();

if(!is_array($ex_sliders)) $ex_sliders = array("No Sliders");
foreach($ex_sliders as $slider) $sliders[] = $slider['title']; 

$active_sidebars = get_option(SN."_active_sidebars");
$active_sidebars[] = "Blog Sidebar";
/* ====================================================================================== */
/* == General Settings Panel ============================================================ */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "General Settings",
	  	           "type" => "section"
		          );
$options[]   = array( 
			      "name" => $themename." Options",
			      "type" => "information",
			      "description" => ""
		           );
		  				  
$options[]   = array( "type" => "open");

		  

	
/* == Sub Panel Begins ================================================================== */

$options[]   = array(
				   "name" => "Basic Settings" , 
				   "type"=>"subtitle" , 
				   "id"=>"basic"
					 );

$options[]   = array(
                  "name" => "Logo Upload",
				  "desc" => "upload your logo here.",
				  "id" => $shortname."_logo",
				  "type" => "upload",
				  "std" => URL."/sprites/i/logo.png"	 
				  );

$options[]   = array(
                  "name" => "Favicon Upload",
				  "desc" => "upload your logo here.",
				  "id" => $shortname."_favicon",
				  "type" => "upload",
				  "std" => "your upload path",
				  "parentClass" => "h-advance"	 
				  );				  
				   
$options[]   = array(
                 "name" => "Tracking Code",
	             "desc" => "Add your Google Analytics code or some other, this will be in footer inside <strong>script tags.</strong>.",
	             "id" => $shortname."_tracking_code",
	             "type" => "textarea",
	             "std" => ""	 
				  );	


$options[]   = array(
                 "name" => "Custom Css Code",
	             "desc" => "Quick way of adding css style to your site.",
	             "id" => $shortname."_custom_css",
	             "type" => "textarea",
	             "std" => ""	 
				  );



$options[]   = array(
                 "name" => "Contact Email",
	             "desc" => "enter your Contact email this will be used for contact form and form builder forms.",
				 "id" => $shortname."_contact_email",
				 "type" => "text",
				 "std" => ""	 
				  );				  				  
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
	
/* == Sub Panel Begins ================================================================== */

$options[]   = array(
				   "name" => "BreadCrumbs" , 
				   "type"=>"subtitle" , 
				   "id"=>"BreadCrumbs"
					 );
					 	
$options[]   =  array( 
				  "name" => "Enable/ Disable Breadcrumbs",
				  "desc" => "Enable/ Disable Breadcrumbs all over the theme.",
				  "id" => $shortname."_breadcrumbs_enable",
				  "type" => "toggle",
				  "std" => "true"
					);	

$options[]   = array(
                  "name" => "Breadcrumb Delimiter",
	              "desc" => "Enter the symbol for separator words in breadcrumb.",
	              "id" => $shortname."_breadcrumb_delimiter",
	              "type" => "text",
	              "std" => "/"	 
				  );
				  
$options[]   = array(
                  "name" => "Enter Home Label",
	              "desc" => "Enter the home label for breadcrumb.",
	              "id" => $shortname."_breadcrumb_home_label",
	              "type" => "text",
	              "std" => "Home"	 
				  );				
									  				  										 
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */




/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Misc" , 
				   "type"=>"subtitle" , 
				   "id"=>"misc"
					 );

$options[]   = array( 
				  "name" => "Pagination Style",
				  "desc" => "select pagination style here.",
				  "id" => $shortname."_pagination",
				  "type" => "select",
				  "options" => array("numbers","next/previous"),
				  "std" => "numbers",
				  "parentClass" => "h-advance"
				 );



$options[]   = array( 
				  "name" => "Show Posts Comments",
				  "desc" => "show/hide posts comments.",
				  "id" => $shortname."_posts_comments",
				  "type" => "radio",
				  "options" => array("Yes","No"),
				  "std" => "Yes",
				 );	

$options[]   = array( 
				  "name" => "Show Page Comments",
				  "desc" => "show/hide pages comments.",
				  "id" => $shortname."_page_comments",
				  "type" => "radio",
				  "options" => array("Yes","No"),
				  "std" => "Yes"
				 );	
				 
$options[]   = array( 
				  "name" => "Show Portfolio Comments",
				  "desc" => "show/hide posts comments.",
				  "id" => $shortname."_portfolio_comments",
				  "type" => "radio",
				  "options" => array("Yes","No"),
				  "std" => "Yes",
				  "parentClass" => "h-advance"
				 );	

$options[]   = array( 
				  "name" => "Show Gallery Comments",
				  "desc" => "show/hide posts comments.",
				  "id" => $shortname."_gallery_comments",
				  "type" => "radio",
				  "options" => array("Yes","No"),
				  "std" => "Yes",
				  "parentClass" => "h-advance"
				 );					 				 				 		 				
				
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */

$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == General Settings Panel Ends ======================================================= */
/* ====================================================================================== */
					 
					 
/* ====================================================================================== */
/* == Home Page Panel =================================================================== */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Home Page",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the basic setting of Ambience. Just follow the info besides the functions and you will be ready in a snap."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Stage" , 
				   "type"=>"subtitle" , 
				   "id"=>"homestage"
					 );


$options[]   = array( 
				  "name" => "Home Page Intro",
				  "desc" => "you can select the layout of top stage here.",
				  "id" => $shortname."_stage_option",
				  "type" => "radio",
				  "options" => array("Slider","Static Image", "Title", "Half Text half Staged Image","none"),
				  "std" => "Half Text half Staged Image"
				  );

$options[]   = array( 
				  "name" => "Select Slider",
				  "desc" => "select the slider type.",
				  "id" => $shortname."_home_slider",
				  "type" => "select",
				  "options" => $sliders,
				  "parentClass" => "h-slider h-options",
				  "std" => "Slider"
				  );

$options[]   = array(
                  "name" => "Upload Image",
				  "desc" => "upload the image to be shown on intro on Home page.",
				  "id" => $shortname."_home_static_image",
				  "type" => "upload",
				  "parentClass" => "h-upload h-options",
				  "std" => ""
                  );	

$options[]   = array(
                  "name" => "Add Title",
				  "desc" => "Add the title to be shown on home page.",
				  "id" => $shortname."_home_title",
				  "type" => "text",
				  "parentClass" => "h-title h-options",
				  "std" => "Your Title"
                  );	
				  				  			  
$options[]   = array(
                  "name" => "Upload Image",
				  "desc" => "upload the image to be shown on intro on Home page.",
				  "id" => $shortname."_home_staged_image",
				  "type" => "upload",
				  "parentClass" => "h-staged h-options",
				  "std" => ""
                  );	

$options[]   = array(
                  "name" => "Add Stage Title",
				  "desc" => "Add the title to be shown on home page.",
				  "id" => $shortname."_home_staged_title",
				  "type" => "text",
				  "parentClass" => "h-staged h-options",
				  "std" => "Your Title"
                  );
				  	
$options[]   = array(
                  "name" => "Add Stage Text",
				  "desc" => "Add the text to be shown on home page supports html code.",
				  "id" => $shortname."_home_staged_text",
				  "type" => "textarea",
				  "parentClass" => "h-staged h-options",
				  "std" => "Your text here"
                  );	
				  				  		
										
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
	
/* == Sub Panel Begins ================================================================== */

$options[]   = array(
				   "name" => "Top 4 Columns" , 
				   "type"=>"subtitle" , 
				   "id"=>"tophomecolumns"
					 );
					 
$options[]   = array( 
				  "name" => "Enable/Disable Top Columns",
				  "desc" => "you can switch on or off the top 4 columns on home page.",
				  "id" => $shortname."_home_top_columns_enable",
				  "type" => "toggle",
				  "std" => "true"
				   );			

				  				   
for($c =1 ; $c < 5 ; $c++) {

$options[]   = array(
                  "name" => "Column $c image",
				  "desc" => "Enter the image path for $c column image(<strong> Leave blank if you do not want the image to appear</strong>).",
				  "id" => $shortname."_home_column{$c}_image",
				  "type" => "upload",
				  "std" => ""	 
				  );
				  
$options[]   = array(
                  "name" => "Enter Title",
				  "desc" => "Enter the title for $c column(<strong> Leave blank if you do not want the title to appear</strong>).",
				  "id" => $shortname."_home_column{$c}_title",
				  "type" => "text",
				  "std" => "Title {$c}"	 
				  );

$options[]   = array(
                  "name" => "Enter Description",
				  "desc" => "Enter the description for $c column.",
				  "id" => $shortname."_home_column{$c}_text",
				  "type" => "textarea",
				  "std" => "your default text"	 
				  );				  

$options[]   = array(
                  "name" => "Enter Button Label",
				  "desc" => "Enter the button label for $c column (<strong> Leave blank if you do not want button to appear</strong>).",
				  "id" => $shortname."_home_column{$c}_button_label",
				  "type" => "text",
				  "std" => "more &rarr;"	 
				  );
				  
$options[]   = array(
                  "name" => "Enter Button Link",
				  "desc" => "Enter the button link for $c column.",
				  "id" => $shortname."_home_column{$c}_button_link",
				  "type" => "text",
				  "std" => "#"	 
				  );
				  				  
}
	   


				  										 
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
/* == Sub Panel Begins ================================================================== */

$options[]   = array(
				   "name" => "Intro Text & Call To Action Module" , 
				   "type"=>"subtitle" , 
				   "id"=>"blurb"
					 );

$options[]   = array( 
				  "name" => "Enable/Disable Intro Text",
				  "desc" => "you can switch on or off the intro blurb.",
				  "id" => $shortname."_blurb_enable",
				  "type" => "toggle",
				  "std" => "true"
				   );	

$options[]   = array(
                  "name" => "Enter Intro text",
				  "desc" => "Enter the intro text.",
				  "id" => $shortname."_blurb_text",
				  "type" => "textarea",
				  "std" => "your default text"	 
				  );
				   			   
$options[]   = array(
                  "name" => "Enter Call to Action Button Label",
				  "desc" => "Enter the button's label.",
				  "id" => $shortname."_blurb_button_label",
				  "type" => "text",
				  "std" => "Know more"	 
				  );

$options[]   = array( 
				  "name" => "Call to Action Button's Options",
				  "desc" => "Enable / Disable or link to pages here.",
				  "id" => $shortname."_blurb_button_link",
				  "type" => "radio",
				  "options" => array("Link to a page","Custom link", "Disable"),
				  "std" => "Custom link"
				  );

 
  $pages = get_pages(); $page_filler = array(); 
  foreach ($pages as $pagg) {
  	$page_filler[get_page_link($pagg->ID)] = $pagg->post_title;
	
	
  }
 
 
$options[]   = array( 
				  "name" => "Link To",
				  "desc" => "select your link here.",
				  "id" => $shortname."_blurb_link",
				  "type" => "select",
				  "options" => $page_filler,
				  "std" => $page_filler[0],
				  "parentClass" => 'blurb-options b-link',
				  "keyed" => true
				);

$options[]   = array( 
				  "name" => "Custom Link",
				  "desc" => "add link here.",
				  "id" => $shortname."_blurb_custom_link",
				  "type" => "text",
				  "std" => "http://",
				  "parentClass" => 'blurb-options b-custom'
				);


												  				  
				  										 
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "HomePage Layout" , 
				   "type"=>"subtitle" , 
				   "id"=>"homelayout"
					 );

$options[]   = array(
				   "name" => "Home layout", 
				   "type"=>"include", 
				   "std"=> HPATH."/option_panel/adv_mods/home_layout.php"
					  );

$options[]   = array( 
				  "name" => "Home Sidebar",
				  "desc" => "you can select the sidebar of home page here.",
				  "id" => $shortname."_home_sidebar",
				  "type" => "select",
				  "options" => $active_sidebars,
				  "std" => "Blog Sidebar"
				  );

$options[]   = array(
                  "name" => "Enable/Disable Recent Posts Area ",
				  "desc" => "enable or disable the recent post area.",
				  "id" => $shortname."_home_rp_enable",
				  "type" => "toggle",
				  "std" => "true"	 
				  );	

				  				  
$options[]   = array(
                  "name" => "Enter Recent Posts Area Title",
				  "desc" => "Enter the title for bottom recent post area.",
				  "id" => $shortname."_home_rp_title",
				  "type" => "text",
				  "std" => "Title"	 
				  );	

$options[]   = array(
                  "name" => "Enter Recent Posts Area Text",
				  "desc" => "Enter the text for bottom recent post area.",
				  "id" => $shortname."_home_rp_text",
				  "type" => "textarea",
				  "std" => " "	 
				  );
				  				  
$options[]   = array(
                  "name" => "Enter Recent Posts Area Button Label",
				  "desc" => "Enter the label for bottom recent post area button.",
				  "id" => $shortname."_home_rp_label",
				  "type" => "text",
				  "std" => "Read the Blog"	 
				  );	
$options[]   = array(
                  "name" => "Enter Recent Posts Area Button Link",
				  "desc" => "Enter the link for bottom recent post area button.",
				  "id" => $shortname."_home_rp_link",
				  "type" => "text",
				  "std" => "#"	 
				  );	
				  				 				  
$options[]   = array( 
				  "name" => "Enter Recent Posts Area Post Type",
				  "desc" => "you can select which posts to display here.",
				  "id" => $shortname."_home_rp_post",
				  "type" => "select",
				  "options" => array('post','portfolio','gallery','events'),
				  "std" => "post"
				  );				  
				  			
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */


/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "HomePage Content" , 
				   "type"=>"subtitle" , 
				   "id"=>"homecontent"
					 );

$options[]   = array(
				   "name" => "Home Editor", 
				   "type"=>"include", 
				   "std"=> HPATH."/option_panel/adv_mods/home.php"
					  );


				
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */





$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == Home Page Panel Ends ============================================================== */
/* ====================================================================================== */
					 
/* ====================================================================================== */
/* == Typography Panel ================================================================== */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Typography Settings",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the typography related settings.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Body Font & Custom Font Settings" , 
				   "type"=>"subtitle" , 
				   "id"=>"bodytypo"
					 );



$options[]   = array(
						"name" => "Typography Panel File", 
						"type"=>"include", 
						"std"=> HPATH."/option_panel/adv_mods/typo.php"
					  );
										
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
	

$options[]   = array(
				   "name" => "Heading's Font Settings" , 
				   "type"=>"subtitle" , 
				   "id"=>"bodytypo"
					 );



$options[]   = array(
                  "name"=>"H1 Font Size",
			      "desc"=>"h1 font size.",
			      "id" => $shortname."_h1_font_size",
				  "type"=>"slider",
				  "max"=>108,
				  "std"=>36,
				  "suffix"=>"px"
					 );

$options[]   = array(
                  "name"=>"H2 Font Size",
			      "desc"=>"h2 font size.",
			      "id" => $shortname."_h2_font_size",
				  "type"=>"slider",
				  "max"=>108,
				  "std"=>32,
				  "suffix"=>"px"
					);

$options[]   = array(
                  "name"=>"H3 Font Size",
			      "desc"=>"h3 font size.",
			      "id" => $shortname."_h3_font_size",
				  "type"=>"slider",
				  "max"=>108,
				  "std"=>28,
				  "suffix"=>"px",
				  "parentClass" => "h-advance"
					 );

$options[]   = array(
                  "name"=>"H4 Font Size",
			      "desc"=>"h4 font size.",
			      "id" => $shortname."_h4_font_size",
				  "type"=>"slider",
				  "max"=>108,
				  "std"=>24,
				  "suffix"=>"px",
				  "parentClass" => "h-advance"
					 );
					 					 					 
$options[]   = array(
                  "name"=>"H5 Font Size",
			      "desc"=>"h5 font size.",
			      "id" => $shortname."_h5_font_size",
				  "type"=>"slider",
				  "max"=>108,
				  "std"=>18,
				  "suffix"=>"px",
				  "parentClass" => "h-advance"
					 );

$options[]   = array(
                  "name"=>"H6 Font Size",
			      "desc"=>"h6 font size.",
			      "id" => $shortname."_h6_font_size",
				  "type"=>"slider",
				  "max"=>108,
				  "std"=>13,
				  "suffix"=>"px",
				  "parentClass" => "h-advance"
					 );
					 					 										
$options[]   = array("type"=>"close_subtitle");

$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == Typography Ends =================================================================== */
/* ====================================================================================== */
					 
/* ====================================================================================== */
/* == Footer Panel ====================================================================== */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Footer",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the footer related settings.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Footer Settings" , 
				   "type"=>"subtitle" , 
				   "id"=>"fsettings"
					 );

$options[]   = array( 
				  "name" => "Footer Layout",
				  "desc" => "toogle display of footer widgets.",
				  "id" => $shortname."_footer_bg_layout",
				  "type" => "radio",
				  "options" => array("Full Width","Boxed"),
				  "std" => "Boxed"
				   );
				   
$options[]   = array( 
				  "name" => "Show Footer Widgets column area",
				  "desc" => "toogle display of footer widgets.",
				  "id" => $shortname."_footer_widgets",
				  "type" => "radio",
				  "options" => array("Yes","No"),
				  "std" => "Yes"
				   );
				   

$options[]   = array(
						"name" => "Footer Panel File", 
						"type"=>"include", 
						"std"=> HPATH."/option_panel/adv_mods/footer.php"
					  );

$options[]   = array( 
				  "name" => "Show Footer Menu",
				  "desc" => "toogle display of footer menu.",
				  "id" => $shortname."_footer_menu",
				  "type" => "radio",
				  "options" => array("Yes","No"),
				  "std" => "Yes",
				  "parentClass" => "h-advance"
				   );

$options[]   = array( 
				  "name" => "Footer Text",
				  "desc" => "footer text.",
				  "id" => $shortname."_footer_text",
				  "type" => "text",
				  "std" => "",
				  "parentClass" => "h-advance"
				   );
				   				   										
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
	


$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == Footer Ends ======================================================================= */
/* ====================================================================================== */

/* ====================================================================================== */
/* == Media Panel ======================================================================= */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Media Settings",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the media related settings.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Image Resizing" , 
				   "type"=>"subtitle" , 
				   "id"=>"imageresizing"
					 );


$options[]   = array( 
				  "name" => "Image resizing",
				  "desc" => "select the method you want for image resizing.",
				  "id" => $shortname."_image_resize",
				  "type" => "radio",
				  "options" => array("Timthumb","Wordpress Core resizer", "none"),
				  "std" => "Timthumb"
				   );	

$options[]   = array( 
				  "name" => "Timbthumb Cropping Options",
				  "desc" => "select the method you want for image resizing.",
				  "id" => $shortname."_timthumb_zc",
				  "type" => "radio",
				  "options" => array("Hard Resize","Smart crop and resize", "Resize Proportionally"),
				  "std" => "Smart crop and resize",
				  "parentClass" => "h-advance"
				   );	


										
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Portfolio Options" , 
				   "type"=>"subtitle" , 
				   "id"=>"portfoliooption"
					 );

$options[] = array(
                  "name"=>"Portfolio 1 Column Items Limit",
			      "desc"=>"set your items per page limit here",
				   "id" => $shortname."_portfolio1_item_limit",
				   "type"=>"slider",
				   "max"=>50,
				   "std"=>6,
				   "suffix"=>"Items");

$options[] = array(
                  "name"=>"Portfolio 2 Column Items Limit",
			      "desc"=>"set your items per page limit here",
				   "id" => $shortname."_portfolio2_item_limit",
				   "type"=>"slider",
				   "max"=>50,
				   "std"=>6,
				   "suffix"=>"Items");

$options[] = array(
                  "name"=>"Portfolio 3 Column Items Limit",
			      "desc"=>"set your items per page limit here",
				   "id" => $shortname."_portfolio3_item_limit",
				   "type"=>"slider",
				   "max"=>50,
				   "std"=>6,
				   "suffix"=>"Items");
				  
$options[] = array(
                  "name"=>"Portfolio 4 Column Items Limit",
			      "desc"=>"set your items per page limit here",
				   "id" => $shortname."_portfolio4_item_limit",
				   "type"=>"slider",
				   "max"=>50,
				   "std"=>6,
				   "suffix"=>"Items");				   				   				   
			
$options[] = array("name"=>"Portfolio 1 Column Words Limit",
			      "desc"=>"set your word limit here",
				   "id" => $shortname."_portfolio1_limit",
				   "type"=>"slider",
				   "max"=>1000,
				   "std"=>250,
				   "suffix"=>"letters");
				 
$options[] = array("name"=>"Portfolio 2 Column Words Limit",
			      "desc"=>"set your word limit here",
				   "id" => $shortname."_portfolio2_limit",
				   "type"=>"slider",
				   "max"=>1000,
				   "std"=>250,
				   "suffix"=>"letters");				   	 

$options[] = array("name"=>"Portfolio 3 Column Words Limit",
			      "desc"=>"set your word limit here",
				   "id" => $shortname."_portfolio3_limit",
				   "type"=>"slider",
				   "max"=>1000,
				   "std"=>250,
				   "suffix"=>"letters");

$options[] = array("name"=>"Portfolio 4 Column Words Limit",
			      "desc"=>"set your word limit here",
				   "id" => $shortname."_portfolio4_limit",
				   "type"=>"slider",
				   "max"=>1000,
				   "std"=>250,
				   "suffix"=>"letters");

$options[]   = array("type"=>"close_subtitle");
$options[]   = array(
				   "name" => "Flickr Options" , 
				   "type"=>"subtitle" , 
				   "id"=>"flickroption"
					 );

			
$options[] = array(
				  "name" => "Flickr API Key",
				  "desc" => "Enter your Flickr Key, to get yours visit <a target='_blank' href='http://www.flickr.com/services/api/misc.api_keys.html'> this page </a>.",
				  "id" => $shortname."_flickr_key",
				  "type"=> "text"
				  );
$options[] = array(
				  		"name" => "Flickr Account Name",
				  		"desc" => " Enter your Flickr account name in here.",
				  		"id" => $shortname."_flickr_name",
				 	 	"type"=> "text"
						);
				  				  
							   				   	
$options[]   = array("type"=>"close_subtitle");
$options[]   = array(
				   "name" => "Galleria Type Gallery Options" , 
				   "type"=>"subtitle" , 
				   "id"=>"galleriaoption"
					 );

			
$options[]   = array( 
				  "name" => "Image Crop",
				  "desc" => "Select Cropping mode for galleria gallery.",
				  "id" => $shortname."_galleria_crop",
				  "type" => "radio",
				  "options" => array("true","false","height","width"),
				  "std" => "true"
				   );	
  
							   				   	
$options[]   = array("type"=>"close_subtitle");
/* == Sub Panel Ends ===================================================================== */

$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == Media Ends ======================================================================== */
/* ====================================================================================== */	

/* ====================================================================================== */
/* == Blog Panel ======================================================================== */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Blog Settings",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the media related settings.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Blog Layout" , 
				   "type"=>"subtitle" , 
				   "id"=>"bloglayout"
					 );


$options[]   = array( 
				  "name" => "Blog Layout",
				  "desc" => "select the layout for blog template.",
				  "id" => $shortname."_blog_layout",
				  "type" => "radio",
				  "options" => array("List Layout","Grid Layout"),
				  "std" => "List Layout"
				   );	

	
$options[] = array(
                  "name"=>"Posts Items Limit",
			      "desc"=>"set your items per page limit here",
				   "id" => $shortname."_posts_item_limit",
				   "type"=>"slider",
				   "max"=>50,
				   "std"=>6,
				   "suffix"=>"Items");	
					  	
$options[]   = 	array( 
						"name" => "Show Author BIO",
						"desc" => "Don't you need an Author Bio then just disbale it here.",
						"id" => $shortname."_author_bio",
						"type" => "toggle",
						"std" => "true"
					  );
					/*
$options[]   = 	array( 
						"name" => "Show Related Posts",
						"desc" => "Want to show your related posts? Then enable them here.",
						"id" => $shortname."_popular",
						"type" => "toggle",
						"std" => "true"
					  );
					  
$options[]   = 	array( 				"name" => "No of posts to be displayed",
						"desc" => "The related post section is using a scroller so you ad as many as you want.",
						"id" => $shortname."_popular_no",
						"type" => "text",
						"std" => "4" ,
				  "parentClass" => "h-advance");
					*/
$options[]   = 	array( 
						"name" => "Enable AddThis Social Set",
						"desc" => "Enable or disable the retweet button below the post.",
						"id" => $shortname."_social_set",
						"type" => "toggle",
						"std" => "true"
					  );
$options[]   = 	 array( 
						"name" => "Set AddThis Icon Style",
						"desc" => "In here you can decide which icon style you want.",
						"id" => $shortname."_social_set_style",
						"type" => "select",
						"std" => "Google Webfonts",
						"options" => array( "Style 1","Style 2","Style 3","Style 4","Style 5","Style 6","Style 7","Style 8" ),
				  "parentClass" => "h-advance"
					  );
	
					  								  	
$options[]   = 	  array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
	


$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == Blog Ends ========================================================================= */
/* ====================================================================================== */					 

/* ====================================================================================== */
/* == Events Panel ====================================================================== */
/* ====================================================================================== 

$options[]   = array( 
		           "name" => "Events Settings",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the event related settings.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== 

$options[]   = array(
				   "name" => "Calendar Layout" , 
				   "type"=>"subtitle" , 
				   "id"=>"event"
					 );




										
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== 
	


$options[]   = array("type"=>"close");

 ====================================================================================== */
/* == Events Ends ======================================================================= */
/* ====================================================================================== */		

/* ====================================================================================== */
/* == Visual Panel ====================================================================== */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Visual Panel",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the visual related settings.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Visual Skins" , 
				   "type"=>"subtitle" , 
				   "id"=>"visual_premade_simple"
					 );

$options[]   = array( 
				  "name" => "Style Type",
				  "desc" => "select the layout for blog template.",
				  "id" => $shortname."_style_listener",
				  "type" => "radio",
				  "options" => array("Default","Plain Shades","Custom Canvas"),
				  "std" => "Default"
				   );	
				   
$options[]   = array(
						"name" => "Visual File", 
						"type"=>"include", 
						"std"=> HPATH."/option_panel/adv_mods/visual.php"
					  );


										
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
	


$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == Visual Ends ======================================================================= */
/* ====================================================================================== */		

/* ====================================================================================== */
/* == Advance Panel ====================================================================== */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Advance Panel",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "In this option panel your able to change the advance related settings.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  
/* == Sub Panel Begins =================================================================== */

$options[]   = array(
				   "name" => "Advance" , 
				   "type"=>"subtitle" , 
				   "id"=>"adv"
					 );
					 
$options[]   = array( 
				  "name" => "Admin Login Logo Enable",
				  "desc" => "Enable / Disable admin logo .",
				  "id" => $shortname."_enable_admin_logo",
				  "type" => "radio",
				  "options" => array("Yes","No"),
				  "std" => "No"
				   );	

$options[]   =	array( 
						"name" => "Admin Logo Area Width",
						"desc" => "set the width of logo holer here.",
						"id" => $shortname."_admin_logo_width",
						"type" => "text",
						"std" => ""
						);	
$options[]   =	array( 
						"name" => "Admin Logo Area Height",
						"desc" => "set the height of logo holer here.",
						"id" => $shortname."_admin_logo_height",
						"type" => "text",
						"std" => ""
						);	
												
$options[]   = array(
                  "name" => "Admin Login Logo Upload",
				  "desc" => "upload your wp admin logo here.",
				  "id" => $shortname."_admin_logo",
				  "type" => "upload",
				  "std" => "your upload path"	 
				  );


				  										
$options[]   = array("type"=>"close_subtitle");

/* == Sub Panel Ends ===================================================================== */
	
$options[]   =  array(
						"name" => "&rarr; 404 Not Found" , 
						"type"=>"subtitle" , 
						"id"=>"notfound"
						);	
$options[]   =	array( 
						"name" => "404 page title here",
						"desc" => "Add your 404 text here.",
						"id" => $shortname."_notfound_title",
						"type" => "text",
						"std" => ""
						);		
					
$options[]   =	array( 
						"name" => "404 image URL",
						"desc" => "Upload your 404 image.  ",
						"id" => $shortname."_notfound_logo",
						"type" => "upload",
						"std" => URL."/sprites/i/notfound.png"
					);	
				
	
$options[]   =	array( 
						"name" => "404 page text here",
						"desc" => "Add your 404 text here.",
						"id" => $shortname."_notfound_text",
						"type" => "textarea",
						"std" => ""
						);		
                
$options[]   =	array("type"=>"close_subtitle");
/* == Sub Panel Ends ===================================================================== */

/* == Sub Panel Begin ===================================================================== */
	
$options[]   =  array(
						"name" => "&rarr; Form " , 
						"type"=>"subtitle" , 
						"id"=>"notfound"
						);	

$options[]   =  array( "name" => "ReCaptacha Public Key",
					"desc" => "Required for the captcha to work, get your key from <a href='http://www.google.com/recaptcha/whyrecaptcha'>here</a>",
					"id" => $shortname."_captcha_public_key",
					"type" => "text",
					"std" =>  "" );		
				
$options[]   =  array( "name" => "ReCaptacha Private Key",
					"desc" => "Required for the captcha to work, get your key from <a href='http://www.google.com/recaptcha/whyrecaptcha'>here</a>",
					"id" => $shortname."_captcha_private_key",
					"type" => "text",
					"std" =>  "" );			
											
$options[]   =	array( 
						"name" => "Reply Message",
						"desc" => "Add yourReply Message text here.",
						"id" => $shortname."_notify_msg",
						"type" => "textarea",
						"std" => "You have received a mail :) "
						);		
					

                
$options[]   =	array("type"=>"close_subtitle");

/* == Sub Panel Begin ===================================================================== */
	
$options[]   =  array(
						"name" => "&rarr; Timer Data" , 
						"type"=>"subtitle" , 
						"id"=>"notfound"
						);	
$options[]   =	array( 
						"name" => "Enter Year in 4 digits",
						"desc" => "Set your deadline date's year",
						"id" => $shortname."_uc_year",
						"type" => "text",
						"std" => "2012"
						);			

$options[]   =	array( 
						"name" => "Enter Month in 2 digits",
						"desc" => "Set your deadline date's year",
						"id" => $shortname."_uc_month",
						"type" => "text",
						"std" => "12"
						);						

$options[]   =	array( 
						"name" => "Enter date in 2 digits",
						"desc" => "Set your deadline date's year",
						"id" => $shortname."_uc_date",
						"type" => "text",
						"std" => "21"
						);	
                
$options[]   =	array("type"=>"close_subtitle");

$options[]   = array("type"=>"close");

/* ====================================================================================== */
/* == Advance Ends ====================================================================== */
/* ====================================================================================== */		

/* ====================================================================================== */
/* == Translation Panel ====================================================================== */
/* ====================================================================================== */

$options[]   = array( 
		           "name" => "Translation",
	  	           "type" => "section"
		          );
$options[]   = array( 
		          "name" => $themename." Options",
		    	  "type" => "information",
		      	  "description" => "Translation manunal for the theme.."
		  );
		  				  
$options[]   = array( "type" => "open");

		  


$options[]   = array(
						"name" => "Translation File", 
						"type"=>"include", 
						"std"=> HPATH."/option_panel/adv_mods/translate.php"
					  );




$options[]   = array("type"=>"close");