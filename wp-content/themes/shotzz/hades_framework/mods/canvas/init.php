<?php

/* ======================================================================= */
/* == Titan Slider ======================================================= */
/* ======================================================================= */

/* 

Author - WPTitans
Code Name - Excaliber
Version - 1.0
Description - Canvas based for creating styles on current theme works on top of Hades framework.

*/

if(!defined('HPATH'))
die(' The File cannot be accessed directly ');

if(!class_exists('Canvas')) {

class Canvas extends Loki {
	
	private $registerPostVariables;
	
	function __construct () { parent::__construct('Canvas'); $this->registerPostVariables = array();  }
	
	function createColorPickerbox($label,$name,$value,$multiple=false,$class='') {
		
		$this->registerPostVariables[] = $name;
		
		if($multiple)
		$name = $name."[]";
		?>
		 <div class="hades_input  clearfix <?php echo $class ?>">
            <label for=""> <?php echo $label ?></label> 
            <div class="colorSelector" ><div style="background-color:#<?php echo  $value; ?>"></div></div>
            <input type="text" value="<?php echo $value; ?>" name="<?php echo $name; ?>" class="<?php echo $name; ?> colorpickerField1" />
      </div>
		
		<?php
		
		}
	
	function createTextbox($label,$name,$value,$multiple=false,$class='') {
		$this->registerPostVariables[] = $name;
		if($multiple)
		$name = $name."[]";
		?>
		 <div class="hades_input  clearfix <?php echo $class ?>">
            <label for=""> <?php echo $label ?></label> 
          
            <input type="text" value="<?php echo $value; ?>" name="<?php echo $name; ?>" class="<?php echo $name; ?> " />
      </div>
		
		<?php
		
		}
		
	function createUploadbox($label,$name,$value,$multiple=false,$class='') {
		$this->registerPostVariables[] = $name;
		if($multiple)
		$name = $name."[]";
		?>
		 <div class="hades_input  clearfix <?php echo $class ?>">
            <label for=""> <?php echo $label ?></label> 
          
            <input type="text" value="<?php echo $value; ?>" name="<?php echo $name; ?>" class="<?php echo $name; ?> " />
            <a href="#" class="button custom_upload_image_button"> Upload </a>
      </div>
		
		<?php
		
		}	
	
	
	function createSelect($label,$name,$value,$options,$multiple=false)
    {
		$options = explode(",",$options);
		$this->registerPostVariables[] = $name;
		
		if($multiple)
		$name = $name."[]";
	?>
    <div class="hades_input clearfix <?php echo $class ?>">
      <label for=""><?php echo $label ?></label>
      <div class="select-wrapper clearfix">
          <select name="<?php echo $name ?>" id="">
          <?php foreach ($options as $option) { ?>
          
          <option <?php 
          if ($value == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?>
          </select>
      </div>
     
      </div>
      <?php
     
	  }		
	
	function manager_admin_init(){	
	global $wpdb , $table_db_name;
	 
	$stacks = "";
	
	if(isset($_POST['action']) &&  $_GET['page']=="CANV" ) :
	
	$stacks = array();
	
	$i = 1;
	  foreach ( $_POST['styles'] as $key => $value ) :
	  
	       
	    $title = $_POST['style_title'][$key];
		$inherit = $_POST['inherit'][$key];
		$bg_color = $_POST['bg_color'][$key];
		$bg_image = $_POST['bg_image'][$key];
		$bg_position = $_POST['bg_position'][$key];
		$top_border_color = $_POST['top_border_color'][$key];
		$top_border_size = $_POST['top_border_size'][$key];
		
		$stage_bg_color = $_POST['stage_bg_color'][$key];
		$stage_bg_image = $_POST['stage_bg_image'][$key];
		$stage_bg_position = $_POST['stage_bg_position'][$key];
		$stage_top_border_color = $_POST['stage_top_border_color'][$key];
		$stage_top_border_size = $_POST['stage_top_border_size'][$key];
		
		$body_color = $_POST['body_color'][$key];
		$sidebar_color =  $_POST['sidebar_color'][$key];
		$footer_color = $_POST['footer_color'][$key];
		$link_color = $_POST['link_color'][$key];
		$link_hover_color = $_POST['link_hover_color'][$key];
		$h1_color = $_POST['h1_color'][$key];
		$h2_color = $_POST['h2_color'][$key];
		$h3_color = $_POST['h3_color'][$key];
		$h4_color = $_POST['h4_color'][$key];
		$h5_color = $_POST['h5_color'][$key];
		$h6_color = $_POST['h6_color'][$key];
		$sidebar_title_color = $_POST['sidebar_title_color'][$key];
		$footer_title_color = $_POST['footer_title_color'][$key];
		
		
		$box_bg_color = $_POST['box_bg_color'][$key];
		$box_bg_image = $_POST['box_bg_image'][$key];
		$box_bg_position = $_POST['box_bg_position'][$key];
		
			
		$menubar_bg_color = $_POST['menubar_bg_color'][$key];
		$menubar_bg_image = $_POST['menubar_bg_image'][$key];
		$menubar_bg_position = $_POST['menubar_bg_position'][$key];
		$menubar_border_color = $_POST['menubar_border_color'][$key];
		$menubar_border_size = $_POST['menubar_border_size'][$key];
		$menubar_color = $_POST['menubar_color'][$key];
		$menubar_hover_color = $_POST['menubar_hover_color'][$key];
		
		
		$sidebarbar_bg_color = $_POST['sidebarbar_bg_color'][$key];
		$sidebarbar_bg_image = $_POST['sidebarbar_bg_image'][$key];
		$sidebarbar_bg_position = $_POST['sidebarbar_bg_position'][$key];
		$sidebarbar_border_color = $_POST['sidebarbar_border_color'][$key];
		$sidebarbar_border_size = $_POST['sidebarbar_border_size'][$key];   
		
		$footer_bg_color = $_POST['footer_bg_color'][$key];
		$footer_bg_image = $_POST['footer_bg_image'][$key];
		$footer_bg_position = $_POST['footer_bg_position'][$key];
		$footer_border_color = $_POST['footer_border_color'][$key];
		$footer_border_size = $_POST['footer_border_size'][$key];   
		
		//$bg_color = $_POST['bg_color'][$key];
		    
	    $stacks[$title] = array(
		
		"title" => $title,
		"inherit" => $inherit,
		"bg_color" => $bg_color ,
		"bg_image" => $bg_image,
		"bg_position" => $bg_position,
		"top_border_color" => $top_border_color,
		"top_border_size" => $top_border_size,
		 
		"stage_bg_color" => $stage_bg_color ,
		"stage_bg_image" => $stage_bg_image,
		"stage_bg_position" => $stage_bg_position,
		"stage_top_border_color" => $stage_top_border_color,
		"stage_top_border_size" => $stage_top_border_size, 
		
		"body_color" => $body_color,
		"footer_color" => $footer_color,
		"sidebar_color" => $sidebar_color,
		"link_color" => $link_color,
		"link_hover_color" => $link_hover_color,
		"h1_color" => $h1_color,
		"h2_color" => $h2_color,
		"h3_color" => $h3_color,
		"h4_color" => $h4_color,
		"h5_color" => $h5_color,
		"h6_color" => $h6_color,
		"sidebar_title_color"  => $sidebar_title_color,
		"footer_title_color"  => $footer_title_color,
		
		"box_bg_color"  => $box_bg_color,
		"box_bg_image"  => $box_bg_image,
		"box_bg_position" => $box_bg_position,
		
		
		"menubar_bg_color" => $menubar_bg_color,
		"menubar_bg_image"  => $menubar_bg_image,
		"menubar_bg_position"  => $menubar_bg_position,
		"menubar_border_size"  => $menubar_border_size,
		"menubar_border_color"  => $menubar_border_color,
		"menubar_color" => $menubar_color,
		"menubar_hover_color" => $menubar_hover_color,
		
		"sidebarbar_bg_color"  => $sidebarbar_bg_color,
		"sidebarbar_bg_image"  => $sidebarbar_bg_image,
		"sidebarbar_bg_position"  => $sidebarbar_bg_position,
		"sidebarbar_border_color"  => $sidebarbar_border_color,
		"sidebarbar_border_size" => $sidebarbar_border_size,
		
		"footer_bg_color"  => $footer_bg_color,
		"footer_bg_image"  => $footer_bg_image,
		"footer_bg_position"  => $footer_bg_position,
		"footer_border_color"  => $footer_border_color,
		"footer_border_size" => $footer_border_size
		);
	  
	  
	  
	  endforeach;
	
	
	update_option(SN."_canvas",serialize($stacks));
	header("Location: admin.php?page=CANV&saved=true");
	die; 
	
	endif;
	
	 
	 wp_enqueue_script('thickbox');
	 wp_enqueue_style('thickbox');
	 wp_enqueue_script('jquery-ui-sortable');
	 
	 wp_enqueue_script("admin-colorpicker",HURL."/js/colorpicker.js",array('jquery'),"1.0");
	 wp_enqueue_style("colorpicker-style", HURL."/css/colorpicker.css", false, "1.0", "all");
	 
	 }	
	function manager_admin_wrap(){	
	global $wpdb , $table_db_name;
	
	$cs = array();
    
	$stylePath =  TEMPLATEPATH."/sprites/stylesheets/plain";
    $styleUrl = URL."/sprites/stylesheets/plain";
	
	$get_styles = scandir($stylePath);
	
	 $i=0; foreach($get_styles as $styles) : if($i>1) {
		$cs[] = $styles; 
	 }  $i++; endforeach;
	
	
	
	?>
	
    <script type="text/javascript">
	
	jQuery(function($){
		
		 $('.colorpickerField1').ColorPicker({

	onSubmit: function(hsb, hex, rgb, el) {

		$(el).val(hex);

		$(el).ColorPickerHide();

	},

	onBeforeShow: function () {
       temp = this;
		$(this).ColorPickerSetColor(this.value);

	},
	
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(300);
		return false;
	},
	onChange: function (hsb, hex, rgb,el) {
		$(temp).val(hex);
		$(temp).parents('.hades_input').find('.colorSelector>div').css('backgroundColor', '#' + hex);
	}

})

.bind('keyup', function(){

	$(this).ColorPickerSetColor(this.value);

});


	    var slider_count ,temp,slide =  $(".slide-list li.hide").first().clone().removeClass('hide');
		var slider   =  $(".custom-list li.hide").first().clone().removeClass('hide');
		$(".custom-list li.hide").first().remove();
	    
		$("#create_slider").live("click",function(e){ 
		temp = slider.clone();
		var sn = $('#custom_post_name').val();
		
		if(sn=="") { $('#custom_post_name').addClass("error"); return; } else  $('#custom_post_name').removeClass("error");
	   $('#custom_post_name').val('');
		
		temp.find(".heading span").html(sn);
		temp.find('.style_title').val(sn);
		
		temp.find('.colorpickerField1').ColorPicker({

	onSubmit: function(hsb, hex, rgb, el) {

		$(el).val(hex);

		$(el).ColorPickerHide();

	},

	onBeforeShow: function () {
       temp = this;
		$(this).ColorPickerSetColor(this.value);

	},
	
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(300);
		return false;
	},
	onChange: function (hsb, hex, rgb,el) {
		$(temp).val(hex);
		$(temp).parents('.hades_input').find('.colorSelector>div').css('backgroundColor', '#' + hex);
	}

})

.bind('keyup', function(){

	$(this).ColorPickerSetColor(this.value);

});
				
		$(".custom-list").append(temp);
		
		});
		$('.style_title').live('focusout',function(){ if($(this).val()=="") return; $(this).parents("li").find(".heading span").html($(this).val()); });
		
	    
		$(".custom-list .heading").live('click',function(){   $(this).next().slideToggle('normal'); });
        
		});
	
	</script>
	
    <?php if( isset($_GET['saved'])) echo "<div class='success_message show' style='opacity:1;visibility:visible'><p>Canvas Saved </p> </div>"; ?>
    <?php if( isset($_GET['deleted'])) echo "<div class='success_message show'><p>Canvas Deleted </p> </div>"; ?>
    
    <div class="hades_wrap">
     <form method="post" enctype="multipart/form-data" >
      <div class="hades-panel clearfix slidermanager">
       
       <label for="custom_post_name"> Enter Canvas Name </label>
       <input type="text" value="" id="custom_post_name" > 
        
       <a href="#" class="button" id="create_slider" > Create </a> 
        <input type="submit" value="Save" class="button-save" name="action" />
        
      </div>
      <div class="hades-panel-body">
      
      
      <ul class='custom-list'>
      <!-- =============================================================================== -->
      <!-- == Clonable List Item ========================================================= -->
      <!-- =============================================================================== -->
      
      <li class='clearfix hide'> 
      
        <div class="heading clearfix">
          <h4>Canvas <span></span> <a href="#" class="delete button"> Delete </a></h4>
        </div>
        <div class="slide-body">
      
       <h2> General Stylings </h2>
        <div class="hades_input clearfix">
        <label for=""> Canvas Title </label><input type="text" value="" name="style_title[]" class="style_title" >
        </div>
    
        <?php 
		$this->createSelect('Inherit Styles ','inherit','WhiteSmoke',implode(",",$cs),true);
		$this->createColorPickerbox('Background Color','bg_color','',true);
		$this->createUploadbox('Background Image','bg_image','',true);
		$this->createSelect('Background Position','bg_position','top left','top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		$this->createColorPickerbox('Top Page Border Color','top_border_color','',true);
		$this->createTextbox('Size','top_border_size','0px',true);
		?>
        
        <h2> Stage Stylings  </h2>
        
         <?php 
		
		$this->createColorPickerbox('Stage Background Color','stage_bg_color','',true);
		$this->createUploadbox('Stage Background Image','stage_bg_image','',true);
		$this->createSelect('Stage Background Position','stage_bg_position','top left','top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		$this->createColorPickerbox('Stage Top Page Border Color','stage_top_border_color','',true);
		$this->createTextbox('Stage Top Page Border Size','stage_top_border_size','0px',true);
		?>
        
        <h2>Typography Stylings</h2>
    
      <?php  
		$this->createColorPickerbox('Body Font Color','body_color','',true);
		$this->createColorPickerbox('Footer Font Color','footer_color','',true);
		$this->createColorPickerbox('Sidebar Font Color','sidebar_color','',true);
		$this->createColorPickerbox('Link Color','link_color','',true);
		$this->createColorPickerbox('Link Hover Color','link_hover_color','',true);
		
		$this->createColorPickerbox('H1 Color','h1_color','',true);
		$this->createColorPickerbox('H2 Color','h2_color','',true);
		$this->createColorPickerbox('H3 Color','h3_color','',true);
		$this->createColorPickerbox('H4 Color','h4_color','',true);
		$this->createColorPickerbox('H5 Color','h5_color','',true);
		$this->createColorPickerbox('H6 Color','h6_color','',true);
		
		$this->createColorPickerbox('Sidebar Title Color','sidebar_title_color','',true);
		$this->createColorPickerbox('Footer Title Color','footer_title_color','',true);
		?>
        
       
        <h2>Box(container) Stylings</h2>
         
         <?php 
		 
		 $this->createColorPickerbox('Box Background Color','box_bg_color','',true);
		 $this->createUploadbox('Box Background Image','box_bg_image','',true);
		 $this->createSelect('Box Image Background Position','box_bg_position','top left','top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		?>
        
       
        <h2>Main Menu Stylings</h2>
         
         <?php 
		 
		 $this->createColorPickerbox('Main Menu Background Color','menubar_bg_color','',true);
		 $this->createUploadbox('Main Menu Background Image','menubar_bg_image','',true);
		 $this->createSelect('Main Menu Image Background Position','menubar_bg_position','top left','top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		 $this->createColorPickerbox('Main Menu Bottom Border Color','menubar_border_color','',true);
		 $this->createTextbox('Main Menu Bottom Border Size','menubar_border_size','0px',true);
		 
		 $this->createColorPickerbox('Main Menu Link Color','menubar_color','',true);
		 $this->createColorPickerbox('Main Menu Link Hover Color','menubar_hover_color','',true);
		?>
       
        <h2>Sidebar Stylings</h2>
       
        <?php 
		
		 $this->createColorPickerbox('Sidebar Widget Background Color','sidebarbar_bg_color','',true);
		 $this->createUploadbox('Sidebar Background Image','sidebarbar_bg_image','',true);
		 $this->createSelect('Sidebar Image Background Position','sidebarbar_bg_position','top left','top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		 
		 $this->createColorPickerbox('Sidebar Widget Border Color','sidebarbar_border_color','',true);
		$this->createTextbox('Sidebar Border Size','sidebarbar_border_size','0px',true);
		?>
        
        
         <h2>Footer Stylings</h2>
       
        <?php 
		
		 $this->createColorPickerbox('Footer Widget Background Color','footer_bg_color','',true);
		 $this->createUploadbox('Footer Background Image','footer_bg_image','',true);
		 $this->createSelect('Footer Image Background Position','footer_bg_position','top left','top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		 
		 $this->createColorPickerbox('Footer Widget Border Color','footer_border_color','',true);
		$this->createTextbox('Footer Border Size','footer_border_size','0px',true);
		?>
        
      </div>
       <input type="hidden" name="styles[]" value="true" />
         </li>
         
         
         <?php 
		 
		 $styles = unserialize(get_option(SN."_canvas")); 
		 if(!is_array($styles)) $styles = array();
         foreach($styles as $canvas) : 
		 ?>
         
          <li class='clearfix '> 
      
      
        <div class="heading clearfix">
          <h4>Canvas <span><?php echo $canvas["title"] ?></span> <a href="#" class="delete button"> Delete </a></h4>
        </div>
        <div class="slide-body hide">
      
       <h2> General Stylings </h2>
        <div class="hades_input clearfix">
        <label for=""> Canvas Title </label><input type="text" value="<?php echo $canvas["title"] ?>" name="style_title[]" class="style_title"  >
        </div>
    
        <?php 
		$this->createSelect('Inherit Styles ','inherit',$canvas["inherit"],"none,".implode(",",$cs),true);
		$this->createColorPickerbox('Background Color','bg_color',$canvas["bg_color"],true);
		$this->createUploadbox('Background Image','bg_image',$canvas["bg_image"],true);
		$this->createSelect('Background Position','bg_position',$canvas['bg_position'] ,'top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		$this->createColorPickerbox('Top Page Border Color','top_border_color',$canvas["top_border_color"],true);
		$this->createTextbox('Size','top_border_size',$canvas["top_border_size"],true);
		?>
        
         <h2> Stage Stylings  </h2>
        
         <?php 
		
		$this->createColorPickerbox('Stage Background Color','stage_bg_color',$canvas["stage_bg_color"],true);
		$this->createUploadbox('Stage Background Image','stage_bg_image',$canvas["stage_bg_image"],true);
		$this->createSelect('Stage Background Position','stage_bg_position',$canvas["stage_bg_position"],'top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		$this->createColorPickerbox('Stage Top Page Border Color','stage_top_border_color',$canvas["stage_top_border_color"],true);
		$this->createTextbox('Stage Top Page Border Size','stage_top_border_size',$canvas["stage_top_border_size"],true);
		?>
        
        <h2>Typography Stylings</h2>
    
      <?php  
		$this->createColorPickerbox('Body Font Color','body_color',$canvas["body_color"],true);
		$this->createColorPickerbox('Footer Font Color','footer_color',$canvas["footer_color"],true);
		$this->createColorPickerbox('Sidebar Font Color','sidebar_color',$canvas["sidebar_color"],true);
		$this->createColorPickerbox('Link Color','link_color',$canvas["link_color"],true);
		$this->createColorPickerbox('Link Hover Color','link_hover_color',$canvas["link_hover_color"],true);
		
		$this->createColorPickerbox('H1 Color','h1_color',$canvas["h1_color"],true);
		$this->createColorPickerbox('H2 Color','h2_color',$canvas["h2_color"],true);
		$this->createColorPickerbox('H3 Color','h3_color',$canvas["h3_color"],true);
		$this->createColorPickerbox('H4 Color','h4_color',$canvas["h4_color"],true);
		$this->createColorPickerbox('H5 Color','h5_color',$canvas["h5_color"],true);
		$this->createColorPickerbox('H6 Color','h6_color',$canvas["h6_color"],true);
		
		$this->createColorPickerbox('Sidebar Title Color','sidebar_title_color',$canvas["sidebar_title_color"],true);
		$this->createColorPickerbox('Footer Title Color','footer_title_color',$canvas["footer_title_color"],true);
		?>
        
       
        <h2>Box(container) Stylings</h2>
         
         <?php 
		 
		 $this->createColorPickerbox('Box Background Color','box_bg_color',$canvas["box_bg_color"],true);
		 $this->createUploadbox('Box Background Image','box_bg_image',$canvas["box_bg_image"],true);
		 $this->createSelect('Box Image Background Position','box_bg_position',$canvas["box_bg_position"],'top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		?>
        
       
        <h2>Main Menu Stylings</h2>
         
         <?php 
		 
		 $this->createColorPickerbox('Main Menu Background Color','menubar_bg_color',$canvas["menubar_bg_color"],true);
		 $this->createUploadbox('Main Menu Background Image','menubar_bg_image',$canvas["menubar_bg_image"],true);
		 $this->createSelect('Main Menu Image Background Position','menubar_bg_position',$canvas["menubar_bg_position"],'top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		 $this->createColorPickerbox('Main Menu Bottom Border Color','menubar_border_color',$canvas["menubar_border_color"],true);
		 $this->createTextbox('Main Menu Bottom Border Size','menubar_border_size',$canvas["menubar_border_size"],true);
		 
		 $this->createColorPickerbox('Main Menu Link Color','menubar_color',$canvas["menubar_color"],true);
		 $this->createColorPickerbox('Main Menu Link Hover Color','menubar_hover_color',$canvas["menubar_hover_color"],true);
		?>
       
        <h2>Sidebar Stylings</h2>
       
        <?php 
		
		 $this->createColorPickerbox('Sidebar Widget Background Color','sidebarbar_bg_color',$canvas["sidebarbar_bg_color"],true);
		 $this->createUploadbox('Sidebar Background Image','sidebarbar_bg_image',$canvas["sidebarbar_bg_image"],true);
		 $this->createSelect('Sidebar Image Background Position','sidebarbar_bg_position',$canvas["sidebarbar_bg_position"],'top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		 
		 $this->createColorPickerbox('Sidebar Widget Border Color','sidebarbar_border_color',$canvas["sidebarbar_border_color"],true);
		$this->createTextbox('Sidebar Border Size','sidebarbar_border_size',$canvas["sidebarbar_border_size"],true);
		?>
        
         <h2>Footer Stylings</h2>
       
        <?php 
		
		 $this->createColorPickerbox('Footer Background Color','footer_bg_color',$canvas["footer_bg_color"],true);
		 $this->createUploadbox('Footer Background Image','footer_bg_image',$canvas["footer_bg_image"],true);
		 $this->createSelect('Footer Image Background Position','footer_bg_position',$canvas["footer_bg_position"],'top left,top right,top center,bottom left,bottom right,bottom center,center center ',true);
		 
		 $this->createColorPickerbox('Footer Widget Border Color','footer_border_color',$canvas["footer_border_color"],true);
		$this->createTextbox('Footer Widget Border Size','footer_border_size',$canvas["footer_border_size"],true);
		?>
        
        
      </div>
       <input type="hidden" name="styles[]" value="true" />
       
     
         </li>
         <?php  endforeach; ?>
         
         
       </ul>
      </div>
       </form>
    </div>  
	  
      
	  
	  <?php
	

	
	 }	 
	 
	  public function initDynamicPosts()
	  {
		global $wpdb , $table_db_name;
		$dynamic_posts = $wpdb->get_results("SELECT * FROM $table_db_name ",ARRAY_A);
		// == Dyanmic post types =============================


    if(!is_array($dynamic_posts)) $dynamic_posts = array();
 
	  foreach($dynamic_posts as $post)
	 {
		 $c = unserialize($post['custom_post']);
		 $title = $post["title"];
		 $o = unserialize($post['options']);
		
		  $custom_obj[]  = new CustomPosts($title,$c,$o["taxonomy"]);
	 }
		
		} 
	
	
	
	}

}

$canvas = new Canvas();

