<?php 


// == Set all setings =====================

function setData()
{
	setMenus();
	setDemoOptions();
	setWidgets();
	setMedia();
}

// == Set media ==========================================

function setMedia()
{
	$p_slides =   array ( 
	   "src" => URL."/sprites/i/default.jpg",
	   "link" => "",  "description" => "" , "type" => "upload" , 
	   "title" => "" );
	 
	 $path = __FILE__;
     $pathwp = explode( 'wp-content', $path );
     $wp_url = $pathwp[0]."wp-content/uploads/default.jpg";

	 $cstatus =   copy( TEMPLATEPATH."/sprites/i/default.jpg",  $wp_url  );
	 
	
	 $wp_query = new WP_Query("post_type=portfolio&posts_per_page=-1");
	 
	 while ( $wp_query->have_posts() ) : $wp_query->the_post();
	 
	 $no = rand(2,7);
	 $portfolio_slides = array();
	 for($i=0;$i<$no;$i++)
     $portfolio_slides[] = $p_slides;
	 
	 $id =  get_the_ID();
	 update_post_meta($id,"gallery_items",$portfolio_slides);
	 
	 if($cstatus) {
	 
	 $filename = "default.jpg";
	 $wp_filetype = wp_check_filetype(basename($filename), null );
     $attachment = array(
     'post_mime_type' => $wp_filetype['type'],
     'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
     'post_content' => '',
     'post_status' => 'inherit'
      );
      $attach_id = wp_insert_attachment( $attachment, $filename, $id );
      require_once(ABSPATH . 'wp-admin/includes/image.php');
      $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
      wp_update_attachment_metadata( $attach_id, $attach_data );
      set_post_thumbnail($id,   $attach_id );
	 }
	 
	 
	endwhile;
	
	 $wp_query = new WP_Query("post_type=gallery&posts_per_page=-1");
	 
	 while ( $wp_query->have_posts() ) : $wp_query->the_post();
	 
	 $no = rand(2,7);
	 $portfolio_slides = array();
	 for($i=0;$i<$no;$i++)
     $portfolio_slides[] = $p_slides;
	 
	 $id =  get_the_ID();
	 update_post_meta($id,"gallery_items",$portfolio_slides);
	 
	 if($cstatus) {
	 
	 $filename = "default.jpg";
	 $wp_filetype = wp_check_filetype(basename($filename), null );
     $attachment = array(
     'post_mime_type' => $wp_filetype['type'],
     'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
     'post_content' => '',
     'post_status' => 'inherit'
      );
      $attach_id = wp_insert_attachment( $attachment, $filename, $id );
      require_once(ABSPATH . 'wp-admin/includes/image.php');
      $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
      wp_update_attachment_metadata( $attach_id, $attach_data );
      set_post_thumbnail($id,   $attach_id );
	 }
	 
	 
	endwhile;
	
	 $wp_query = new WP_Query("post_type=post&posts_per_page=-1");
	 
	 while ( $wp_query->have_posts() ) : $wp_query->the_post();
	 
	 $id =  get_the_ID();
	 
	 if($cstatus) {
	 
	 $filename = "default.jpg";
	 $wp_filetype = wp_check_filetype(basename($filename), null );
     $attachment = array(
     'post_mime_type' => $wp_filetype['type'],
     'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
     'post_content' => '',
     'post_status' => 'inherit'
      );
      $attach_id = wp_insert_attachment( $attachment, $filename, $id );
      require_once(ABSPATH . 'wp-admin/includes/image.php');
      $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
      wp_update_attachment_metadata( $attach_id, $attach_data );
      set_post_thumbnail($id,   $attach_id );
	 }
	 
	 
	endwhile;
	
	
	 $wp_query = new WP_Query("post_type=events&posts_per_page=-1");
	 
	 while ( $wp_query->have_posts() ) : $wp_query->the_post();
	 
	  $no = rand(2,7);
	 $portfolio_slides = array();
	 for($i=0;$i<$no;$i++)
     $portfolio_slides[] = $p_slides;
	 
	 $id =  get_the_ID();
	 update_post_meta($id,"gallery_items",$portfolio_slides);
	 
	 if($cstatus) {
	 
	 $filename = "default.jpg";
	 $wp_filetype = wp_check_filetype(basename($filename), null );
     $attachment = array(
     'post_mime_type' => $wp_filetype['type'],
     'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
     'post_content' => '',
     'post_status' => 'inherit'
      );
      $attach_id = wp_insert_attachment( $attachment, $filename, $id );
      require_once(ABSPATH . 'wp-admin/includes/image.php');
      $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
      wp_update_attachment_metadata( $attach_id, $attach_data );
      set_post_thumbnail($id,   $attach_id );
	 }
	 
	 
	endwhile;
}

// == Set Widgets =========================================
function setWidgets()
{

$sidebars = get_option("sidebars_widgets");

$sidebars["sidebar-1"] = array("search-2", "tag_cloud-2","recent-comments-2");

$sidebars["sidebar-3"] = array ("customboxwidget-3");
$sidebars["sidebar-2"] = array ("customboxwidget-4");

$sidebars["sidebar-4"] = array ( "twitter_widget-2" );
$sidebars["sidebar-5"] = array (  "categories-2");
$sidebars["sidebar-6"] = array (  "superpost-2");
$sidebars["sidebar-7"] = array (  "links-2");
      
update_option("sidebars_widgets",$sidebars);


$feature = get_option("widget_superpost");	
$feature[2] =  array
        (
            "count" => 2,
            "title" => "Recent work",
            "post_type" => "portfolio",
			"post_filter" => "recent",
			"excerpt" => "90"
			
         );
$feature[3] =  array
        (
            "count" => 2,
            "title" => "Latest Events",
            "post_type" => "events",
			"post_filter" => "recent",
			"excerpt" => "90"
         );		 
$feature["_multiwidget"] =   1 ;
update_option("widget_superpost",$feature);

$search = get_option("widget_search");	
$search[2] =array("title" => "");
$search["_multiwidget"] =   1 ;
update_option("widget_search",$search);



$twitter = get_option("widget_twitter_widget");	
$twitter[2] =array("title" => "Latest from Twitter" , "username" => "WPTitan", "tweet_count" => 3);
$twitter["_multiwidget"] =   1 ;
update_option("widget_twitter_widget",$twitter);

$categories = get_option("widget_categories");	
$categories[2] =array("title" => "Categories" , "count" => "1", "hierarchical" => "0" , "dropdown" =>"0");
$categories["_multiwidget"] =   1 ;
update_option("widget_categories",$categories);		
		
	
$links = get_option("widget_links");	
$links[2] = array( "images" => 1 , "name" => 1 , "description" =>  0 ,"rating" => 0, "category" => 0);
$links["_multiwidget"] =   1 ;
update_option("widget_links",$links);		

$tags = get_option("widget_tag_cloud");	
$tags[2] = array( "title" => "Tags " , "taxonomy" => "post_tag");
$tags["_multiwidget"] =   1 ;
update_option("widget_tag_cloud",$tags);	



$custom_box = get_option("widget_customboxwidget");	
$custom_box[3] =array(
	"link" => "#",
	"description" => "Duis vitae pharetra lorem. Etiam quis mauris felis. Quisque id magnat libero aliquet iaculis. Suspendisse mollis sodales sapien. Sed eleifend enim libero.
Vestibulum accumsan tristique massa, ac aliquam augue volutpat eget. Donec justo eros, gravida tristique ornare sit amet, rhoncus a leo.",
	"title" => "Vestibulum accumsan tristique massa, ac aliquam augue volutpat eget. ",
	"intro_image_link" => "",
	"label" => "Continue"
	);

$custom_box[4] =array(
	"link" => "#",
	"description" => "Duis vitae pharetra lorem. Etiam quis mauris felis. Quisque id magnat libero aliquet iaculis. Suspendisse mollis sodales sapien. Sed eleifend enim libero.
Vestibulum accumsan tristique massa, ac aliquam augue volutpat eget. Donec justo eros, gravida tristique ornare sit amet, rhoncus a leo.",
	"title" => "Vestibulum accumsan tristique massa, ac aliquam augue volutpat eget. Donec justo eros, gravida tristique ornare sit amet, rhoncus a leo. Nulla facilisi. Integer rutrum nisl eros. Proin eget turpis nec magna porta blandit sit amet vel dolor.",
	"intro_image_link" => "",
	"label" => "Continue"
	);
 			 
			  			 
$custom_box["_multiwidget"] =   1 ;
update_option("widget_customboxwidget",$custom_box);


}
function setMenus()
{
	$gmes = "Menus ";
	global $wpdb;
    $table_db_name = $wpdb->prefix . "terms";
    $rows = $wpdb->get_results("SELECT * FROM $table_db_name where  name='Main Menu' OR name='Footer Menu'",ARRAY_A);
    $menu_ids = array();
	foreach($rows as $row)
	$menu_ids[$row["name"]] = $row["term_id"] ; 

	set_theme_mod( 'nav_menu_locations', array_map( 'absint', array(   'primary_nav' =>$menu_ids['Main Menu'] ,'footer_nav' => $menu_ids['Footer Menu']) ) );
	
	$items = wp_get_nav_menu_items( $menu_ids['Main Menu']); 
	
	$i = 0;
	foreach($items as $item)
	{
		if($item->title=="Home")
		{
			$item->url = home_url();
		}
		if($item->title=="Mega Menu")
		{
			update_post_meta($item->ID,"menu-item-megamenu-".$item->ID,"on");
			update_post_meta($item->ID,"menu-item-megamenu-layout-".$item->ID,"column");
		}
		if($item->title=="Powered by WPTitans")
		{
			update_post_meta($item->ID,"menu-item-textbox-".$item->ID,"<p>Ut ut egestas mi. Suspendisse scelerisque ante mattis est condimentum at hendrerit massa volutpat. Morbi dapibus feugiat ipsum, a mattis velit pharetra in.</p><p>
Aliquam mattis egestas sapien eu eleifend. Maecenas condimentum euismod libero, in egestas ipsum venenatis pharetra.</p>");
			update_post_meta($item->ID,"menu-item-enable-textbox-".$item->ID,"on");
		
		}
		if($item->title=="About Yvora")
		{
			update_post_meta($item->ID,"menu-item-textbox-".$item->ID,"<p>Ut ut egestas mi. Suspendisse scelerisque ante mattis est condimentum at hendrerit massa volutpat. Morbi dapibus feugiat ipsum, a mattis velit pharetra in.</p><p>
Aliquam mattis egestas sapien eu eleifend. Maecenas condimentum euismod libero, in egestas ipsum venenatis pharetra.</p>");
			update_post_meta($item->ID,"menu-item-enable-textbox-".$item->ID,"on");
		
		}
		
	}
	
}

// == Set Media Content ==============================================

function setMediaContent() {

 $gmes = "Media Items ( portfolios and galleries ) ";
	 
	  $p_slides =   array ( 
	   "src" => URL."sprites/i/demo.png",
	   "link" => "",  "description" => "" , "type" => "upload" , 
	   "title" => "" );
	 
	  $cstatus =   copy( TEMPLATEPATH."/images/demo.png",  $wp_url  );
	 
	  $wp_query = new WP_Query("post_type=portfolio&posts_per_page=-1");
	 
	  while ( $wp_query->have_posts() ) : $wp_query->the_post();
	 
		   $no = rand(2,7);
		   $portfolio_slides = array();
		   for($i=0;$i<$no;$i++)
		   $portfolio_slides[] = $p_slides;
	 
		   $id =  get_the_ID();
		   update_post_meta($id,"gallery_items",$portfolio_slides);
	 
		   if($cstatus) {
		   
		   $filename = "demo.png";
		   $wp_filetype = wp_check_filetype(basename($filename), null );
		   $attachment = array(
		   'post_mime_type' => $wp_filetype['type'],
		   'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
		   'post_content' => '',
		   'post_status' => 'inherit'
			);
			$attach_id = wp_insert_attachment( $attachment, $filename, $id );
			
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			set_post_thumbnail($id,   $attach_id );
		   }
	 
	 
	endwhile;
	
	
}

// == Enable Demo Content ===========================

function setDemoContent()
{
	if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);
	require_once ABSPATH . 'wp-admin/includes/import.php';
    $importer_error = false;
	
	if ( !class_exists( 'WP_Importer' ) ) {
	$class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
		if ( file_exists( $class_wp_importer ) )
		{
			require_once($class_wp_importer);
		}
		else
		{
			$importer_error = true;
		}
    }
	
	if ( !class_exists( 'WP_Import' ) ) {
	  $class_wp_import = HPATH . '/mods/odin/importer/wordpress-importer.php';
	  if ( file_exists( $class_wp_import ) )
	  require_once($class_wp_import);
	  else
	  $importerError = true;
	  
    }

	  if($importer_error)
	  {
		  die("Error in import :(");
	  }
	  else
	  {
		  if ( class_exists( 'WP_Import' )) 
		  {
			  include_once('importer/odin-import-class.php');
		  }
		  
		  
		  if(!is_file(HPATH."/mods/odin/dummy.xml"))
		  {
			  echo "The XML file containing the dummy content is not available or could not be read in <pre>".HPATH."</pre><br/> You might want to try to set the file permission to chmod 777.<br/>If this doesn't work please use the wordpress importer and import the XML file from hades_framework -> mods -> odin folder , dummy.xml manually <a href='/wp-admin/import.php'>here.</a>";
		  }
		  else
		  {
	  
			  $wp_import = new odin_wp_import();
			  $wp_import->fetch_attachments = true;
			  $wp_import->import(HPATH."/mods/odin/dummy.xml");
			  $wp_import->saveOptions();
			
		  }
	  }
   
   
    
}


function setDemoOptions() {

$theme_options = " W3sib3B0aW9uX25hbWUiOiJTSE9fYWN0aXZlX3NpZGViYXJzIiwib3B0aW9uX3ZhbHVlIjoiczo4NjpcImE6Mzp7aTowO3M6MTM6XCJSaWdodCBTaWRlYmFyXCI7aToxO3M6MTU6XCJSaWdodCBTaWRlYmFyIDJcIjtpOjI7czoxNjpcIlJpZ2h0IFNpZGViYXIgMSBcIjt9XCI7In0seyJvcHRpb25fbmFtZSI6IlNIT19hZG1pbl9sb2dvIiwib3B0aW9uX3ZhbHVlIjoieW91ciB1cGxvYWQgcGF0aCJ9LHsib3B0aW9uX25hbWUiOiJTSE9fYWRtaW5fbG9nb19oZWlnaHQiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2FkbWluX2xvZ29fd2lkdGgiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2F1dGhvcl9iaW8iLCJvcHRpb25fdmFsdWUiOiJ0cnVlIn0seyJvcHRpb25fbmFtZSI6IlNIT19iZF9zaXplIiwib3B0aW9uX3ZhbHVlIjoiMTEifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2Jsb2dfbGF5b3V0Iiwib3B0aW9uX3ZhbHVlIjoiTGlzdCBMYXlvdXQifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2JsdXJiX2J1dHRvbl9sYWJlbCIsIm9wdGlvbl92YWx1ZSI6IkdldCBpbiBUb3VjaCJ9LHsib3B0aW9uX25hbWUiOiJTSE9fYmx1cmJfYnV0dG9uX2xpbmsiLCJvcHRpb25fdmFsdWUiOiJMaW5rIHRvIGEgcGFnZSJ9LHsib3B0aW9uX25hbWUiOiJTSE9fYmx1cmJfY3VzdG9tX2xpbmsiLCJvcHRpb25fdmFsdWUiOiJodHRwOlwvXC8ifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2JsdXJiX2VuYWJsZSIsIm9wdGlvbl92YWx1ZSI6InRydWUifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2JsdXJiX2xpbmsiLCJvcHRpb25fdmFsdWUiOiJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvNDA0LWVycm9yLXBhZ2VcLyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fYmx1cmJfdGV4dCIsIm9wdGlvbl92YWx1ZSI6Ik51bGxhbSBtb2xsaXMgY29uZ3VlIGZlbGlzLCBzaXQgYW1ldCBjb25zZXF1YXQgcXVhbSB0ZW1wb3Igc2VkLiBOdW5jIG5lYyBzb2RhbGVzIGFudGUuIE51bGxhbSBwb3N1ZXJlLCBtZXR1cy4ifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2JvZHlfZm9udCIsIm9wdGlvbl92YWx1ZSI6IkFyaWFsIn0seyJvcHRpb25fbmFtZSI6IlNIT19ib2R5X2ZvbnRfc3R5bGUiLCJvcHRpb25fdmFsdWUiOiJub3JtYWwifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2JvZHlfZm9udF91bml0Iiwib3B0aW9uX3ZhbHVlIjoicHgifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2JyZWFkY3J1bWJzX2VuYWJsZSIsIm9wdGlvbl92YWx1ZSI6InRydWUifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2JyZWFkY3J1bWJfZGVsaW1pdGVyIiwib3B0aW9uX3ZhbHVlIjoiXHUwMGJiIn0seyJvcHRpb25fbmFtZSI6IlNIT19icmVhZGNydW1iX2hvbWVfbGFiZWwiLCJvcHRpb25fdmFsdWUiOiJIb21lIn0seyJvcHRpb25fbmFtZSI6IlNIT19jYW52YXMiLCJvcHRpb25fdmFsdWUiOiJzOjE1NzU6XCJzOjE1NjU6XCJzOjE1NTU6XCJhOjE6e3M6NDpcInRlc3RcIjthOjQ5OntzOjU6XCJ0aXRsZVwiO3M6NDpcInRlc3RcIjtzOjc6XCJpbmhlcml0XCI7czoxOlwiMVwiO3M6ODpcImJnX2NvbG9yXCI7czo2OlwiZWJmMmY1XCI7czo4OlwiYmdfaW1hZ2VcIjtzOjA6XCJcIjtzOjExOlwiYmdfcG9zaXRpb25cIjtzOjg6XCJ0b3AgbGVmdFwiO3M6MTY6XCJ0b3BfYm9yZGVyX2NvbG9yXCI7czowOlwiXCI7czoxNTpcInRvcF9ib3JkZXJfc2l6ZVwiO3M6MzpcIjRweFwiO3M6MTQ6XCJzdGFnZV9iZ19jb2xvclwiO3M6MDpcIlwiO3M6MTQ6XCJzdGFnZV9iZ19pbWFnZVwiO3M6MDpcIlwiO3M6MTc6XCJzdGFnZV9iZ19wb3NpdGlvblwiO3M6ODpcInRvcCBsZWZ0XCI7czoyMjpcInN0YWdlX3RvcF9ib3JkZXJfY29sb3JcIjtzOjA6XCJcIjtzOjIxOlwic3RhZ2VfdG9wX2JvcmRlcl9zaXplXCI7czozOlwiMHB4XCI7czoxMDpcImJvZHlfY29sb3JcIjtzOjA6XCJcIjtzOjEyOlwiZm9vdGVyX2NvbG9yXCI7czowOlwiXCI7czoxMzpcInNpZGViYXJfY29sb3JcIjtzOjA6XCJcIjtzOjEwOlwibGlua19jb2xvclwiO3M6MDpcIlwiO3M6MTY6XCJsaW5rX2hvdmVyX2NvbG9yXCI7czowOlwiXCI7czo4OlwiaDFfY29sb3JcIjtzOjA6XCJcIjtzOjg6XCJoMl9jb2xvclwiO3M6MDpcIlwiO3M6ODpcImgzX2NvbG9yXCI7czowOlwiXCI7czo4OlwiaDRfY29sb3JcIjtzOjA6XCJcIjtzOjg6XCJoNV9jb2xvclwiO3M6MDpcIlwiO3M6ODpcImg2X2NvbG9yXCI7czowOlwiXCI7czoxOTpcInNpZGViYXJfdGl0bGVfY29sb3JcIjtzOjA6XCJcIjtzOjE4OlwiZm9vdGVyX3RpdGxlX2NvbG9yXCI7czowOlwiXCI7czoxMjpcImJveF9iZ19jb2xvclwiO3M6MDpcIlwiO3M6MTI6XCJib3hfYmdfaW1hZ2VcIjtzOjA6XCJcIjtzOjE1OlwiYm94X2JnX3Bvc2l0aW9uXCI7czo4OlwidG9wIGxlZnRcIjtzOjE1OlwidG9wYmFyX2JnX2NvbG9yXCI7czowOlwiXCI7czoxNTpcInRvcGJhcl9iZ19pbWFnZVwiO3M6MDpcIlwiO3M6MTg6XCJ0b3BiYXJfYmdfcG9zaXRpb25cIjtzOjg6XCJ0b3AgbGVmdFwiO3M6MTk6XCJ0b3BiYXJfYm9yZGVyX2NvbG9yXCI7czowOlwiXCI7czoxODpcInRvcGJhcl9ib3JkZXJfc2l6ZVwiO3M6MzpcIjBweFwiO3M6MTY6XCJtZW51YmFyX2JnX2NvbG9yXCI7czowOlwiXCI7czoxNjpcIm1lbnViYXJfYmdfaW1hZ2VcIjtzOjA6XCJcIjtzOjE5OlwibWVudWJhcl9iZ19wb3NpdGlvblwiO3M6ODpcInRvcCBsZWZ0XCI7czoxOTpcIm1lbnViYXJfYm9yZGVyX3NpemVcIjtzOjM6XCIwcHhcIjtzOjIwOlwibWVudWJhcl9ib3JkZXJfY29sb3JcIjtzOjA6XCJcIjtzOjEzOlwibWVudWJhcl9jb2xvclwiO3M6MDpcIlwiO3M6MTk6XCJzaWRlYmFyYmFyX2JnX2NvbG9yXCI7czowOlwiXCI7czoxOTpcInNpZGViYXJiYXJfYmdfaW1hZ2VcIjtzOjA6XCJcIjtzOjIyOlwic2lkZWJhcmJhcl9iZ19wb3NpdGlvblwiO3M6ODpcInRvcCBsZWZ0XCI7czoyMzpcInNpZGViYXJiYXJfYm9yZGVyX2NvbG9yXCI7czowOlwiXCI7czoyMjpcInNpZGViYXJiYXJfYm9yZGVyX3NpemVcIjtzOjM6XCIwcHhcIjtzOjE1OlwiZm9vdGVyX2JnX2NvbG9yXCI7czowOlwiXCI7czoxNTpcImZvb3Rlcl9iZ19pbWFnZVwiO3M6MDpcIlwiO3M6MTg6XCJmb290ZXJfYmdfcG9zaXRpb25cIjtzOjg6XCJ0b3AgbGVmdFwiO3M6MTk6XCJmb290ZXJfYm9yZGVyX2NvbG9yXCI7czowOlwiXCI7czoxODpcImZvb3Rlcl9ib3JkZXJfc2l6ZVwiO3M6MzpcIjBweFwiO319XCI7XCI7XCI7In0seyJvcHRpb25fbmFtZSI6IlNIT19jYW52YXNfc3R5bGUiLCJvcHRpb25fdmFsdWUiOiJ0ZXN0In0seyJvcHRpb25fbmFtZSI6IlNIT19jYXB0Y2hhX3ByaXZhdGVfa2V5Iiwib3B0aW9uX3ZhbHVlIjoiIn0seyJvcHRpb25fbmFtZSI6IlNIT19jYXB0Y2hhX3B1YmxpY19rZXkiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2NvbnRhY3RfZW1haWwiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2N1Zm9uX2ZvbnQiLCJvcHRpb25fdmFsdWUiOiJBY2lkIn0seyJvcHRpb25fbmFtZSI6IlNIT19jdXN0b21fY3NzIiwib3B0aW9uX3ZhbHVlIjoiIn0seyJvcHRpb25fbmFtZSI6IlNIT19jdXN0b21fZm9udCIsIm9wdGlvbl92YWx1ZSI6IlBUIFNhbnMgTmFycm93In0seyJvcHRpb25fbmFtZSI6IlNIT19jdXN0b21fZ19mb250Iiwib3B0aW9uX3ZhbHVlIjoiIn0seyJvcHRpb25fbmFtZSI6IlNIT19jdXN0b21fZ19mb250X2VuYWJsZSIsIm9wdGlvbl92YWx1ZSI6ImZhbHNlIn0seyJvcHRpb25fbmFtZSI6IlNIT19lbWFpbF91cmwiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2VuYWJsZV9hZG1pbl9sb2dvIiwib3B0aW9uX3ZhbHVlIjoiTm8ifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2ZhY2Vib29rIiwib3B0aW9uX3ZhbHVlIjoiaHR0cDpcL1wvd3d3LmZhY2Vib29rLmNvbVwvcGFnZXNcL1dQVGl0YW5zXC8xMzMxNjkxNDM0NDQzMzEifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2ZhbmN5X2ZvbnQiLCJvcHRpb25fdmFsdWUiOiJEYW5jaW5nIFNjcmlwdCJ9LHsib3B0aW9uX25hbWUiOiJTSE9fZmF2aWNvbiIsIm9wdGlvbl92YWx1ZSI6InlvdXIgdXBsb2FkIHBhdGgifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2ZiX2lkIiwib3B0aW9uX3ZhbHVlIjoiIn0seyJvcHRpb25fbmFtZSI6IlNIT19mbGlja3Jfa2V5Iiwib3B0aW9uX3ZhbHVlIjoiNjg1Zjg5Yjc1ZWZlNWMxN2VhYzhlNGVjYjkwZmY0ZjIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2ZsaWNrcl9uYW1lIiwib3B0aW9uX3ZhbHVlIjoiUHV6emxlcjQ4NzkifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2Zvb3Rlcl9iZ19sYXlvdXQiLCJvcHRpb25fdmFsdWUiOiJCb3hlZCJ9LHsib3B0aW9uX25hbWUiOiJTSE9fZm9vdGVyX2xheW91dCIsIm9wdGlvbl92YWx1ZSI6ImZvdXItY29sIn0seyJvcHRpb25fbmFtZSI6IlNIT19mb290ZXJfbWVudSIsIm9wdGlvbl92YWx1ZSI6IlllcyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fZm9vdGVyX3RleHQiLCJvcHRpb25fdmFsdWUiOiJcdTAwYTkgMjAxMSBcdTAwYjcgWXZvcmEgV29yZFByZXNzIFRoZW1lIFx1MDBiNyBBbGwgUmlnaHRzIFJlc2VydmVkIn0seyJvcHRpb25fbmFtZSI6IlNIT19mb290ZXJfd2lkZ2V0cyIsIm9wdGlvbl92YWx1ZSI6IlllcyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fZm9ybXMiLCJvcHRpb25fdmFsdWUiOiJzOjQ0NjpcImE6MTp7aTowO2E6OTp7czozOlwia2V5XCI7czoxMDpcIlI0WTc2OUY0ODRcIjtzOjE4OlwiZW1haWxfbm90aWZpY2F0aW9uXCI7TjtzOjIxOlwiY2FwdGFjaGFfdmVyaWZpY2F0aW9uXCI7TjtzOjEyOlwiYXV0b19yZXNwb25kXCI7TjtzOjEyOlwibGF5b3V0X3N0eWxlXCI7czo1OlwiYmxvY2tcIjtzOjEyOlwibGFiZWxfdmFsdWVzXCI7YTozOntpOjA7czoxOTpcImNsaWNrIHRvIGVkaXQgbGFiZWxcIjtpOjE7czoxOTpcImNsaWNrIHRvIGVkaXQgbGFiZWxcIjtpOjI7czoxOTpcImNsaWNrIHRvIGVkaXQgbGFiZWxcIjt9czoxMDpcIm5hbWVfdmFsdWVcIjthOjM6e2k6MDtzOjA6XCJcIjtpOjE7czowOlwiXCI7aToyO3M6MDpcIlwiO31zOjEyOlwiZm9ybV9lbGVtZW50XCI7YTozOntpOjA7czo0OlwidGV4dFwiO2k6MTtzOjQ6XCJ0ZXh0XCI7aToyO3M6ODpcInRleHRhcmVhXCI7fXM6MjM6XCJlbWFpbF9ub3RpZmljYXRpb25fbWFpbFwiO3M6MDpcIlwiO319XCI7In0seyJvcHRpb25fbmFtZSI6IlNIT19nYWxsZXJpYV9jcm9wIiwib3B0aW9uX3ZhbHVlIjoidHJ1ZSJ9LHsib3B0aW9uX25hbWUiOiJTSE9fZ2FsbGVyeV9jb21tZW50cyIsIm9wdGlvbl92YWx1ZSI6IlllcyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fZ29vZ2xlIiwib3B0aW9uX3ZhbHVlIjoiaHR0cHM6XC9cL3BsdXMuZ29vZ2xlLmNvbVwvMTAyMDU0MjE2ODI4MDA1ODcyMzMxXC8ifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2gxX2ZvbnRfc2l6ZSIsIm9wdGlvbl92YWx1ZSI6IjMyIn0seyJvcHRpb25fbmFtZSI6IlNIT19oMl9mb250X3NpemUiLCJvcHRpb25fdmFsdWUiOiIyNCJ9LHsib3B0aW9uX25hbWUiOiJTSE9faDNfZm9udF9zaXplIiwib3B0aW9uX3ZhbHVlIjoiMTgifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2g0X2ZvbnRfc2l6ZSIsIm9wdGlvbl92YWx1ZSI6IjE0In0seyJvcHRpb25fbmFtZSI6IlNIT19oNV9mb250X3NpemUiLCJvcHRpb25fdmFsdWUiOiIxNCJ9LHsib3B0aW9uX25hbWUiOiJTSE9faDZfZm9udF9zaXplIiwib3B0aW9uX3ZhbHVlIjoiMTQifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfNHRvcF90aXRsZSIsIm9wdGlvbl92YWx1ZSI6Ill2b3JhIFByZW1pdW0gV29yZFByZXNzIFRoZW1lIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjFfYnV0dG9uX2xhYmVsIiwib3B0aW9uX3ZhbHVlIjoibW9yZSBcdTIxOTIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfY29sdW1uMV9idXR0b25fbGluayIsIm9wdGlvbl92YWx1ZSI6IiMifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfY29sdW1uMV9pbWFnZSIsIm9wdGlvbl92YWx1ZSI6Imh0dHA6XC9cL3dwdGl0YW5zLmNvbVwvc2hvdHp6XC9maWxlc1wvMjAxMVwvMTBcL2NvbDQtaWNvbi5wbmcifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfY29sdW1uMV90ZXh0Iiwib3B0aW9uX3ZhbHVlIjoiRHVpcyB0ZW1wb3Igc2FwaWVuIHZpdGFlIG5pc2kgdm9sdXRwYXQgcGhcclxuYXJldHJhLiBOdW5jIGVzdCBuaXNsLCBhdWN0b3IgZWdldCBwb3J0dGl0b3IgXHJcbnRlbXB1cywgbW9sZXN0aWUgYWMgdHVycGlzLiAifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfY29sdW1uMV90aXRsZSIsIm9wdGlvbl92YWx1ZSI6IkR1aXMgdGVtcG9yIHNhcGllbiB2aXRhZSBuaXNpIHBvcnR0aXRvciAgdGVtcHUifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfY29sdW1uMl9idXR0b25fbGFiZWwiLCJvcHRpb25fdmFsdWUiOiJtb3JlIFx1MjE5MiJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9jb2x1bW4yX2J1dHRvbl9saW5rIiwib3B0aW9uX3ZhbHVlIjoiIyJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9jb2x1bW4yX2ltYWdlIiwib3B0aW9uX3ZhbHVlIjoiaHR0cDpcL1wvd3B0aXRhbnMuY29tXC9zaG90enpcL2ZpbGVzXC8yMDExXC8xMFwvY29sMy1pY29uLnBuZyJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9jb2x1bW4yX3RleHQiLCJvcHRpb25fdmFsdWUiOiJEdWlzIHRlbXBvciBzYXBpZW4gdml0YWUgbmlzaSB2b2x1dHBhdCBwaFxyXG5hcmV0cmEuIE51bmMgZXN0IG5pc2wsIGF1Y3RvciBlZ2V0IHBvcnR0aXRvciBcclxudGVtcHVzLCBtb2xlc3RpZSBhYyB0dXJwaXMuICJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9jb2x1bW4yX3RpdGxlIiwib3B0aW9uX3ZhbHVlIjoiRHVpcyB0ZW1wb3Igc2FwaWVuIHZpdGFlIG5pc2kgcG9ydHRpdG9yIHRpdG9yICJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9jb2x1bW4zX2J1dHRvbl9sYWJlbCIsIm9wdGlvbl92YWx1ZSI6Im1vcmUgXHUyMTkyIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjNfYnV0dG9uX2xpbmsiLCJvcHRpb25fdmFsdWUiOiIjIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjNfaW1hZ2UiLCJvcHRpb25fdmFsdWUiOiJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC9jb2wxLWljb24ucG5nIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjNfdGV4dCIsIm9wdGlvbl92YWx1ZSI6IkR1aXMgdGVtcG9yIHNhcGllbiB2aXRhZSBuaXNpIHZvbHV0cGF0IHBoXHJcbmFyZXRyYS4gTnVuYyBlc3QgbmlzbCwgYXVjdG9yIGVnZXQgcG9ydHRpdG9yIFxyXG50ZW1wdXMsIG1vbGVzdGllIGFjIHR1cnBpcy4gIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjNfdGl0bGUiLCJvcHRpb25fdmFsdWUiOiJEdWlzIHRlbXBvciBzYXBpZW4gdml0YWUgbmlzaSBwb3J0dGl0b2FjIHR1cnBpcyJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9jb2x1bW40X2J1dHRvbl9sYWJlbCIsIm9wdGlvbl92YWx1ZSI6Im1vcmUgXHUyMTkyIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjRfYnV0dG9uX2xpbmsiLCJvcHRpb25fdmFsdWUiOiIjIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjRfaW1hZ2UiLCJvcHRpb25fdmFsdWUiOiJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC9jb2wyLWljb24ucG5nIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjRfdGV4dCIsIm9wdGlvbl92YWx1ZSI6IkR1aXMgdGVtcG9yIHNhcGllbiB2aXRhZSBuaXNpIHZvbHV0cGF0IHBoXHJcbmFyZXRyYS4gTnVuYyBlc3QgbmlzbCwgYXVjdG9yIGVnZXQgcG9ydHRpdG9yIFxyXG50ZW1wdXMsIG1vbGVzdGllIGFjIHR1cnBpcy4gIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX2NvbHVtbjRfdGl0bGUiLCJvcHRpb25fdmFsdWUiOiJEdWlzIHRlbXBvciBzYXBpZW4gdml0YWUgbmlzaSBwbyBpcyB0ZW1wb3Igc2FwaWVuICJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9sYXlvdXQiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfcnBfZW5hYmxlIiwib3B0aW9uX3ZhbHVlIjoidHJ1ZSJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9ycF9sYWJlbCIsIm9wdGlvbl92YWx1ZSI6IlJlYWQgdGhlIEJsb2cifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfcnBfbGluayIsIm9wdGlvbl92YWx1ZSI6IiMifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfcnBfcG9zdCIsIm9wdGlvbl92YWx1ZSI6InBvc3QifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfcnBfdGV4dCIsIm9wdGlvbl92YWx1ZSI6IiBQcmFlc2VudCBwdXJ1cyB2ZWxpdCwgcGxhY2VyYXQgbmVjIHVudCB1bSBhbGVzdWFkYSBhZGlwaXNjaW5nIGFudGUudXJpcyBldCB0ZWxsdXMgdmVsIGl0LCBldCBhbGlxdWFtIGZlbGlzZW5hcyB2ZXN0aWJ1XHJcblxyXG5OdW5jIGVnZXN0YXMganVzdG8gdml0YWUgZWxpdCB0ZW1wdXMgc2VkIGFkaXBpc2NpbmcgcmlzdXMgcHJldGl1bS4gRnVzY2UgZWxpdCBtYXNzYSwgcHJldGl1bSBub24gdmVoaWN1bGEgc2VtcGVyLCJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9ycF90aXRsZSIsIm9wdGlvbl92YWx1ZSI6IkxhdGVzdCBmcm9tIHRoZSBibG9nIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX3Njcm9sbF9lbmFibGUiLCJvcHRpb25fdmFsdWUiOiJ0cnVlIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX3Njcm9sbF9wb3N0Iiwib3B0aW9uX3ZhbHVlIjoicG9zdCJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9zY3JvbGxfdGl0bGUiLCJvcHRpb25fdmFsdWUiOiJMYXRlc3QgZnJvbSB0aGUgQmxvZyJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9zaWRlYmFyIiwib3B0aW9uX3ZhbHVlIjoiQmxvZyBTaWRlYmFyIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX3NsaWRlciIsIm9wdGlvbl92YWx1ZSI6IlNsaWRlciAxIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX3N0YWdlZF9pbWFnZSIsIm9wdGlvbl92YWx1ZSI6Imh0dHA6XC9cL3dwdGl0YW5zLmNvbVwvc2hvdHp6XC9maWxlc1wvMjAxMVwvMTBcL2lNYWMucG5nIn0seyJvcHRpb25fbmFtZSI6IlNIT19ob21lX3N0YWdlZF90ZXh0Iiwib3B0aW9uX3ZhbHVlIjoiPHA+UGhhc2VsbHVzIHRpbmNpZHVudCB0aW5jaWR1bnQgc2VtcGVyLiBTZWQgcHJldGl1bSBvcm5hcmUgcG9zdWVyZS4gU2VkIGlkIG51bmMgbWV0dXMsIHNpdCBhbWV0IHB1bHZpbmFyIGVzdC4gU2VkIHZpdGFlIHF1YW0gZWdldCBtaSBkYXBpYnVzIGNvbnZhbGxpcy4gPFwvcD5cclxuXHJcbjxwPlByYWVzZW50IG51bGxhIGVzdCwgYWxpcXVhbSBhIGN1cnN1cyBjb25ndWUsIGV1aXNtb2Qgc2VkIGF1Z3VlLiBNYWVjZW5hcyBiaWJlbmR1bSBoZW5kcmVyaXQgdmVzdGlidWx1bS4gTWF1cmlzIGJsYW5kaXQsIHJpc3VzIHZlbCBhbGlxdWV0IHJob25jdXMsIGxhY3VzIGxlbyBvcm5hcmUgYXVndWUuICA8XC9wPlxyXG5cclxuW2J1dHRvbiByYWRpdXM9XFwnMnB4XFwnIGJhY2tncm91bmQ9XFwnIzI0MjQyNFxcJyBib3JkZXI9XFwnIzI0MjQyNFxcJyBjb2xvcj1cXCcjZmZmZmZmXFwnIGxpbms9XFwnaHR0cDpcL1wvc3ZlbmdyYXBoLmRldmlhbnRhcnQuY29tXC9cXCcgc2l6ZT1cXCdzbWFsbFxcJ10gQnV0dG9uIFtcL2J1dHRvbl0ifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfc3RhZ2VkX3RpdGxlIiwib3B0aW9uX3ZhbHVlIjoiUGhhc2VsbHVzIHRpbmNpZHVudCB0aW5jaWR1bnQgc2VtcGVyIG9yYmkgZXJvcyBudWxsYSwgZmF1Y2lidXMgaWQgcGVsbGVudGVzcXVlIG5lYyJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV9zdGF0aWNfaW1hZ2UiLCJvcHRpb25fdmFsdWUiOiJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC8xMC5qcGcifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2hvbWVfdGVzdGltb25pYWxfZW5hYmxlIiwib3B0aW9uX3ZhbHVlIjoidHJ1ZSJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV90aXRsZSIsIm9wdGlvbl92YWx1ZSI6IlBoYXNlbGx1cyB0aW5jaWR1bnQgdGluY2lkdW50IHNlbXBlci4gU2VkIHByZXRpdW0gb3JuYXJlIHBvc3VlcmUuIFNlZCBpZCBudW5jIG1ldHVzLCBzaXQgYW1ldCBwdWx2aW5hciBlc3QuIFNlZCB2aXRhZSBxdWFtIGVnZXQgbWkgZGFwaWJ1cyBjb252YWxsaXMuICJ9LHsib3B0aW9uX25hbWUiOiJTSE9faG9tZV90b3BfY29sdW1uc19lbmFibGUiLCJvcHRpb25fdmFsdWUiOiJ0cnVlIn0seyJvcHRpb25fbmFtZSI6IlNIT19pbWFnZV9yZXNpemUiLCJvcHRpb25fdmFsdWUiOiJUaW10aHVtYiJ9LHsib3B0aW9uX25hbWUiOiJTSE9faW5hY3RpdmVfc2lkZWJhcnMiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX2xpbmtlZGluIiwib3B0aW9uX3ZhbHVlIjoiIyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fbG9nbyIsIm9wdGlvbl92YWx1ZSI6Imh0dHA6XC9cL3dwdGl0YW5zLmNvbVwvc2hvdHp6XC9maWxlc1wvMjAxMVwvMTBcL2xvZ28ucG5nIn0seyJvcHRpb25fbmFtZSI6IlNIT19ub3Rmb3VuZF9sb2dvIiwib3B0aW9uX3ZhbHVlIjoiaHR0cDpcL1wvd3B0aXRhbnMuY29tXC95dm9yYVwvd3AtY29udGVudFwvdGhlbWVzXC95dm9yYVwvc3ByaXRlc1wvaVwvbm90Zm91bmQucG5nIn0seyJvcHRpb25fbmFtZSI6IlNIT19ub3Rmb3VuZF90ZXh0Iiwib3B0aW9uX3ZhbHVlIjoiIn0seyJvcHRpb25fbmFtZSI6IlNIT19ub3Rmb3VuZF90aXRsZSIsIm9wdGlvbl92YWx1ZSI6IiJ9LHsib3B0aW9uX25hbWUiOiJTSE9fbm90aWZ5X21zZyIsIm9wdGlvbl92YWx1ZSI6IlRoYW5rIHlvdSBmb3IgY29udGFjdGluZyB5b3UsIHdlIHdpbGwgYmUgaW4gdG91Y2ggc29vbiA6KSAifSx7Im9wdGlvbl9uYW1lIjoiU0hPX29wdGlvbnNfbG9nbyIsIm9wdGlvbl92YWx1ZSI6Imh0dHA6XC9cL3dwdGl0YW5zLmNvbVwveXZvcmFcL2ZpbGVzXC8yMDExXC8wOVwvbG9nby5qcGcifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3BhZ2VfY29tbWVudHMiLCJvcHRpb25fdmFsdWUiOiJZZXMifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3BhZ2luYXRpb24iLCJvcHRpb25fdmFsdWUiOiJudW1iZXJzIn0seyJvcHRpb25fbmFtZSI6IlNIT19wbGFpbl90aGVtZSIsIm9wdGlvbl92YWx1ZSI6IlR1cnF1b2lzZSJ9LHsib3B0aW9uX25hbWUiOiJTSE9fcG9wdWxhciIsIm9wdGlvbl92YWx1ZSI6InRydWUifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3BvcHVsYXJfbm8iLCJvcHRpb25fdmFsdWUiOiI0In0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW8xX2l0ZW1fbGltaXQiLCJvcHRpb25fdmFsdWUiOiIzIn0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW8xX2xpbWl0Iiwib3B0aW9uX3ZhbHVlIjoiMjUwIn0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW8yX2l0ZW1fbGltaXQiLCJvcHRpb25fdmFsdWUiOiI0In0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW8yX2xpbWl0Iiwib3B0aW9uX3ZhbHVlIjoiMjUwIn0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW8zX2l0ZW1fbGltaXQiLCJvcHRpb25fdmFsdWUiOiI2In0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW8zX2xpbWl0Iiwib3B0aW9uX3ZhbHVlIjoiMjUwIn0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW80X2l0ZW1fbGltaXQiLCJvcHRpb25fdmFsdWUiOiI4In0seyJvcHRpb25fbmFtZSI6IlNIT19wb3J0Zm9saW80X2xpbWl0Iiwib3B0aW9uX3ZhbHVlIjoiODIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3BvcnRmb2xpb19jb21tZW50cyIsIm9wdGlvbl92YWx1ZSI6IlllcyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fcG9zdHNfY29tbWVudHMiLCJvcHRpb25fdmFsdWUiOiJZZXMifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3Bvc3RzX2l0ZW1fbGltaXQiLCJvcHRpb25fdmFsdWUiOiI2In0seyJvcHRpb25fbmFtZSI6IlNIT19yc3NfdXJsIiwib3B0aW9uX3ZhbHVlIjoiIn0seyJvcHRpb25fbmFtZSI6IlNIT19zaG93X3RvcGJhciIsIm9wdGlvbl92YWx1ZSI6IlllcyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fc2t5cGUiLCJvcHRpb25fdmFsdWUiOiIjIn0seyJvcHRpb25fbmFtZSI6IlNIT19zbGlkZXJzIiwib3B0aW9uX3ZhbHVlIjoiczo2Mjg2Olwiczo2Mjc2OlwiYToxOntzOjg6XCJTbGlkZXIgMVwiO2E6ODp7czo1OlwidGl0bGVcIjtzOjg6XCJTbGlkZXIgMVwiO3M6NTpcIndpZHRoXCI7czozOlwiOTgwXCI7czo2OlwiaGVpZ2h0XCI7czozOlwiNDQ1XCI7czo0OlwidHlwZVwiO3M6OTpcIkdyaWQgV2FsbFwiO3M6ODpcImludGVydmFsXCI7czoxOlwiNVwiO3M6ODpcImF1dG9wbGF5XCI7czo0OlwidHJ1ZVwiO3M6ODpcImNvbnRyb2xzXCI7czo0OlwidHJ1ZVwiO3M6NjpcInNsaWRlc1wiO2E6MTY6e2k6MDthOjQ6e3M6MTE6XCJzbGlkZV90aXRsZVwiO3M6Mjk6XCJQZWxsZW50ZXNxdWUgbmVjIGN1cnN1cyBuaXNsLlwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDc6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC8zMS5qcGdcIjtzOjExOlwiZGVzY3JpcHRpb25cIjtzOjE3OTpcIlZlc3RpYnVsdW0gYWNjdW1zYW4gdHJpc3RpcXVlIG1hc3NhLCBhYyBhbGlxdWFtIGF1Z3VlIHZvbHV0cGF0IGVnZXQuIERvbmVjIGp1c3RvIGVyb3MsIGdyYXZpZGEgdHJpc3RpcXVlIG9ybmFyZSBzaXQgYW1ldCwgcmhvbmN1cyBhIGxlby4gTnVsbGEgZmFjaWxpc2kuIEludGVnZXIgcnV0cnVtIG5pc2wgZXJvcy4gXCI7fWk6MTthOjQ6e3M6MTE6XCJzbGlkZV90aXRsZVwiO3M6Mjk6XCJQZWxsZW50ZXNxdWUgbmVjIGN1cnN1cyBuaXNsLlwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDY6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC8yLmpwZ1wiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MTc5OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2EsIGFjIGFsaXF1YW0gYXVndWUgdm9sdXRwYXQgZWdldC4gRG9uZWMganVzdG8gZXJvcywgZ3JhdmlkYSB0cmlzdGlxdWUgb3JuYXJlIHNpdCBhbWV0LCByaG9uY3VzIGEgbGVvLiBOdWxsYSBmYWNpbGlzaS4gSW50ZWdlciBydXRydW0gbmlzbCBlcm9zLiBcIjt9aToyO2E6NDp7czoxMTpcInNsaWRlX3RpdGxlXCI7czoyOTpcIlBlbGxlbnRlc3F1ZSBuZWMgY3Vyc3VzIG5pc2wuXCI7czoxMDpcInNsaWRlX2xpbmtcIjtzOjE6XCIjXCI7czoxMTpcInNsaWRlX2ltYWdlXCI7czo0NjpcImh0dHA6XC9cL3dwdGl0YW5zLmNvbVwvc2hvdHp6XC9maWxlc1wvMjAxMVwvMTBcLzQuanBnXCI7czoxMTpcImRlc2NyaXB0aW9uXCI7czoxNzk6XCJWZXN0aWJ1bHVtIGFjY3Vtc2FuIHRyaXN0aXF1ZSBtYXNzYSwgYWMgYWxpcXVhbSBhdWd1ZSB2b2x1dHBhdCBlZ2V0LiBEb25lYyBqdXN0byBlcm9zLCBncmF2aWRhIHRyaXN0aXF1ZSBvcm5hcmUgc2l0IGFtZXQsIHJob25jdXMgYSBsZW8uIE51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO31pOjM7YTo0OntzOjExOlwic2xpZGVfdGl0bGVcIjtzOjI5OlwiUGVsbGVudGVzcXVlIG5lYyBjdXJzdXMgbmlzbC5cIjtzOjEwOlwic2xpZGVfbGlua1wiO3M6MTpcIiNcIjtzOjExOlwic2xpZGVfaW1hZ2VcIjtzOjQ3OlwiaHR0cDpcL1wvd3B0aXRhbnMuY29tXC9zaG90enpcL2ZpbGVzXC8yMDExXC8xMFwvMTAuanBnXCI7czoxMTpcImRlc2NyaXB0aW9uXCI7czoxNzk6XCJWZXN0aWJ1bHVtIGFjY3Vtc2FuIHRyaXN0aXF1ZSBtYXNzYSwgYWMgYWxpcXVhbSBhdWd1ZSB2b2x1dHBhdCBlZ2V0LiBEb25lYyBqdXN0byBlcm9zLCBncmF2aWRhIHRyaXN0aXF1ZSBvcm5hcmUgc2l0IGFtZXQsIHJob25jdXMgYSBsZW8uIE51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO31pOjQ7YTo0OntzOjExOlwic2xpZGVfdGl0bGVcIjtzOjI5OlwiUGVsbGVudGVzcXVlIG5lYyBjdXJzdXMgbmlzbC5cIjtzOjEwOlwic2xpZGVfbGlua1wiO3M6MTpcIiNcIjtzOjExOlwic2xpZGVfaW1hZ2VcIjtzOjQ2OlwiaHR0cDpcL1wvd3B0aXRhbnMuY29tXC9zaG90enpcL2ZpbGVzXC8yMDExXC8xMFwvMS5qcGdcIjtzOjExOlwiZGVzY3JpcHRpb25cIjtzOjE3OTpcIlZlc3RpYnVsdW0gYWNjdW1zYW4gdHJpc3RpcXVlIG1hc3NhLCBhYyBhbGlxdWFtIGF1Z3VlIHZvbHV0cGF0IGVnZXQuIERvbmVjIGp1c3RvIGVyb3MsIGdyYXZpZGEgdHJpc3RpcXVlIG9ybmFyZSBzaXQgYW1ldCwgcmhvbmN1cyBhIGxlby4gTnVsbGEgZmFjaWxpc2kuIEludGVnZXIgcnV0cnVtIG5pc2wgZXJvcy4gXCI7fWk6NTthOjQ6e3M6MTE6XCJzbGlkZV90aXRsZVwiO3M6Mjk6XCJQZWxsZW50ZXNxdWUgbmVjIGN1cnN1cyBuaXNsLlwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDY6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC81LmpwZ1wiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MTc5OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2EsIGFjIGFsaXF1YW0gYXVndWUgdm9sdXRwYXQgZWdldC4gRG9uZWMganVzdG8gZXJvcywgZ3JhdmlkYSB0cmlzdGlxdWUgb3JuYXJlIHNpdCBhbWV0LCByaG9uY3VzIGEgbGVvLiBOdWxsYSBmYWNpbGlzaS4gSW50ZWdlciBydXRydW0gbmlzbCBlcm9zLiBcIjt9aTo2O2E6NDp7czoxMTpcInNsaWRlX3RpdGxlXCI7czo0MjpcIk51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDY6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC85LmpwZ1wiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MTc5OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2EsIGFjIGFsaXF1YW0gYXVndWUgdm9sdXRwYXQgZWdldC4gRG9uZWMganVzdG8gZXJvcywgZ3JhdmlkYSB0cmlzdGlxdWUgb3JuYXJlIHNpdCBhbWV0LCByaG9uY3VzIGEgbGVvLiBOdWxsYSBmYWNpbGlzaS4gSW50ZWdlciBydXRydW0gbmlzbCBlcm9zLiBcIjt9aTo3O2E6NDp7czoxMTpcInNsaWRlX3RpdGxlXCI7czo0MjpcIk51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDY6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC82LmpwZ1wiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MTc5OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2EsIGFjIGFsaXF1YW0gYXVndWUgdm9sdXRwYXQgZWdldC4gRG9uZWMganVzdG8gZXJvcywgZ3JhdmlkYSB0cmlzdGlxdWUgb3JuYXJlIHNpdCBhbWV0LCByaG9uY3VzIGEgbGVvLiBOdWxsYSBmYWNpbGlzaS4gSW50ZWdlciBydXRydW0gbmlzbCBlcm9zLiBcIjt9aTo4O2E6NDp7czoxMTpcInNsaWRlX3RpdGxlXCI7czo0MjpcIk51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDY6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC84LmpwZ1wiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MTc5OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2EsIGFjIGFsaXF1YW0gYXVndWUgdm9sdXRwYXQgZWdldC4gRG9uZWMganVzdG8gZXJvcywgZ3JhdmlkYSB0cmlzdGlxdWUgb3JuYXJlIHNpdCBhbWV0LCByaG9uY3VzIGEgbGVvLiBOdWxsYSBmYWNpbGlzaS4gSW50ZWdlciBydXRydW0gbmlzbCBlcm9zLiBcIjt9aTo5O2E6NDp7czoxMTpcInNsaWRlX3RpdGxlXCI7czo0MjpcIk51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDc6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC83MS5qcGdcIjtzOjExOlwiZGVzY3JpcHRpb25cIjtzOjE3OTpcIlZlc3RpYnVsdW0gYWNjdW1zYW4gdHJpc3RpcXVlIG1hc3NhLCBhYyBhbGlxdWFtIGF1Z3VlIHZvbHV0cGF0IGVnZXQuIERvbmVjIGp1c3RvIGVyb3MsIGdyYXZpZGEgdHJpc3RpcXVlIG9ybmFyZSBzaXQgYW1ldCwgcmhvbmN1cyBhIGxlby4gTnVsbGEgZmFjaWxpc2kuIEludGVnZXIgcnV0cnVtIG5pc2wgZXJvcy4gXCI7fWk6MTA7YTo0OntzOjExOlwic2xpZGVfdGl0bGVcIjtzOjM1OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2FcIjtzOjEwOlwic2xpZGVfbGlua1wiO3M6MTpcIiNcIjtzOjExOlwic2xpZGVfaW1hZ2VcIjtzOjQ3OlwiaHR0cDpcL1wvd3B0aXRhbnMuY29tXC9zaG90enpcL2ZpbGVzXC8yMDExXC8xMFwvMTEuanBnXCI7czoxMTpcImRlc2NyaXB0aW9uXCI7czoxNzk6XCJWZXN0aWJ1bHVtIGFjY3Vtc2FuIHRyaXN0aXF1ZSBtYXNzYSwgYWMgYWxpcXVhbSBhdWd1ZSB2b2x1dHBhdCBlZ2V0LiBEb25lYyBqdXN0byBlcm9zLCBncmF2aWRhIHRyaXN0aXF1ZSBvcm5hcmUgc2l0IGFtZXQsIHJob25jdXMgYSBsZW8uIE51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO31pOjExO2E6NDp7czoxMTpcInNsaWRlX3RpdGxlXCI7czozNTpcIlZlc3RpYnVsdW0gYWNjdW1zYW4gdHJpc3RpcXVlIG1hc3NhXCI7czoxMDpcInNsaWRlX2xpbmtcIjtzOjE6XCIjXCI7czoxMTpcInNsaWRlX2ltYWdlXCI7czo0NzpcImh0dHA6XC9cL3dwdGl0YW5zLmNvbVwvc2hvdHp6XC9maWxlc1wvMjAxMVwvMTBcLzEyLmpwZ1wiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MTc5OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2EsIGFjIGFsaXF1YW0gYXVndWUgdm9sdXRwYXQgZWdldC4gRG9uZWMganVzdG8gZXJvcywgZ3JhdmlkYSB0cmlzdGlxdWUgb3JuYXJlIHNpdCBhbWV0LCByaG9uY3VzIGEgbGVvLiBOdWxsYSBmYWNpbGlzaS4gSW50ZWdlciBydXRydW0gbmlzbCBlcm9zLiBcIjt9aToxMjthOjQ6e3M6MTE6XCJzbGlkZV90aXRsZVwiO3M6MzU6XCJWZXN0aWJ1bHVtIGFjY3Vtc2FuIHRyaXN0aXF1ZSBtYXNzYVwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDc6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC8xNi5qcGdcIjtzOjExOlwiZGVzY3JpcHRpb25cIjtzOjE3OTpcIlZlc3RpYnVsdW0gYWNjdW1zYW4gdHJpc3RpcXVlIG1hc3NhLCBhYyBhbGlxdWFtIGF1Z3VlIHZvbHV0cGF0IGVnZXQuIERvbmVjIGp1c3RvIGVyb3MsIGdyYXZpZGEgdHJpc3RpcXVlIG9ybmFyZSBzaXQgYW1ldCwgcmhvbmN1cyBhIGxlby4gTnVsbGEgZmFjaWxpc2kuIEludGVnZXIgcnV0cnVtIG5pc2wgZXJvcy4gXCI7fWk6MTM7YTo0OntzOjExOlwic2xpZGVfdGl0bGVcIjtzOjM1OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2FcIjtzOjEwOlwic2xpZGVfbGlua1wiO3M6MTpcIiNcIjtzOjExOlwic2xpZGVfaW1hZ2VcIjtzOjQ3OlwiaHR0cDpcL1wvd3B0aXRhbnMuY29tXC9zaG90enpcL2ZpbGVzXC8yMDExXC8xMFwvMTUuanBnXCI7czoxMTpcImRlc2NyaXB0aW9uXCI7czoxNzk6XCJWZXN0aWJ1bHVtIGFjY3Vtc2FuIHRyaXN0aXF1ZSBtYXNzYSwgYWMgYWxpcXVhbSBhdWd1ZSB2b2x1dHBhdCBlZ2V0LiBEb25lYyBqdXN0byBlcm9zLCBncmF2aWRhIHRyaXN0aXF1ZSBvcm5hcmUgc2l0IGFtZXQsIHJob25jdXMgYSBsZW8uIE51bGxhIGZhY2lsaXNpLiBJbnRlZ2VyIHJ1dHJ1bSBuaXNsIGVyb3MuIFwiO31pOjE0O2E6NDp7czoxMTpcInNsaWRlX3RpdGxlXCI7czozNTpcIlZlc3RpYnVsdW0gYWNjdW1zYW4gdHJpc3RpcXVlIG1hc3NhXCI7czoxMDpcInNsaWRlX2xpbmtcIjtzOjE6XCIjXCI7czoxMTpcInNsaWRlX2ltYWdlXCI7czo0NzpcImh0dHA6XC9cL3dwdGl0YW5zLmNvbVwvc2hvdHp6XC9maWxlc1wvMjAxMVwvMTBcLzEzLmpwZ1wiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MTc5OlwiVmVzdGlidWx1bSBhY2N1bXNhbiB0cmlzdGlxdWUgbWFzc2EsIGFjIGFsaXF1YW0gYXVndWUgdm9sdXRwYXQgZWdldC4gRG9uZWMganVzdG8gZXJvcywgZ3JhdmlkYSB0cmlzdGlxdWUgb3JuYXJlIHNpdCBhbWV0LCByaG9uY3VzIGEgbGVvLiBOdWxsYSBmYWNpbGlzaS4gSW50ZWdlciBydXRydW0gbmlzbCBlcm9zLiBcIjt9aToxNTthOjQ6e3M6MTE6XCJzbGlkZV90aXRsZVwiO3M6MzU6XCJWZXN0aWJ1bHVtIGFjY3Vtc2FuIHRyaXN0aXF1ZSBtYXNzYVwiO3M6MTA6XCJzbGlkZV9saW5rXCI7czoxOlwiI1wiO3M6MTE6XCJzbGlkZV9pbWFnZVwiO3M6NDc6XCJodHRwOlwvXC93cHRpdGFucy5jb21cL3Nob3R6elwvZmlsZXNcLzIwMTFcLzEwXC8xNy5qcGdcIjtzOjExOlwiZGVzY3JpcHRpb25cIjtzOjE3OTpcIlZlc3RpYnVsdW0gYWNjdW1zYW4gdHJpc3RpcXVlIG1hc3NhLCBhYyBhbGlxdWFtIGF1Z3VlIHZvbHV0cGF0IGVnZXQuIERvbmVjIGp1c3RvIGVyb3MsIGdyYXZpZGEgdHJpc3RpcXVlIG9ybmFyZSBzaXQgYW1ldCwgcmhvbmN1cyBhIGxlby4gTnVsbGEgZmFjaWxpc2kuIEludGVnZXIgcnV0cnVtIG5pc2wgZXJvcy4gXCI7fX19fVwiO1wiOyJ9LHsib3B0aW9uX25hbWUiOiJTSE9fc29jaWFsX3NldCIsIm9wdGlvbl92YWx1ZSI6ImZhbHNlIn0seyJvcHRpb25fbmFtZSI6IlNIT19zb2NpYWxfc2V0X3N0eWxlIiwib3B0aW9uX3ZhbHVlIjoiU3R5bGUgMSJ9LHsib3B0aW9uX25hbWUiOiJTSE9fc3RhZ2Vfb3B0aW9uIiwib3B0aW9uX3ZhbHVlIjoiU2xpZGVyIn0seyJvcHRpb25fbmFtZSI6IlNIT19zdHlsZV9saXN0ZW5lciIsIm9wdGlvbl92YWx1ZSI6IkRlZmF1bHQifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3N1YnNjcmliZSIsIm9wdGlvbl92YWx1ZSI6IiMifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3Rlc3RpbW9uaWFscyIsIm9wdGlvbl92YWx1ZSI6InM6MTY0ODpcInM6MTYzODpcImE6NDp7aTowO2E6Mzp7czo0OlwibmFtZVwiO3M6NjpcIkxvcmVtIFwiO3M6NDpcImxpbmtcIjtzOjE2OlwiaHR0cDpcL1wvYXBwbGUuY29tXCI7czoxMTpcImRlc2NyaXB0aW9uXCI7czozMTE6XCJJIGRpZG4ndCBzZWUgaXQgdGhlbiwgYnV0IGl0IHR1cm5lZCBvdXQgdGhhdCBnZXR0aW5nIGZpcmVkIGZyb20gQXBwbGUgd2FzIHRoZSBiZXN0IHRoaW5nIHRoYXQgY291bGQgaGF2ZSBldmVyIGhhcHBlbmVkIHRvIG1lLiBUaGUgaGVhdmluZXNzIG9mIGJlaW5nIHN1Y2Nlc3NmdWwgd2FzIHJlcGxhY2VkIGJ5IHRoZSBsaWdodG5lc3Mgb2YgYmVpbmcgYSBiZWdpbm5lciBhZ2FpbiwgbGVzcyBzdXJlIGFib3V0IGV2ZXJ5dGhpbmcuIEl0IGZyZWVkIG1lIHRvIGVudGVyIG9uZSBvZiB0aGUgbW9zdCBjcmVhdGl2ZSBwZXJpb2RzIG9mIG15IGxpZmUuIFwiO31pOjE7YTozOntzOjQ6XCJuYW1lXCI7czo2OlwiTG9yZW0gXCI7czo0OlwibGlua1wiO3M6MTY6XCJodHRwOlwvXC9hcHBsZS5jb21cIjtzOjExOlwiZGVzY3JpcHRpb25cIjtzOjMxMTpcIkkgZGlkbid0IHNlZSBpdCB0aGVuLCBidXQgaXQgdHVybmVkIG91dCB0aGF0IGdldHRpbmcgZmlyZWQgZnJvbSBBcHBsZSB3YXMgdGhlIGJlc3QgdGhpbmcgdGhhdCBjb3VsZCBoYXZlIGV2ZXIgaGFwcGVuZWQgdG8gbWUuIFRoZSBoZWF2aW5lc3Mgb2YgYmVpbmcgc3VjY2Vzc2Z1bCB3YXMgcmVwbGFjZWQgYnkgdGhlIGxpZ2h0bmVzcyBvZiBiZWluZyBhIGJlZ2lubmVyIGFnYWluLCBsZXNzIHN1cmUgYWJvdXQgZXZlcnl0aGluZy4gSXQgZnJlZWQgbWUgdG8gZW50ZXIgb25lIG9mIHRoZSBtb3N0IGNyZWF0aXZlIHBlcmlvZHMgb2YgbXkgbGlmZS4gXCI7fWk6MjthOjM6e3M6NDpcIm5hbWVcIjtzOjY6XCJMb3JlbSBcIjtzOjQ6XCJsaW5rXCI7czoxNjpcImh0dHA6XC9cL2FwcGxlLmNvbVwiO3M6MTE6XCJkZXNjcmlwdGlvblwiO3M6MzExOlwiSSBkaWRuJ3Qgc2VlIGl0IHRoZW4sIGJ1dCBpdCB0dXJuZWQgb3V0IHRoYXQgZ2V0dGluZyBmaXJlZCBmcm9tIEFwcGxlIHdhcyB0aGUgYmVzdCB0aGluZyB0aGF0IGNvdWxkIGhhdmUgZXZlciBoYXBwZW5lZCB0byBtZS4gVGhlIGhlYXZpbmVzcyBvZiBiZWluZyBzdWNjZXNzZnVsIHdhcyByZXBsYWNlZCBieSB0aGUgbGlnaHRuZXNzIG9mIGJlaW5nIGEgYmVnaW5uZXIgYWdhaW4sIGxlc3Mgc3VyZSBhYm91dCBldmVyeXRoaW5nLiBJdCBmcmVlZCBtZSB0byBlbnRlciBvbmUgb2YgdGhlIG1vc3QgY3JlYXRpdmUgcGVyaW9kcyBvZiBteSBsaWZlLiBcIjt9aTozO2E6Mzp7czo0OlwibmFtZVwiO3M6NjpcIkxvcmVtIFwiO3M6NDpcImxpbmtcIjtzOjE2OlwiaHR0cDpcL1wvYXBwbGUuY29tXCI7czoxMTpcImRlc2NyaXB0aW9uXCI7czozMTE6XCJJIGRpZG4ndCBzZWUgaXQgdGhlbiwgYnV0IGl0IHR1cm5lZCBvdXQgdGhhdCBnZXR0aW5nIGZpcmVkIGZyb20gQXBwbGUgd2FzIHRoZSBiZXN0IHRoaW5nIHRoYXQgY291bGQgaGF2ZSBldmVyIGhhcHBlbmVkIHRvIG1lLiBUaGUgaGVhdmluZXNzIG9mIGJlaW5nIHN1Y2Nlc3NmdWwgd2FzIHJlcGxhY2VkIGJ5IHRoZSBsaWdodG5lc3Mgb2YgYmVpbmcgYSBiZWdpbm5lciBhZ2FpbiwgbGVzcyBzdXJlIGFib3V0IGV2ZXJ5dGhpbmcuIEl0IGZyZWVkIG1lIHRvIGVudGVyIG9uZSBvZiB0aGUgbW9zdCBjcmVhdGl2ZSBwZXJpb2RzIG9mIG15IGxpZmUuIFwiO319XCI7XCI7In0seyJvcHRpb25fbmFtZSI6IlNIT190ZXh0dXJlZF90aGVtZSIsIm9wdGlvbl92YWx1ZSI6ImF1cmEifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3RpbXRodW1iX3pjIiwib3B0aW9uX3ZhbHVlIjoiU21hcnQgY3JvcCBhbmQgcmVzaXplIn0seyJvcHRpb25fbmFtZSI6IlNIT190b2dnbGVfY3VzdG9tX2ZvbnQiLCJvcHRpb25fdmFsdWUiOiJHb29nbGUgV2ViZm9udHMifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3RyYWNraW5nX2NvZGUiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3R3aXR0ZXIiLCJvcHRpb25fdmFsdWUiOiJodHRwOlwvXC90d2l0dGVyLmNvbVwvIyFcL3dwdGl0YW4ifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3R3aXR0ZXJfaWQiLCJvcHRpb25fdmFsdWUiOiIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3VjX2RhdGUiLCJvcHRpb25fdmFsdWUiOiIyMSJ9LHsib3B0aW9uX25hbWUiOiJTSE9fdWNfbW9udGgiLCJvcHRpb25fdmFsdWUiOiIxMiJ9LHsib3B0aW9uX25hbWUiOiJTSE9fdWNfeWVhciIsIm9wdGlvbl92YWx1ZSI6IjIwMTIifSx7Im9wdGlvbl9uYW1lIjoiU0hPX3lvdXR1YmUiLCJvcHRpb25fdmFsdWUiOiJodHRwOlwvXC93d3cueW91dHViZS5jb21cL3VzZXJcL3dwdGl0YW5zIn1d ";

$theme_options = base64_decode($theme_options);	
$input = json_decode($theme_options,true);
	
foreach($input as $key => $val)
update_option($val["option_name"],$val["option_value"]);

$slider_options = array("Grid Wall","jQuery Slider","Scrollable Slider","3D(flash) Slider","Accordion Slider","HTML5 Slider","jQuery Slider","jQuery Slider","jQuery Slider");

$sliders = array();

for($i=1;$i<9;$i++)
{
	$sl = array();
	for($j=0;$j<13;$j++)
	{
		$sl[] =  array
                        (
                            "slide_title" => "Pellentesque nec cursus nisl.",
                            "slide_link" => "#",
                            "slide_image" => URL."/sprites/i/default.jpg",
                            "description" => "Vestibulum accumsan tristique massa, ac aliquam augue volutpat eget. Donec justo eros, gravida tristique ornare sit amet, rhoncus a leo. Nulla facilisi. Integer rutrum nisl eros. "
                        );
	}
	
	$sliders["Slider {$i}"] = array(
	        "title" => "Slider {$i}",
            "width" => 980,
            "height" => 445,
            "type" => $slider_options[$i-1],
            "interval" => "5",
            "autoplay" => "true",
            "controls" => "true" ,
			"slides" => $sl
	);
}
 
 
$force_option = array(

SN."_active_sidebars" =>  array ( "Right Sidebar" ,"Right Sidebar 2","Right Sidebar 1 "),
SN."_flickr_key" => "",
SN."_flickr_name" => "",
SN."_home_column1_image" => "",
SN."_home_column2_image" => "",
SN."_home_column3_image" => "",
SN."_home_column4_image" => "",
SN."_home_staged_image" => URL."/sprites/i/default.jpg",
SN."_home_static_image" => URL."/sprites/i/default.jpg",
SN."_logo" => URL."/sprites/i/logo.png",
SN."_stage_option" => "Slider",
SN."_home_slider" => "Slider 1",
SN."_forms" => array(),
SN."_sliders" => serialize($sliders)
);
	
	foreach($force_option as $k => $v)
    update_option($k,$v);	
	

 
 
 		
}