<?php
    include(HPATH."/helper/phpFlickr.php");
	
	$key = (!$super_options[SN."_flickr_key"]) ? false : $super_options[SN."_flickr_key"];
    $flickr_name = (!$super_options[SN."_flickr_name"]) ? NULL : $super_options[SN."_flickr_name"];    
	
	 if(!$key) { echo '<div class="error_box"> <h4> No API KEY ADDED </h4> </div>'; } else { 
  
  $f = new phpFlickr($key);
  $person = $f->people_findByUsername($flickr_name);
  $photos_url = $f->urls_getUserPhotos($person['id']);
  $photos = $f->people_getPublicPhotos($person['id'], NULL, NULL, 16);
 
				   }
  
  
   if($key) { 
  ?>
  
  
  <div class="galleria" style='height:550px;'>
  <?php 
  $slides = get_post_meta($post->ID,"gallery_items",true);
  
  foreach ((array)$photos['photos']['photo'] as $photo) { 
  $theImageSrc = $f->buildPhotoURL($photo, "medium_640");
      echo "<a href='".($theImageSrc)."'  ><img src='".($theImageSrc)."' alt=\"".$photos_url.$photo["id"]."\" title='$photo[title]' /></a>";
  }
  
  
  ?>
  
  </div>
   <div id="galleria_crop" class="hide"><?php echo $super_options[SN."_galleria_crop"]; ?></div>
  <?php } ?>