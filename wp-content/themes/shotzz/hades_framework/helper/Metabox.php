<?php

/* ======================================================================= */
/* == Custom Box Maker =================================================== */
/* ======================================================================= */

/* 

Author - WPTitans
Code Name - Custom Box Maker
Version - 1.0
Description - Creates custom meta boxes for themes that works on Hades Plus Framework.

*/


if(!class_exists('CustomBox')) {

// == Class defination begins	=====================================================
  class CustomBox {
  
  private $metaData;
  private $name;
  private $page; 
  private $context; 
  private $priority;
  private $fields;
  private $tag;
  function __construct($title,$data)
  {
	
	$this->name = $title;
	$this->metaData = $data;
	$this->page = $data["post_type"];
	$this->context = $data["context"]; 
	$this->priority = $data["priority"];
	$this->fields = $data["input_fields"];
	$this->tag = trim(str_replace(" ","",$this->name)) ; 
	add_action( 'admin_init', array( &$this, 'add_custom_meta_box' ) );
	add_action( 'save_post', array( &$this, 'custom_save_data' ));
  }
  
  
  
  function add_custom_meta_box(){
	
	  
    	add_meta_box( 
		    $this->tag,
			$this->name , 
			array(&$this,"custom_html_wrap")  , 
			$this->page, 
			$this->context, 
			$this->priority );
	 }
	 
  function custom_html_wrap(){
	    global $post;
		$tag = $this->tag;
		$custom = get_post_meta($post->ID,$tag,true);
        echo '<input type="hidden" name="'.$tag.'" id="'.$tag.'" value="'.wp_create_nonce($tag).'" />';
      
		
	    $i = 0;
		$field = $this->fields;
	    for(;$i<count($field[0]);$i++)
	    {
			switch($field[0][$i])
			{
				case "text" : ?>  
				
                <div class="hades_input clearfix"><label for="<?php echo $field[2][$i]; ?>" style=""> <?php echo $field[1][$i]; ?> </label><input type="text" name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>" value="<?php 
				  if( get_post_meta($post->ID, $field[2][$i],true)!="") 
				    echo get_post_meta($post->ID, $field[2][$i],true); else echo $field[3][$i]; ?>" /></div>
				
				<?php break;
				
				case "datepicker" : ?>  
				
                <div class="hades_input clearfix"><label for="<?php echo $field[2][$i]; ?>" style=""> <?php echo $field[1][$i]; ?> </label><input type="text" name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>" value="<?php 
				  if( get_post_meta($post->ID, $field[2][$i],true)!="") 
				    echo get_post_meta($post->ID, $field[2][$i],true); else echo $field[3][$i]; ?>" class="ev_datepicker" /></div>
				
				<?php break;
				
				case "textarea" : ?>  
				
                <div class="hades_input clearfix"><label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label><textarea type="text" name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>"  ><?php 
				  if( get_post_meta($post->ID, $field[2][$i],true)!="") 
				    echo get_post_meta($post->ID, $field[2][$i],true); else echo $field[3][$i]; ?></textarea></div>
				
				<?php break;
				
				case "checkbox" : $options = explode(",",$field[3][$i]); ?>  
				
                <div class="hades_input clearfix">
                <label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label>
                    <div class="hades_radio clearfix">
                     
                    <?php $j =0; $checked = ''; $correct_option = get_post_meta($post->ID, $field[2][$i],true);
					
					if(!is_array($correct_option)) $correct_option = array();
					
					
					 foreach($options as $option) {
						
						if(in_array($option,$correct_option))
						$checked = 'checked="checked"';
						else
						$checked = '';
						
						echo '  <p class="clearfix"><label for="'.$field[2][$i].$j.'"> '.$option.' </label> <input type="checkbox" value="'.$option.'" name="'.$field[2][$i].'[]" id="'.$field[2][$i].$j.'" '.$checked.' /> </p>';
						$j++; } ?>
                    
                    </div>
                </div>
				
				<?php break;
				
				
				case "radio" : $options = explode(",",$field[3][$i]); ?>  
				
                <div class="hades_input clearfix">
                <label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label>
                    <div class="hades_radio">
                     
                  <?php $j =0; $checked = ''; $correct_option = get_post_meta($post->ID, $field[2][$i],true); foreach($options as $option) {
						
						if($correct_option==$option)
						$checked = 'checked="checked"';
						else
						$checked = '';
						
						echo '  <p class="clearfix"><label for="'.$field[2][$i].$j.'"> '.$option.' </label> <input type="radio" value="'.$option.'" name="'.$field[2][$i].'" id="'.$field[2][$i].$j.'" '.$checked.' /> </p>';
						$j++; } ?>
                    
                    </div>
                </div>
				
				<?php break;
				
				
				case "select" : $checked = ''; $correct_option = get_post_meta($post->ID, $field[2][$i],true);  $options = explode(",",$field[3][$i]); ?>  
				
                <div class="hades_input clearfix">
                <label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label>
                <select name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>">     
                    <?php $j =0; foreach($options as $option) {
						
						if($correct_option==$option)
						$checked = 'selected="selected"';
						else
						$checked = '';
						
						echo '  <option value="'.$option.'" '.$checked.' > '.$option.' </option>';
						$j++; } ?>
                    
               </select>
                </div>
				
				<?php break;
				
				case "image" : $options = explode(",",$field[3][$i]); ?>  
				  <div class="hades_input clearfix"><label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label><input type="text" name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>" value="<?php 
				  if( get_post_meta($post->ID, $field[2][$i],true)!="") 
				    echo get_post_meta($post->ID, $field[2][$i],true); else echo $field[3][$i]; ?>" /> <a href="#" class="button custom_upload_image_button"> Upload </a>  </div>
				<?php break;
				
				case "datepicker" : $options = explode(",",$field[3][$i]); ?>  
				  <div class="hades_input clearfix"><label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label><input type="text" name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>" value="<?php 
				  if( get_post_meta($post->ID, $field[2][$i],true)!="") 
				    echo get_post_meta($post->ID, $field[2][$i],true); else echo $field[3][$i]; ?>" class="datapicker" />   </div>
				<?php break;
				
				case "separator" : ?>  
				 <div class="mseparator clearfix"></div>
				<?php break;
				
				case "time" :
				$correct_option = get_post_meta($post->ID, $field[2][$i],true);
				$time = array("12:00 AM","12:15 AM","12:30 AM","12:45 AM","1:00 AM","1:15 AM","1:30 AM","1:45 AM","2:00 AM","2:15 AM","2:30 AM","2:45 AM","3:00 AM","3:15 AM","3:30 AM","3:45 AM","4:00 AM","4:15 AM","4:30 AM","4:45 AM","5:00 AM","5:15 AM","5:30 AM","5:45 AM","6:00 AM","6:15 AM","6:30 AM","6:45 AM","7:00 AM","7:15 AM","7:30 AM","7:45 AM","8:00 AM","8:15 AM","8:30 AM","8:45 AM","9:00 AM","9:15 AM","9:30 AM","9:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","1:00 PM","1:15 PM","1:30 PM","1:45 PM","2:00 PM","2:15 PM","2:30 PM","2:45 PM","3:00 PM","3:15 PM","3:30 PM","3:45 PM","4:00 PM","4:15 PM","4:30 PM","4:45 PM","5:00 PM","5:15 PM","5:30 PM","5:45 PM","6:00 PM","6:15 PM","6:30 PM","6:45 PM","7:00 PM","7:15 PM","7:30 PM","7:45 PM","8:00 PM","8:15 PM","8:30 PM","8:45 PM","9:00 PM","9:15 PM","9:30 PM","9:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM");
				 ?> 
				 <div class="hades_input clearfix">
                  <label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label>
                  <select name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>">     
                    <?php $j =0; foreach($time as $option) {
						
						if($correct_option==$option)
						$checked = 'selected="selected"';
						else
						$checked = '';
						
						echo '  <option value="'.$option.'" '.$checked.' > '.$option.' </option>';
						$j++; } ?>
                </select>
				</div>
				<?php break;
				
				
				case "country" : ?> 
				 <div class="hades_input clearfix">
                  <label for="<?php echo $field[2][$i]; ?>"> <?php echo $field[1][$i]; ?> </label>
                  <select name="<?php echo $field[2][$i]; ?>" id="<?php echo $field[2][$i]; ?>">     
                   <optgroup label="">
                        <option value="" selected="selected">Select Country</option> 
                    </optgroup>
                    <optgroup label="common choices">
                        <option value="United Kingdom">United Kingdom</option> 
                        <option value="United States">United States</option> 
                        <option value="France">France</option> 
                        <option value="Germany">Germany</option> 
                        <option value="Spain">Spain</option> 
                        <option value="Italy">Italy</option> 
                        <option value="Canada">Canada</option> 
                    </optgroup>
                    <optgroup label="other countries">
                        <option value="Afghanistan">Afghanistan</option> 
                        <option value="Albania">Albania</option> 
                        <option value="Algeria">Algeria</option> 
                        <option value="American Samoa">American Samoa</option> 
                        <option value="Andorra">Andorra</option> 
                        <option value="Angola">Angola</option> 
                        <option value="Anguilla">Anguilla</option> 
                        <option value="Antarctica">Antarctica</option> 
                        <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                        <option value="Argentina">Argentina</option> 
                        <option value="Armenia">Armenia</option> 
                        <option value="Aruba">Aruba</option> 
                        <option value="Australia">Australia</option> 
                        <option value="Austria">Austria</option> 
                        <option value="Azerbaijan">Azerbaijan</option> 
                        <option value="Bahamas">Bahamas</option> 
                        <option value="Bahrain">Bahrain</option> 
                        <option value="Bangladesh">Bangladesh</option> 
                        <option value="Barbados">Barbados</option> 
                        <option value="Belarus">Belarus</option> 
                        <option value="Belgium">Belgium</option> 
                        <option value="Belize">Belize</option> 
                        <option value="Benin">Benin</option> 
                        <option value="Bermuda">Bermuda</option> 
                        <option value="Bhutan">Bhutan</option> 
                        <option value="Bolivia">Bolivia</option> 
                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
                        <option value="Botswana">Botswana</option> 
                        <option value="Bouvet Island">Bouvet Island</option> 
                        <option value="Brazil">Brazil</option> 
                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
                        <option value="Brunei Darussalam">Brunei Darussalam</option> 
                        <option value="Bulgaria">Bulgaria</option> 
                        <option value="Burkina Faso">Burkina Faso</option> 
                        <option value="Burundi">Burundi</option> 
                        <option value="Cambodia">Cambodia</option> 
                        <option value="Cameroon">Cameroon</option> 
                        <option value="Canada">Canada</option> 
                        <option value="Cape Verde">Cape Verde</option> 
                        <option value="Cayman Islands">Cayman Islands</option> 
                        <option value="Central African Republic">Central African Republic</option> 
                        <option value="Chad">Chad</option> 
                        <option value="Chile">Chile</option> 
                        <option value="China">China</option> 
                        <option value="Christmas Island">Christmas Island</option> 
                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
                        <option value="Colombia">Colombia</option> 
                        <option value="Comoros">Comoros</option> 
                        <option value="Congo">Congo</option> 
                        <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
                        <option value="Cook Islands">Cook Islands</option> 
                        <option value="Costa Rica">Costa Rica</option> 
                        <option value="Cote D'ivoire">Cote D'ivoire</option> 
                        <option value="Croatia">Croatia</option> 
                        <option value="Cuba">Cuba</option> 
                        <option value="Cyprus">Cyprus</option> 
                        <option value="Czech Republic">Czech Republic</option> 
                        <option value="Denmark">Denmark</option> 
                        <option value="Djibouti">Djibouti</option> 
                        <option value="Dominica">Dominica</option> 
                        <option value="Dominican Republic">Dominican Republic</option> 
                        <option value="Ecuador">Ecuador</option> 
                        <option value="Egypt">Egypt</option> 
                        <option value="El Salvador">El Salvador</option> 
                        <option value="Equatorial Guinea">Equatorial Guinea</option> 
                        <option value="Eritrea">Eritrea</option> 
                        <option value="Estonia">Estonia</option> 
                        <option value="Ethiopia">Ethiopia</option> 
                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
                        <option value="Faroe Islands">Faroe Islands</option> 
                        <option value="Fiji">Fiji</option> 
                        <option value="Finland">Finland</option> 
                        <option value="France">France</option> 
                        <option value="French Guiana">French Guiana</option> 
                        <option value="French Polynesia">French Polynesia</option> 
                        <option value="French Southern Territories">French Southern Territories</option> 
                        <option value="Gabon">Gabon</option> 
                        <option value="Gambia">Gambia</option> 
                        <option value="Georgia">Georgia</option> 
                        <option value="Germany">Germany</option> 
                        <option value="Ghana">Ghana</option> 
                        <option value="Gibraltar">Gibraltar</option> 
                        <option value="Greece">Greece</option> 
                        <option value="Greenland">Greenland</option> 
                        <option value="Grenada">Grenada</option> 
                        <option value="Guadeloupe">Guadeloupe</option> 
                        <option value="Guam">Guam</option> 
                        <option value="Guatemala">Guatemala</option> 
                        <option value="Guinea">Guinea</option> 
                        <option value="Guinea-bissau">Guinea-bissau</option> 
                        <option value="Guyana">Guyana</option> 
                        <option value="Haiti">Haiti</option> 
                        <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
                        <option value="Honduras">Honduras</option> 
                        <option value="Hong Kong">Hong Kong</option> 
                        <option value="Hungary">Hungary</option> 
                        <option value="Iceland">Iceland</option> 
                        <option value="India">India</option> 
                        <option value="Indonesia">Indonesia</option> 
                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
                        <option value="Iraq">Iraq</option> 
                        <option value="Ireland">Ireland</option> 
                        <option value="Israel">Israel</option> 
                        <option value="Italy">Italy</option> 
                        <option value="Jamaica">Jamaica</option> 
                        <option value="Japan">Japan</option> 
                        <option value="Jordan">Jordan</option> 
                        <option value="Kazakhstan">Kazakhstan</option> 
                        <option value="Kenya">Kenya</option> 
                        <option value="Kiribati">Kiribati</option> 
                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
                        <option value="Korea, Republic of">Korea, Republic of</option> 
                        <option value="Kuwait">Kuwait</option> 
                        <option value="Kyrgyzstan">Kyrgyzstan</option> 
                        <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
                        <option value="Latvia">Latvia</option> 
                        <option value="Lebanon">Lebanon</option> 
                        <option value="Lesotho">Lesotho</option> 
                        <option value="Liberia">Liberia</option> 
                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
                        <option value="Liechtenstein">Liechtenstein</option> 
                        <option value="Lithuania">Lithuania</option> 
                        <option value="Luxembourg">Luxembourg</option> 
                        <option value="Macao">Macao</option> 
                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
                        <option value="Madagascar">Madagascar</option> 
                        <option value="Malawi">Malawi</option> 
                        <option value="Malaysia">Malaysia</option> 
                        <option value="Maldives">Maldives</option> 
                        <option value="Mali">Mali</option> 
                        <option value="Malta">Malta</option> 
                        <option value="Marshall Islands">Marshall Islands</option> 
                        <option value="Martinique">Martinique</option> 
                        <option value="Mauritania">Mauritania</option> 
                        <option value="Mauritius">Mauritius</option> 
                        <option value="Mayotte">Mayotte</option> 
                        <option value="Mexico">Mexico</option> 
                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
                        <option value="Moldova, Republic of">Moldova, Republic of</option> 
                        <option value="Monaco">Monaco</option> 
                        <option value="Mongolia">Mongolia</option> 
                        <option value="Montserrat">Montserrat</option> 
                        <option value="Morocco">Morocco</option> 
                        <option value="Mozambique">Mozambique</option> 
                        <option value="Myanmar">Myanmar</option> 
                        <option value="Namibia">Namibia</option> 
                        <option value="Nauru">Nauru</option> 
                        <option value="Nepal">Nepal</option> 
                        <option value="Netherlands">Netherlands</option> 
                        <option value="Netherlands Antilles">Netherlands Antilles</option> 
                        <option value="New Caledonia">New Caledonia</option> 
                        <option value="New Zealand">New Zealand</option> 
                        <option value="Nicaragua">Nicaragua</option> 
                        <option value="Niger">Niger</option> 
                        <option value="Nigeria">Nigeria</option> 
                        <option value="Niue">Niue</option> 
                        <option value="Norfolk Island">Norfolk Island</option> 
                        <option value="Northern Mariana Islands">Northern Mariana Islands</option> 
                        <option value="Norway">Norway</option> 
                        <option value="Oman">Oman</option> 
                        <option value="Pakistan">Pakistan</option> 
                        <option value="Palau">Palau</option> 
                        <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
                        <option value="Panama">Panama</option> 
                        <option value="Papua New Guinea">Papua New Guinea</option> 
                        <option value="Paraguay">Paraguay</option> 
                        <option value="Peru">Peru</option> 
                        <option value="Philippines">Philippines</option> 
                        <option value="Pitcairn">Pitcairn</option> 
                        <option value="Poland">Poland</option> 
                        <option value="Portugal">Portugal</option> 
                        <option value="Puerto Rico">Puerto Rico</option> 
                        <option value="Qatar">Qatar</option> 
                        <option value="Reunion">Reunion</option> 
                        <option value="Romania">Romania</option> 
                        <option value="Russian Federation">Russian Federation</option> 
                        <option value="Rwanda">Rwanda</option> 
                        <option value="Saint Helena">Saint Helena</option> 
                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
                        <option value="Saint Lucia">Saint Lucia</option> 
                        <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
                        <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
                        <option value="Samoa">Samoa</option> 
                        <option value="San Marino">San Marino</option> 
                        <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
                        <option value="Saudi Arabia">Saudi Arabia</option> 
                        <option value="Senegal">Senegal</option> 
                        <option value="Serbia and Montenegro">Serbia and Montenegro</option> 
                        <option value="Seychelles">Seychelles</option> 
                        <option value="Sierra Leone">Sierra Leone</option> 
                        <option value="Singapore">Singapore</option> 
                        <option value="Slovakia">Slovakia</option> 
                        <option value="Slovenia">Slovenia</option> 
                        <option value="Solomon Islands">Solomon Islands</option> 
                        <option value="Somalia">Somalia</option> 
                        <option value="South Africa">South Africa</option> 
                        <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
                        <option value="Spain">Spain</option> 
                        <option value="Sri Lanka">Sri Lanka</option> 
                        <option value="Sudan">Sudan</option> 
                        <option value="Suriname">Suriname</option> 
                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
                        <option value="Swaziland">Swaziland</option> 
                        <option value="Sweden">Sweden</option> 
                        <option value="Switzerland">Switzerland</option> 
                        <option value="Syrian Arab Republic">Syrian Arab Republic</option> 
                        <option value="Taiwan, Province of China">Taiwan, Province of China</option> 
                        <option value="Tajikistan">Tajikistan</option> 
                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
                        <option value="Thailand">Thailand</option> 
                        <option value="Timor-leste">Timor-leste</option> 
                        <option value="Togo">Togo</option> 
                        <option value="Tokelau">Tokelau</option> 
                        <option value="Tonga">Tonga</option> 
                        <option value="Trinidad and Tobago">Trinidad and Tobago</option> 
                        <option value="Tunisia">Tunisia</option> 
                        <option value="Turkey">Turkey</option> 
                        <option value="Turkmenistan">Turkmenistan</option> 
                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
                        <option value="Tuvalu">Tuvalu</option> 
                        <option value="Uganda">Uganda</option> 
                        <option value="Ukraine">Ukraine</option> 
                        <option value="United Arab Emirates">United Arab Emirates</option> 
                        <option value="United Kingdom">United Kingdom</option> 
                        <option value="United States">United States</option> 
                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
                        <option value="Uruguay">Uruguay</option> 
                        <option value="Uzbekistan">Uzbekistan</option> 
                        <option value="Vanuatu">Vanuatu</option> 
                        <option value="Venezuela">Venezuela</option> 
                        <option value="Viet Nam">Viet Nam</option> 
                        <option value="Virgin Islands, British">Virgin Islands, British</option> 
                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
                        <option value="Wallis and Futuna">Wallis and Futuna</option> 
                        <option value="Western Sahara">Western Sahara</option> 
                        <option value="Yemen">Yemen</option> 
                        <option value="Zambia">Zambia</option> 
                        <option value="Zimbabwe">Zimbabwe</option>
                    </optgroup>
                </select>
				</div>
				<?php break;
				
			}
		  
	    }
	 
	  
	 }	 
	
	function custom_save_data() {
		global $post;
		
		 if ( !wp_verify_nonce( $_POST[$this->tag], $this->tag ) )
         return;
	    
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;


		$i = 0;
		$field = $this->fields;
	    for(;$i<count($field[0]);$i++)
	    {
			update_post_meta($post->ID, $field[2][$i] , $_POST[$field[2][$i]]);
		}
		
		}
	 
	 
	 } // == Class Ends ================================================================
  }
  
