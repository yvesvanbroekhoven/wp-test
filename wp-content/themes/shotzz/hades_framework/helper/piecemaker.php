﻿<?php
$path = __FILE__;
$pathwp = explode( 'wp-content', $path );
$wp_url = $pathwp[0];

require_once( $wp_url.'/wp-load.php' );


$sliders = unserialize(get_option(SN.'_sliders'));

$name = $_GET['name'];
$slider = $sliders[$name];		
$slides = $slider['slides'];		
if(!is_array($slides))
$slides = array();

$width  = $slider["width"];
$height = $slider["height"];

$theme_path = get_template_directory_uri() ;		
$auto = $slider["interval"];
$menu_distance = 20;

if($slider['autoplay']!="true")
	$auto = false;

if($slider['controls']!="true")
		$menu_distance = 100; 
		 
$output = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<Piecemaker>

XML;

//Content
	
$output .= '	<Contents>';
	
 foreach($slides as $slide) :
        
        
             $theImageSrc = $slide["slide_image"];
							global $blog_id;
							if (isset($blog_id) && $blog_id > 0) {
							$imageParts = explode('/files/', $theImageSrc);
							if (isset($imageParts[1])) {
								$theImageSrc = '/blogs.dir/' . $blog_id . '/files/' . $imageParts[1];
							}
						}
			 $imgurl = $theme_path."/timthumb.php?src=".urlencode($theImageSrc)."&h={$height}&w={$width}";       
				 $output .= '		<Image Source="'.$imgurl.'" Title="'.$slide['slide_title'].'" >';    
                   $output .= '<Text>&lt;h1&gt;'.$slide['slide_title'].'&lt;/h1&gt;&lt;p&gt;'.strip_tags  ( stripslashes($slide['description']) ).'&lt;/p&gt;</Text>';
		
				$output .= '		</Image>'."\n";
		
     endforeach;  
	$output .= '	</Contents>'."\n";
	


//Settings
$output .= '<Settings ImageWidth="'.$width.'" ImageHeight="'.$height.'" LoaderColor="0x333333" InnerSideColor="0x222222" SideShadowAlpha="1.8" DropShadowAlpha="0.6" DropShadowDistance="2" DropShadowScale="1" DropShadowBlurX="30" DropShadowBlurY="4" MenuDistanceX="'.$menu_distance.'" MenuDistanceY="15" MenuColor1="0x999999" MenuColor2="0x333333" MenuColor3="0xFFFFFF" ControlSize="40" ControlDistance="20" ControlColor1="0x222222" ControlColor2="0xFFFFFF" ControlAlpha="0.8" ControlAlphaOver="0.95" ControlsX="'.($width/2).'" ControlsY="'.($height-40).'" ControlsAlign="center" TooltipHeight="30" TooltipColor="0x222222" TooltipTextY="5" TooltipTextStyle="P-Italic" TooltipTextColor="0xFFFFFF" TooltipMarginLeft="5" TooltipMarginRight="7" TooltipTextSharpness="50" TooltipTextThickness="-100" InfoWidth="'.(int)($width/2).'" InfoBackground="0xFFFFFF" InfoBackgroundAlpha="0.95" InfoMargin="15" InfoSharpness="0" InfoThickness="0" Autoplay="'.$auto.'" FieldOfView="45"></Settings>';

//End
$output .= <<<XML
	<Transitions>
    <Transition Pieces="9" Time="1.2" Transition="easeInOutBack" Delay="0.1" DepthOffset="300" CubeDistance="30"></Transition>
    <Transition Pieces="15" Time="3" Transition="easeInOutElastic" Delay="0.03" DepthOffset="200" CubeDistance="10"></Transition>
    <Transition Pieces="5" Time="1.3" Transition="easeInOutCubic" Delay="0.1" DepthOffset="500" CubeDistance="50"></Transition>
    <Transition Pieces="9" Time="1.25" Transition="easeInOutBack" Delay="0.1" DepthOffset="900" CubeDistance="5"></Transition>
  </Transitions>
	
</Piecemaker>
XML;

echo $output;

?>