<?php

// == Layout Calculation =========================================

$hasSidebar = "";
$sidebar =    get_post_meta($post->ID,'_sidebar',true);
$sidebar = (trim($sidebar)=="") ? "right-sidebar" : $sidebar; // ~~ For posts existing prior to theme activation should have right sidebar by default ~ 

if($sidebar!="full") :
	
	$hasSidebar = ($sidebar == "right-sidebar") ?	"hasRightSidebar" : "hasLeftSidebar";
	$layout = 'two-third-width'; 
	$width = 629;
else :
	$width = 980;
	$layout = 'full-width';
endif;

 get_header(); ?>   

<?php  if(have_posts()): while(have_posts()) : the_post(); ?>
 <?php  if($super_options[SN."_breadcrumbs_enable"]=="true") $helper->breadcrumbs(); ?> <!-- The breadcrumb for the theme -->
<div id="page-starter"> <!--  Parent Slider Container -->
   <div class="title container">
     
   </div>
</div> <!-- End of Parent Slider Container -->


<div class="container page content   <?php echo $hasSidebar; ?> clearfix"> <!-- Start of loop -->
   
    <div class="<?php if($sidebar!="full") echo "two-third-width"; else echo "full-width"; ?> preload" id="main-content">
 	 <div class="post-title">
     <h1 class="custom-font"><?php the_title(); ?></h1>
     </div>   
     <?php 
	 if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) 
	 {
		   $id = get_post_thumbnail_id();
		   $ar = wp_get_attachment_image_src( $id, array(9999,9999) );
		   $theImageSrc = $helper->getMUFix($ar[0]);
		   echo $helper->imageDisplay($theImageSrc , 350 , $width , false , true );  
	 }
	 ?>
     <?php the_content(); ?>
    
    
    
     <?php if($super_options[SN."_social_set"]=="" || $super_options[SN."_social_set"]=="ON") { ?>     
         <div class="social-stuff clearfix">
                <h6 class="custom-font heading"> <?php _e("Share this:",'h-framework'); ?></h6> <?php $helper->socialStuff(); ?> 
         </div>  
      <?php } ?>   
     
		<div class="details"><p>This entry was posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?> and is filed under <?php the_category(', ') ?>. You can follow any responses to this entry through the <?php post_comments_feed_link('RSS 2.0'); ?> feed or <a href="">trackback</a> from your own site. 
		<?php if ( comments_open() && pings_open() ) {
		// Both Comments and Pings are open ?>
		You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(); ?>" rel="trackback">trackback</a> from your own site.
		
		<?php } elseif ( !comments_open() && pings_open() ) {
		// Only Pings are Open ?>
		Responses are currently closed, but you can <a href="<?php trackback_url(); ?> " rel="trackback">trackback</a> from your own site.
		
		<?php } elseif ( comments_open() && !pings_open() ) {
		// Comments are open, Pings are not ?>
		You can skip to the end and leave a response. Pinging is currently not allowed.
		
		<?php } elseif ( !comments_open() && !pings_open() ) {
		// Neither Comments, nor Pings are open ?>
		Both comments and pings are currently closed.
		<?php }  ?>
		</p></div><!-- .details -->
   
    
    <?php if($super_options[SN."_author_bio"]=="" || $super_options[SN."_author_bio"]=="true") { ?>                    
    <div id="authorbox" class="clearfix">  
      <div class="author-avatar">
      
      <?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '80' ); }?>  
      </div>
      <div class="authortext">   
      <h3 class="custom-font">About the author</h3>
      <p><?php the_author_meta('description'); ?></p>  
      </div>  
    </div>
    <?php }?>  
    
    
    <?php if($super_options[SN."_posts_comments"]!="No") : ?>
    <div id="comments_template">
              <?php comments_template(); ?>
    </div>
    <?php endif; ?>
    
    
     
    </div>
     <?php  
	  wp_reset_query();
	  if($sidebar!="full") 
	  get_sidebar();  
	 ?> 

</div>

<?php endwhile; endif; ?> <!-- End of loop -->

<?php get_footer(); ?>
      