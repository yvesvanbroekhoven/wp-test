// JavaScript Document
document.documentElement.className += 'active';
jQuery(function($){
	
$.fn.KBSlider = function(options){
	
/* ================================================================================================ */
/* == Plugin Options ============================================================================== */
/* ================================================================================================ */	

	var defaults = {
		              time:4000,
					  width:500,
					  height:300,
					  effect:'none',
					  autoplay:true,
					  mode:"all",
					  listControls:true,
					  arrowControls:true,
					  callback:function(){   } ,
					  rel : 10
					  
					};
	
	
	var options = $.extend(defaults, options);
/* ================================================================================================ */
/* == Variables & Precaching ====================================================================== */
/* ================================================================================================ */	
	
	return this.each(function()
		{	
		var root = $(this).addClass('mainslider');
		root.wrap('<div class="KBSlider" />');	
		var parent = root.parent(),
		li = root.find("li"),
	    images = li.find("img"),
		temp,pos,rel,speed,timer,image_timer,counter,arr,wait,index,block,w,h,src,parent,im,override=false,in_animation = false,controls,root_parent,        current = li.eq(1).toggleClass('active'),prev = li.first().addClass("reset"),
		bool = true,first_bool = true, image_matrix = Array();
		
		li.each(function(i){
			temp = Array();
			
			temp[0] =  $(this).find("img").width();
			temp[1] =  $(this).find("img").height();
			im = getScaleSize(temp[1],temp[0],options.height,options.width);
            temp[2] = im[0];
			temp[3] = im[1];		
			image_matrix[i] = temp;
			
			});
		options.width = root.width();
		options.height = root.height();
		
		
		
		root.css({
			width: root.width(),
			height: root.height()
			});	
			
		parent.css({
			width: root.width(),
			height: root.height()
			});	
		
		if(root.find('.controls').length>0)
		{
			if(root.find('.controls').val()=="true")
			{
				options.listControls=true;
				options.arrowControls=true;
			}
			else
			{
				options.listControls=false;
				options.arrowControls=false;
			}
		}
		
		if(root.find('.autoplay').length>0)
		{
			if(root.find('.autoplay').val()=="true")
				options.autoplay=true;
			else
				options.autoplay=false;
			
		}
		
		if(root.find('.interval').length>0)
				options.time=root.find('.interval').val();
		if(root.find('.mode').length>0)
				options.mode = root.find('.mode').val();
		if(root.find('.effect').length>0)
				options.effect = parseInt(root.find('.effect').val());
	
	
	
		root.find('input').remove();
			
		li.first().find("span").css("display","block");
		type= "img";
		current.find(type).hide();	
	
		if(options.listControls==true)
	    appendControls();
	    if(options.arrowControls==true)
		appendarrowControls()		
/* ================================================================================================ */
/* === Switcher Module ============================================================================ */
/* ================================================================================================ */			
		function switcher()
			{
				prev = (current.prev().length>0) ? current.prev().removeClass("reset") : li.last().removeClass("reset");
				current.toggleClass("active reset");
				
				current = (current.next().length>0)  ? current.next() : li.first(); 
			    current.find(type).hide();
				
				current.find('img').css({
					  top : 0 , left : 0 ,
					  width: image_matrix[current.index()][0] ,
					  height : image_matrix[current.index()][1]
					  });
					
				current.addClass("active");
				options.callback(current.find(type)[0]);
		   }

/* ================================================================================================ */
/* == Custom Effects ============================================================================== */
/* ================================================================================================ */	

// == ~~ Pan Top Left to Bottom Right  ~~ ====================================

	  
	 function panDiagonalTop(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		wfactor = 0,
		hfactor = 0,
		rel =parseInt(options.rel);
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor > ( getData[0] - options.width ) || hfactor > ( getData[1] - options.height ) ) {
				  clearInterval(wait);
				 
			  }
			
			  im.style.left = 	-wfactor+"px";
			  im.style.top = 	-hfactor+"px";
			 
			  wfactor = wfactor + w_ratio;
			  hfactor = hfactor + h_ratio;
			   
		  }, speed);
		
	};	

// == ~~ Pan Bottom Right to Top Left  ~~ ====================================

	  
	 function panDiagonalBottom(image)
	{
		
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		wfactor = ( getData[0] - options.width ),
		hfactor = ( getData[1] - options.height ),
		rel =parseInt(options.rel);
		
		image.css({ "left" : -(getData[0] - options.width)+"px" , "top" : -(getData[1] - options.height)+"px"  });
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
	    clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor <= 0 || hfactor <= 0  ) {
				  clearInterval(wait);
				  
			  }
			
			  im.style.left = 	-wfactor+"px";
			  im.style.top = 	-hfactor+"px";
			 
			  wfactor = wfactor - w_ratio;
			  hfactor = hfactor - h_ratio;
			   
		  }, speed);
	};	
		
// == ~~ Pan Bottom to Top  ~~ ====================================

	  
	 function panBottomToTop(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		hfactor = ( getData[1] - options.height ),
		rel =parseInt(options.rel);
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		image.css("top",-(getData[1] - options.height)+"px");
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( hfactor <= 0 ) {
				  clearInterval(wait);
				  }
			
			  im.style.top = 	-hfactor+"px";
			 
			 
			  hfactor = hfactor - h_ratio;
		  }, speed);
		
	};	
	
	
// == ~~ Pan Right to Left  ~~ ====================================

	  
	 function panRightToLeft(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		wfactor = ( getData[0] - options.width ),
		rel =parseInt(options.rel);
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		image.css("left",-(getData[0] - options.width)+"px");
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor <= 0 ) {
				  clearInterval(wait);
				 
			  }
			
			  im.style.left = 	-wfactor+"px";
			 
			 
			  wfactor = wfactor - w_ratio;
		  }, speed);
		
	};	
	

// == ~~ Pan Left to Right  ~~ ====================================

	  
	 function panLeftToRight(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		wfactor = 0,
		rel =parseInt(options.rel);
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor > ( getData[0] - options.width ) ) {
				  clearInterval(wait);
				  
			  }
			
			  im.style.left = 	-wfactor+"px";
			 
			 
			  wfactor = wfactor + w_ratio;
		  }, speed);
		
	};	
	
// == ~~ Pan Top to Bottom  ~~ ====================================

	  
	 function panTopToBottom(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		hfactor = 0,
		rel =parseInt(options.rel);
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( hfactor > ( getData[1] - options.height ) ) {
				  clearInterval(wait);
				  
			  }
			
			  im.style.top = 	-hfactor+"px";
			 
			 
			  hfactor = hfactor + h_ratio;
		  }, speed);
		
	};	
	
	// == ~~ Zoom Out Only ~~ ====================================

	  
	 function zoomOutTopLeft(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		
		wfactor = getData[0],
		hfactor = getData[1],
		rel = 3;
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * 20  )/options.time;
		var h_ratio = ( ( getData[1] - getData[3] ) * 20 )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( (diagonal_d*2) * rel ) /options.time;
		
		
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor < options.width || hfactor < options.height ) {
				  clearInterval(wait);
				 
			  }
			
			  im.style.width = 	wfactor+"px";
			  im.style.height = hfactor+"px";
			    
			  wfactor = wfactor - w_ratio;
			  hfactor = hfactor - h_ratio;
		  }, speed);
		
	};	
	
// == ~~ Zoom Out Only ~~ ====================================

	  
	 function zoomInTopLeft(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		
		wfactor = getData[2],
		hfactor = getData[3],
		rel =parseInt(options.rel);
		
		image.css({
			width:getData[2],height:getData[3]
			});
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor > getData[0] || hfactor > getData[1] ) {
				  clearInterval(wait);
				  
			  }
			
			  im.style.width = 	wfactor+"px";
			  im.style.height = hfactor+"px";
			    
			  wfactor = wfactor + w_ratio;
			  hfactor = hfactor + h_ratio;
		  }, speed);
		
	};	

// == ~~ Zoom In Top Right Only ~~ ====================================

	  
	 function zoomInTopRight(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		
		wfactor = getData[2],
		hfactor = getData[3],
		wpanfactor = 0,
		rel =parseInt(options.rel);
		
		image.css({
			width:getData[2],height:getData[3]
			});
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor > getData[0] || hfactor > getData[1] ) {
				  clearInterval(wait);
				  
			  }
			
			  im.style.width = 	wfactor+"px";
			  im.style.height = hfactor+"px";
			  im.style.left =  -wpanfactor+"px";
			  
			  wpanfactor =    wpanfactor + w_ratio;
			  wfactor = wfactor + w_ratio;
			  hfactor = hfactor + h_ratio;
		  }, speed);
		
	};	


// == ~~ Zoom In Bottom Right Only ~~ ====================================

	  
	 function zoomInBottomRight(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		
		wfactor = getData[2],
		hfactor = getData[3],
		wpanfactor = 0,
		hpanfactor = 0,
		rel =parseInt(options.rel);
		
		image.css({
			width:getData[2],height:getData[3]
			});
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor > getData[0] || hfactor > getData[1] ) {
				  clearInterval(wait);
				  
			  }
			
			  im.style.width = 	wfactor+"px";
			  im.style.height = hfactor+"px";
			  im.style.left =  -wpanfactor+"px";
			  im.style.top =  -hpanfactor+"px";
			  
			  wpanfactor = wpanfactor + w_ratio;
			  hpanfactor = hpanfactor + h_ratio;
			  wfactor = wfactor + w_ratio;
			  hfactor = hfactor + h_ratio;
		  }, speed);
		
	};	

// == ~~ Zoom In Bottom Top Only ~~ ====================================

	  
	 function zoomInBottomLeft(image)
	{
		in_animation = true;
		
		// == Get the Image Matrix ===============
		
		var getData = image_matrix[image.parents("li").index()],
		
		// == Calculate the decrement factor ===============
		
		wfactor = getData[2],
		hfactor = getData[3],
		hpanfactor = 0,
		rel =parseInt(options.rel);
		
		image.css({
			width:getData[2],height:getData[3] 
			});
		
		// == Get the Image ratio (necessary for proportional effect) ===============
		
		var w_ratio = ( ( getData[0] - getData[2] ) * rel )/options.time;
		var h_ratio = ( ( getData[1] - getData[3] ) * rel )/options.time;
		
		// == Get speed by calculation diagonal distance wrt viewport and image dimensions =============
		
		var viewport_d =   Math.sqrt( options.width * options.width +  options.height + options.height );
		var viewport_i =   Math.sqrt( getData[0] *  getData[0] +  getData[1] + getData[1] );
		
		var diagonal_d = viewport_i - viewport_d;
		
		var speed = ( diagonal_d * rel * 30 ) /options.time;
		
		image.fadeIn('slow');
		
		im = image[0];
		
		clearInterval(timer);
		endeffect(image);
		current.find("span").fadeIn('normal');
		in_animation = false;
		if(override==false) // Return if manually triggered
		image_timer = setTimeout(function() {  
		clearInterval(wait);   current.find("span").fadeOut('fast');    switcher(); effects();  },options.time); 
		
	  	wait = setInterval(function() {
			
			  if( wfactor > getData[0] || hfactor > getData[1] ) {
				  clearInterval(wait);
				  
			  }
			
			  im.style.width = 	wfactor+"px";
			  im.style.height = hfactor+"px";
			  im.style.top =  -hpanfactor+"px";
			  
			  hpanfactor = hpanfactor + h_ratio;
			  wfactor = wfactor + w_ratio;
			  hfactor = hfactor + h_ratio;
		  }, speed);
		
	};	
				
/* ================================================================================================ */
/* ================================= Effects Switching & Ending =================================== */
/* ================================================================================================ */		
	
	function endeffect(image)
	{
        if(options.listControls==true)
			   {
			   controls.removeClass("control_active");
			   controls.eq(current.index()).addClass("control_active");
			   }
		
				  
						
	};
	function effects()
	{
		 if(li.find("img").is(":animated") )
		 return;
		 
		 var ch = Math.floor(Math.random()* 11);
		
		 if(!isNaN(options.effect))
		 ch = options.effect;
		  
		 if(bool==true)
		  {
			  li.first().find("span").hide();
			  bool=false;
			   first_bool = false;
		  }
		 
		 switch(ch)
		 {
		
		 case 0:panBottomToTop(current.find("img"));break;
		 case 1:panLeftToRight(current.find("img"));break;	 
		 case 2:panTopToBottom(current.find("img"));break;	 
		 case 3:panRightToLeft(current.find("img"));break;	
		 
		 case 4: panDiagonalTop(current.find("img")); break;
		 case 5: panDiagonalBottom(current.find("img")); break;
		  
		 case 6:zoomInTopLeft(current.find("img"));break;
		 case 7:zoomOutTopLeft(current.find("img"));break;
		 
		 case 8:zoomInTopRight(current.find("img"));break;
		 case 9:zoomInBottomRight(current.find("img"));break;
		 case 10:zoomInBottomLeft(current.find("img"));break;
		 
		 }
	}

/* ================================================================================================ */
/* == Control Options ============================================================================= */
/* ================================================================================================ */	

	function appendarrowControls()
	{
		var prev = jQuery("<a href='#'>").addClass('q-prev');
		parent.append(prev);
		var next = jQuery("<a href='#'>").addClass('q-next');
		parent.append(next);
		
		parent.find(".q-prev").bind("click",function(e){
			 var index = current.index()-1;
			 if(first_bool==true&&index==0)
			 index = 4; 
			 
			 if(index<0)
			 index = li.length-1;
			 setImage(index);  
			 e.preventDefault();
			});
		parent.find(".q-next").bind("click",function(e){
			 var index = current.index()+1;
			 if(first_bool==true&&index==2)
			 index = 1; 
			 
			 if(index>li.length-1)
			 index = 0;
			 setImage(index);  
			 e.preventDefault();
			});	
		
	}
		 function appendControls()
	 {
		var str = "<ul class='controls'>";
		for(var i=0;i<li.length;i++)
		str = str + "<li>"+(i+1)+"</li>";
		str = str+"</ul>";
		
		 root.after(str);
		 
		 controls = parent.find(".controls li");
		controls.first().addClass("control_active");
		
		controls.bind({
		click:function(){ setImage($(this).index()); 	},
		mouseover:function(){ $(this).toggleClass("control_hover"); },
		mouseout:function(){ $(this).toggleClass("control_hover"); }
		  });
		 
		
	 }

/* ================================================================================================ */
/* == Image Settings ============================================================================== */
/* ================================================================================================ */	
	 
      function setImage(index)
	{  
	
     	if(first_bool==true)
	    {
			 if(in_animation==true||current.index()-1==index)
		return;
		}
		else
	  if(in_animation==true||current.index()==index)
		return;
		
		li.removeClass("reset active");
		current.find("span").hide();	
		clearTimeout(image_timer); // Manual Override...
		
		if(first_bool==true)
		li.first().addClass("reset");
		
		current.addClass("reset");
		prev = current;
		current = li.eq(index).addClass("active");
		current.find('img').hide();
		override = true;
		effects();
	
	}
	
	
	
	 function getScaleSize(imgh,imgw, maxh, maxw) {
  var ratio = maxw/maxh , dim = Array();
  if (imgh/imgw> ratio){
     // height is the problem
    if (imgh > maxh){
     
    }
  } else {
    // width is the problem
    if (imgw > maxh){
      dim[1] = Math.round(imgh*(maxw/imgw));
      dim[0] = maxw;
    }
  } 
  return dim;
}

		
			
			if(options.autoplay==true)
			 image_timer = setTimeout(function() {   effects();  },4000);  // Starting the Slideshow
			
		});
	
	


};
 
});