jQuery(function($){
	
$.fn.titangallery = function(options){
  var defaults = {
	width:960,
	height:550,
	interval:4000,
	loadtime:100
	  };
	  
  var options = $.extend(defaults, options);
		
  var obj = $(this),temp,i,j,timer,parent,current_thumb;
  var carousel = obj.find(".thumbnails-carousel");
  var thumbnails = carousel.find(".cwrapper a");	
  var thumb_images = thumbnails.children("img").css("visibility","hidden");
  var stage = obj.children(".main_stage");
  var medium_thumb = carousel.find(".medium_thumb"),medium_img = medium_thumb.children("img");
  var bottom_bar = obj.find('.titan_bottom_bar');
  stage.height(obj.height()-30) , caption = stage.find('div.caption');
 current_thumb = thumbnails.first();
  
  var scrollable =carousel.find(".cwrapper_wrap").scrollable({ api:true });
  var pos = {
	  
	  x_min : carousel.find(".cwrapper_wrap").position().left,
	  x_max : carousel.find(".cwrapper_wrap").position().left +  carousel.find(".cwrapper_wrap").width()
	  };
  
 
  bottom_bar.find('.cprev').click(function(e){  scrollable.prev(); e.preventDefault(); });
  bottom_bar.find('.cnext').click(function(e){  scrollable.next();  e.preventDefault(); });
  
  var preload = function(container,time){
	   
	  temp = container.find("img");
	
	  temp.each(function(){
		  
		  $(this).bind("load error",function(){
		 
		  $(this).css({ "visibility": "visible" }).animate({ opacity:"1" },time);
		  
		  }).each(function(){
                if(this.complete || ($.browser.msie && parseInt($.browser.version) == 6)) { $(this).trigger('load'); }
            });
		  
	  });
	   
	  }
  
  var setmainimage = function(src){
    caption.hide();
	 stage.children('a').attr('href',src);
	temp = stage.find("img").animate({opacity:0},function(){
	$(this).remove();
	
	
	
	if(current_thumb.position().left+current_thumb.width()>pos.x_max  )
	{
		scrollable.next();
	}
	if(current_thumb.position().left<pos.x_min )
	{
		scrollable.prev();
	}
	
	var in_hover = false;
	obj.hover(function(){
		
		caption.fadeIn('fast');
		in_hover = true;
		},function(){
			in_hover = false;
			setTimeout(function(){ 	caption.fadeOut('fast'); },1000);
			
			});
	
	temp = jQuery("<img />").attr("src",src).css({ "visibility":"hidden" , "opacity" : 0 })
	stage.children("a").append(temp);
	preload(stage.children("a"));
    var flag = true;
	
	temp.bind("load",function(e){ 
	
	stage.find('div.caption h4').html(current_thumb.find('.title').val());
    stage.find('div.caption p').html(current_thumb.find('.desc').val());
    caption.fadeIn('normal');
	setTimeout(function(){ 
		if(!in_hover)
		caption.fadeOut('fast'); 
		},3000);
	
  	  if(flag==true) {
		
	     if(this.complete==true)
	     flag = false;
	    }
	 
	  });
	});
   
	
  };
  
  
  
   preload(obj,250);
   stage.find('div.caption h4').html(current_thumb.find('.title').val());
   stage.find('div.caption p').html(current_thumb.find('.desc').val());
   
   
   
   thumbnails.hover(function(){
	  temp = $(this);
	  var offset = temp.children("img").height() - 67;
	  
	  medium_img.attr("src",temp.children("img").attr("src"));
	  
	  medium_img.css({ "visibility":"hidden" , "opacity" : 0 });
	  
	  medium_thumb.hide().css("left",temp.position().left-39);
	  medium_thumb.stop(true,true).fadeIn("normal");
	   
	   },function(){
		   
		 medium_thumb.fadeOut("fast");
		   
		   });
   
   thumbnails.bind("click",function(e){
	   current_thumb = $(this);
	   setmainimage(current_thumb.attr("href"));
	
	   e.preventDefault();
	   });
   
   obj.find(".gallery_prev").click(function(e){
	   current_thumb = (current_thumb.prev().length==0) ? thumbnails.last() : current_thumb.prev();
	   setmainimage(current_thumb.attr("href"));
	   e.preventDefault();
	   });
  
    obj.find(".gallery_next").click(function(e){
	   current_thumb = (current_thumb.next().length==0) ? thumbnails.first() : current_thumb.next();
	   setmainimage(current_thumb.attr("href"));
	   e.preventDefault();
	   });	
	   
	   
	  $(window).load(function(){ caption.fadeIn('normal'); 
	  
	  }); 
	   
  /* ======================================================= */
  /* == End of plugin ====================================== */
  /* ======================================================= */
  
};



});
	
