<?php
/**
 * @KingSize 2011
 **/
 ###### Theme Setting #########
global $get_options;
$get_options = get_option('wm_theme_settings');
###############################
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head> <!-- Header starts here -->
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="keywords" content="juwelier, juweliers, Haesevoets, merk, merken, topmerk, topmerken, trendy, exclusief, exclusieve, collectie, herstelling, jong, modern, juweel, juwelen, uurwerk, uurwerken, horloge, horloges, ring, ringen, sieraden, trouwring, trouwringen, collier, hanger, ketting, armband, diamant, Baume Mercier, Baume, Mercier, IWC, Schaffhausen, Bell Ross, Jean, Richard, JeanRichard, Steffen, Swiss Kubik, Haesevoets Exquisite, Centoventuno, Mattioli, Pasquale Bruni, Tamara Comolli, Comoli, Comolli, Vhernier" />
		<meta name="description" content="Juwelier Haesevoets in Waregem staat al 20 jaar garant voor perfectie. Ontdek exclusieve juwelen en horloges van topmerken binnen een aangenaam en sereen kader. Van klassiek tot modern, trendy of tijdloos, altijd klassevol. Samen met u gaan we op zoek naar de prachtigste combinaties binnen onze uitgebreide collectie van juwelen en uurwerken. Net als u heeft elk juweel of uurwerk een eigen verhaal — een eigen kleur of timbre. Een uitgelezen service met topmerken voor uw tevredenheid. Kwaliteit, dag na dag, al 20 jaar." />
		
		<title><?php wp_title('&laquo; ', true, 'right'); ?><?php bloginfo('name'); ?></title> <!-- Website Title of WordPress Blog -->	
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/fonts.css" /> <!-- Style Sheet -->
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" /> <!-- Style Sheet -->
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" /> <!-- Pingback Call -->

		<!--[if lte IE 8]>						
			<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/stylesIE.css" type="text/css" media="screen" />
		<![endif]-->		
		<!--[if lte IE 7]>				
			<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/stylesIE7.css" type="text/css" media="screen" />
		<![endif]-->
		
		
		<script type="text/javascript">		
			// Template Directory going here
			var template_directory = '<?php echo get_template_directory_uri(); ?>';

		</script>
		
		
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/swfobject.js" type="text/javascript" charset="utf-8"></script>
		
		<script type="text/javascript">
	    //	swfobject.registerObject("jhplayer", "9.0.115", "<?php echo get_template_directory_uri(); ?>/expressInstall.swf");
	    </script>
		
		<script type="text/javascript">		
			// Homepage Background Slider Options
			var sliderTime = <?php echo get_option('wm_slider_seconds');?>; // time between slides change in ms
			var slideDirection = '<?php echo get_option('wm_slider_transition'); ?>'; // fade, horizontal, vertical
			var dir = '<?php echo get_option('wm_slider_direction'); ?>'; // direction; 'tb' = normal, 'bt' = reversed
		</script>

		<!-- Do Not Remove the Below -->
		<?php if(is_singular()) wp_enqueue_script('comment-reply'); ?>
		<?php if ($tpl_body_id!="slideviewer") {  wp_enqueue_script("jquery"); } ?>
		<?php wp_head(); ?>
		<!-- Do Not Remove the Above -->
		
		<!-- theme setting head include wp admin -->
		<?php
		$head_include = "";
		$head_include = $get_options['wm_head_include'];
		echo $head_include;
		?>
		<!-- End theme setting head include -->
		
		<!-- Portfolio control CSS and JS-->		
		<?php include (TEMPLATEPATH . '/lib/portofolio_template_style_js.php'); ?>		
		<!-- END Portfolio control CSS and JS-->
		
		<?php if ( get_option('wm_no_rightclick_enabled') == "1" ) {?>
		<!-- Disable Right-click -->
		<script type="text/javascript" language="javascript">
			jQuery(function($) {
				$(this).bind("contextmenu", function(e) {
					e.preventDefault();
				});
			}); 
		</script>
		<!-- END of Disable Right-click -->
		<?php } ?>
		
		<?php if( get_option('wm_custom_css') ) { ?><style><?php echo get_option('wm_custom_css');?></style><?php } ?>

		
</head> 
<!-- Header ends here -->
<?php
  //getting the current page template set from the page custom options	
  $current_page_template = get_option('current_page_template');  

 //Overlay handling	
    $body_overlay = "body_home";
  if ( get_option('wm_grid_hide_enabled') == "1" ) { 	
	$body_overlay = "body_about";
  }
?>
<?php if(is_home()) {?>
<!--[if lte IE 7]>				
<style>
.body_home #menu_wrap
{margin: 0;}
</style>
<![endif]-->
<body <?php body_class('body_home'); ?>>

<?php
function get_browserInfo() 
{ 
    $userAgent = $_SERVER['HTTP_USER_AGENT']; 
    $browserName = 'Unknown';
    $browserPlatform = 'Unknown';
    $browserVersion = '';

    if (preg_match('/linux/i', $userAgent)) {
        $browserPlatform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $userAgent)) {
        $browserPlatform = 'mac';
    }
    elseif (preg_match('/windows|win32|win64/i', $userAgent)) {
        $browserPlatform = 'win';
    }
    
    if (preg_match('/iPod|iPhone|iPad/i', $userAgent)) {
        $browserPlatform = 'ios';
    }
    
    if(preg_match('/MSIE/i',$userAgent) && !preg_match('/Opera/i',$userAgent)) 
    { 
        $browserName = 'explorer'; 
        $ub = 'MSIE'; 
    } 
    elseif(preg_match('/Firefox/i',$userAgent)) 
    { 
        $browserName = 'firefox'; 
        $ub = 'Firefox'; 
    } 
    elseif(preg_match('/Chrome/i',$userAgent)) 
    { 
        $browserName = 'chrome'; 
        $ub = 'Chrome'; 
    } 
    elseif(preg_match('/Safari/i',$userAgent)) 
    { 
        $browserName = 'safari'; 
        $ub = 'Safari'; 
    } 
    elseif(preg_match('/Konqueror/i',$userAgent)) 
    { 
        $browserName = 'safari'; 
        $ub = 'Safari'; 
    } 
    elseif(preg_match('/Opera/i',$userAgent)) 
    { 
        $browserName = 'opera'; 
        $ub = 'Opera'; 
    } 
    elseif(preg_match('/Netscape/i',$userAgent)) 
    { 
        $browserName = 'netscape'; 
        $ub = 'Netscape'; 
    } 
    
    $known = array('Version', $ub, 'other');
    $pattern = '#(?P<browser>'.join('|', $known).')[/ ]+(?P<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $userAgent, $matches)) 
    {
    }
    
    $i = count($matches['browser']);
    if ($i != 1) {
        if (strripos($userAgent, 'Version') < strripos($userAgent,$ub))
        {
            $browserVersion = $matches['version'][0];
        }
        else 
        {
            $browserVersion = $matches['version'][1];
        }
    }
    else 
    {
        $browserVersion = $matches['version'][0];
    }
    
    if ($browserVersion == null || $browserVersion == '') 
    {
    	$browserVersion = '?';
    }
    
    $browserVersionSimple = preg_split('/\./', $browserVersion);
    
    return array(
        'name' => $browserName,
        'version' => $browserVersionSimple[0],
        'platform' => $browserPlatform
    );
} 

$modernBrowser = false;
$browserInfo = get_browserInfo();

$browserName = $browserInfo['name'];
$browserVersion = $browserInfo['version'];
$browserPlatform = $browserInfo['platform'];

$sound = 0;

if ($sound == 1)
{
?>

	    <div style="position:absolute;z-index:10;">
	    	<?php if (($browserName == 'explorer') && ($browserVersion == '9')) { ?>
	    	
	       	<object type="application/x-shockwave-flash" data="<?php echo get_template_directory_uri(); ?>/stream.swf" width="0" height="0">
				<param name="movie" value="<?php echo get_template_directory_uri(); ?>/stream.swf" />
			</object>
			
			<?php } elseif (($browserName == 'explorer') && ($browserVersion == '8')) { ?>
	    	
	       	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="0" height="0" id="stream" align="middle" >
			    <param name="movie" value="<?php echo get_template_directory_uri(); ?>/stream.swf" />
			</object>
			
			<?php } else { ?>
	    
	      	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="0" height="0" id="stream" align="middle" >
			    <param name="movie" value="<?php echo get_template_directory_uri(); ?>/stream.swf" />
			   	<?php if ($browserName != 'explorer') { ?>
			    <object type="application/x-shockwave-flash" data="<?php echo get_template_directory_uri(); ?>/stream.swf" width="0" height="0">
			        <param name="movie" value="<?php echo get_template_directory_uri(); ?>/stream.swf" />
			    </object>
			    <?php } ?>
			</object>
			
			<?php } ?>
	   </div>

<?php 
}
	} else {?>
<body <?php body_class($body_overlay." ".$current_page_template); ?>>
<?php } ?>

	<!-- START of the Full-width Background Image -->				
	<?php include (TEMPLATEPATH . '/lib/theme-background.php'); ?>		
	<!-- END of the Full-width Background Image -->

    <!-- Wrapper starts here -->
	<div id="wrapper">
		
	     <!-- Navigation starts here -->	 
		<div id="menu_wrap">
			
			<!-- Menu starts here -->
			<div id="menu">
		    	
				<!-- Logo -->
		      	<div id="logo">   
				  <?php
				  //get custom logo
				  $theme_custom_logo = $get_options['wm_logo_upload'];

					if(!empty($theme_custom_logo))
					{
						$url = get_template_directory_uri();
					?>
				  	  <style type="text/css" media="all" scoped="scoped">
						#logo h1 a {
							background: url("<?php echo $theme_custom_logo ?>") no-repeat scroll center top transparent;
						}
					   </style>							
					<?php
					}
				  ?>
			        <h1><a href="<?php echo home_url(); ?>/" class="logo_image index" ></a></h1>      
		      	</div>
		      	<!-- Logo ends here -->
		      
		      	
		      	<!-- Navbar -->
				<?php 
					wp_nav_menu( array(
					 'sort_column' =>'menu_order',
					 'container' => 'ul',
					 'theme_location' => 'header-nav',
					 'fallback_cb' => 'null',
					 'menu_id' => 'navbar',
					 'link_before' => '',
					 'link_after' => '',
					 'depth' => 0,
					 'walker' => new description_walker())
					 );
				?>
			    <!-- Navbar ends here -->			    	       
		    </div>
		    <!-- Menu ends here -->
		    
		    <!-- Hide menu arrow -->
			<?php if ( get_option('wm_menu_hide_enabled') == "1" ) {?>
		    <div id="hide_menu">   
		    	<a href="#" class="menu_visible">Hide menu</a> 
					
					<?php if ( get_option('wm_menu_tooltip_enabled') == "1" ) {?>
		        	<div class="menu_tooltip">
						
                        <div class="tooltip_hide"><?php if( get_option('wm_menu_tooltip') ) { ?><p><?php echo get_option('wm_menu_tooltip');?></p><?php } else { ?><p>Hide the navigation</p><?php } ?></div>
						<div class="tooltip_show"><?php if( get_option('wm_menu_show_tooltip') ) { ?><p><?php echo get_option('wm_menu_show_tooltip');?></p><?php } else { ?><p>Show the navigation</p><?php } ?></div>
						
			        </div>  
					<?php } else { ?>
					<!-- No Tool Tip -->
					<?php } ?>					
		    </div>
				<?php } else { ?>	
				<div id="hide_menu">    
				</div>
				<?php } ?>
				<!-- Hide menu arrow ends here -->
		       
		</div>
		<!-- Navigation ends here -->
	
	<?php if(is_home()) {?>
	</div>
	<?php } ?>
	