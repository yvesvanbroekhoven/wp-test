<?php
/**
 * @KingSize 2011
 * Comment Form single.php
 **/
?>
<?php if ('open' == $post->comment_status) : ?> 		

	<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
		<p>Je moet <a href="<?php echo wp_login_url( get_permalink() ); ?>">ingelogd zijn</a> om een reactie te verzenden.</p><br/>
	<?php else : ?>
<!-- Leave a comment box -->										
	<div id="respond">
		<h3 class="comments_head">Laat een bericht achter</h3>	
		<p class="comment_info">Velden met een * zijn verplicht</p>	

		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="comment_form"> 	
		<?php if ( is_user_logged_in() ) : ?>
			Ingelogd als <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Uitloggen &raquo;</a><br/><br/>
		<?php else : ?>
			<p><label class="name_label" for="form_name">Naam*</label>
			<input type="text" name="author" id='form_name' class="textbox" /></p>
				
			<p><label class="email_label" for="form_email"> E-mail*</label> 
			<input type="text" name="email" id='form_email' class="textbox"/></p>
					
			<p><label class="website_label" for="form_website"> Site</label>
			<input type="text" name="url" id='form_website' class="textbox"/></p>
		<?php endif; ?>
		
			<p> <label class="message_label" for="form_message">Bericht*</label> <textarea name="comment" id='form_message' rows="6" cols="25" class="textbox"></textarea> </p>
					
			<p><input type="submit" name="submit" id="form_submit" value="Submit!" />&nbsp;<?php cancel_comment_reply_link("Cancel Reply"); ?> </p>
			<?php comment_id_fields(); ?> 
			<?php do_action('comment_form', $post->ID); ?>
		</form>	
	</div>
<!-- Leave a comment box ends here -->	

	<?php endif; // If registration required and not logged in ?>
<?php endif; ?> 